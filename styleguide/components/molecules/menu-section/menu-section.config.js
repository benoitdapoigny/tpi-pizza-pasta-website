/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Menu Section',
	display: {
		padding: '30px',
		background: '#F0F0F0'
	},
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			title: 'Burgers',
			list: [
				{
					name: 'The Beef Burger',
					description: 'Gruyère, rucola, oignons rouges, moutarde en grains et mayonnaise, pommes frites',
					price: '20.00'
				},
				{
					name: 'The Beef Burger',
					description: 'Gruyère, rucola, oignons rouges, moutarde en grains et mayonnaise, pommes frites',
					price: '20.00'
				},
				{
					name: 'The Beef Burger',
					description: 'Gruyère, rucola, oignons rouges, moutarde en grains et mayonnaise, pommes frites',
					price: '20.00'
				},
				{
					name: 'The Beef Burger',
					description: 'Gruyère, rucola, oignons rouges, moutarde en grains et mayonnaise, pommes frites',
					price: '20.00'
				}
			],
			informations: [
				{
					title: 'Sauces au choix',
					list: ['Poivre vert: 4.50', 'Beurre maison: 4.50', 'Morilles: 4.50']
				},
				{
					title: 'Provenance de nos viandes',
					list: ['Suisse: boeuf et veau', 'Canada, Australie: cheval', 'France, Brésil: poulet']
				}
			],
			holiday: null
		},
	},
	variants: [
		{
			name: 'weekmenu',
			display: {
				background: '#2B2828'
			},
			context: {
				modifiers: ['menu-section--weekmenu'],
				html_attrs: [],
				data: {
					title: 'Proposition de la semaine',
					list: [
						{
							name: 'Entrecôte Parisienne de boeuf, beurre maison, pomme fites, salade',
							description: null,
							price: '23.00'
						},
						{
							name: 'La chasse, spätzli et tout le tsoin tsoin',
							description: null,
							price: '28.00'
						},
						{
							name: 'Salade italienne',
							description: 'Tomates, mozarellade buffala, ruccola et jambon cru',
							price: '17.50'
						},
						{
							name: 'Pizza orientale',
							description: 'Sauce tomate, mozarella, merguez, poivrons, oeuf cru, origano',
							price: '17.00'
						},
					],
					informations: null,
					holiday: null
				},
			},
		},
	],
};
