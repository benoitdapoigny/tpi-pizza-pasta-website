Pizza Pasta Styleguide
====================

This styleguide is more than a styleguide. It's our toolbox to develop kickass styleguide-drive-award-winning-futuristic frontends. It includes:

a css framework
---------------

### What's included
- theme settings (colors, sizes,…)
- sass helpers (functions, mixins)
- css resets
- basic print styles

### Need
Have a common ground in our projects that encourages best practices and avoid re-inventing the wheel for simple things.

It could be used with potentially any stack. Think of this framework as the cornerstone for all our components.

### Philosophy

Keep it low, brother.

* **low complexity** - it has to be understandable by a mid-level developer in 1h
* **low dependency** - we want to use this in different contexts and stacks
* **low abstraction** - we are not creating the new cssJQuery, `mixin float-left() {float: left;}` is useless and doesn't encourage us developers to know what we're doing
* **low surprise** - if we cannot know what a helper does by reading its name, drop it (see [POLA](https://en.wikipedia.org/wiki/Principle_of_least_astonishment))
* **low defaults** - resets and global styles are kept to a minimum, so we don't need to override a lot in our code

_Think of it this way: We do custom experience websites and apps. We don't want to use an abstraction of what the web platform offers to make it "easier". We want to understand the tools and have access to some simple explicit helpers that save time and encourage best practices._

### Discover
  1. Read the file `./main.scss` to understand what's included
  2. Read the file `./scss/_variables.scss` to review what we have as variables
  3. Go through the documentation on http://localhost:4000/

basic components
----------------

### What's included
- most common atoms (buttons, typography,…)
- most common molecultes (accordion, modal,…)
- examples of layouts

### Need
Have a set of simple common components that we can use to kickstart the development of a page/site/app.

When we build a project, we want to be able to focus on customizing and improving details (performance, accessibility, interactions), rather than bootstrapping always-needed components and taking existential naming decisions all the time.

### Philosophy

Keep it low, brother.

* **low leaking** - actually, ZERO leaking; when I edit component-a I don't expect component-b to change even a tiny bit. it's (almost) always better to have duplicated code than unexpected side-effects.
* **low dependency** - even though we have a nice css framework, we want to keep our components modular and exportable. An example of what we don't want is http://refills.bourbon.io/. Even though the components are nice, they depend on bourbon so strongly (lots of variables used) that bourbon cannot evolve freely and it requires quite some work to use the components if we don't include the whole bourbon stack as well.
* **low surprise** - each component is responsible of its own styles and the position of its children, that's it.

### Discover
Open your styleguide and check components, including their css, html and config code, all visible in the bottom panel.
