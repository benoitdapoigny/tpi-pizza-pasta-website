/**
 * Allow to track events to Google Analytics and/or Facebook Pixel
 * To enable it, add the data-metric attribute to an Element.
 *
 * data-metric="my event name"		The value of the attribute is optional. Default: click
 * data-metric-type="event type"	Optional. The event type to triger the event. Default: click
 * data-metric-label="My label"		Optional. The associate label of name the event. Default: null
 *
 * @class MetricTracking
 */
export default class MetricTracking {

	/**
	 * Constructor.
	 */
	constructor(element, $) {
		this.el = element;
		this.$ = $;
	}

	/**
	 * Bind.
	 */
	bind() {
		this.name = this.el.data('metric') || 'click';
		this.type = this.el.data('metric-type') || 'click';
		this.label = this.el.data('metric-label') || null;

		this.el.on( this.type, this.handler.bind(this) );
	}

	/**
	 * Event handler.
	 */
	handler() {
		if (window.ga) window.ga( 'send', 'event', this.name, this.label );
		if (window.fbq) window.fbq( 'trackCustom', this.name, {label: this.label} );
	}

	destroy() {
		// Unbind events attached to the DOM
		this.el.off( this.type );
	}

}
