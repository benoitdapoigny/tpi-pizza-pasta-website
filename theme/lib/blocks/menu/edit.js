import { Component, Fragment } from '@wordpress/element';
import { Button } from '@wordpress/components';

import MenuSection from './menu-section';

export default class Menu extends Component {
	constructor(...args) {
		super(...args);

		this.state = {
		};
	}

	componentDidMount() {
		const { attributes: attrs } = this.props;
	}

	addRow(index) {
		const newSections = [...(this.props.attributes.sections || [])];
		newSections[index].list.push({ name: '', description: '', price: '' });
		this.props.setAttributes({ sections: newSections });
	}

	removeRow(index) {
		let newSections = [...(this.props.attributes.sections || [])];
		newSections.splice(index, 1);
		this.props.setAttributes({ sections: newSections });
	}

	render() {
		const { attributes: attrs, setAttributes, isSelected } = this.props;
		const sections = attrs.sections;
		if (typeof sections == 'undefined' || sections.length === 0) {
			setAttributes( { sections: [ {
				title: 'Insérez le titre de votre section',
				list: [
					{ name: 'Nom du plat, ex: Salade verte', description: 'Description du plat (optionel)', price: 'Prix, ex: 12.00' }
				]
			} ] } )
		}

		return (
			<Fragment>
				<div class="menu" data-menu>
					<div class="menu__inner">
						<h1 class="menu__title">Carte</h1>
						<ul class="menu__list">
							{ ( sections || [] ).map( ( card, i ) => (
								<li class="menu__list-section" key={ i }>
									<MenuSection
										context={ 'edit' }
										attrs={ card }
										addRow={ () => { this.addRow(i) } }
										removeRow={ () => { this.removeRow(i) } }
										updateSection={ () => { this.updateSection() } }
										sections={ this.props.attributes.sections }
										index={ i }
										setAttributes={ this.props.setAttributes }
									/>
								</li>
							) ) }
						</ul>
						<Button
							isPrimary
							onClick={ () => {
								const newSections = [...(sections || [])];
								newSections.push({title: '', list: [
										{ name: '', description: '', price: '' }
									]
								})
								this.props.setAttributes({ sections: newSections })
							} }
						>{'Ajouter une section'}</Button>
					</div>
				</div>
			</Fragment>
		);
	}
}
