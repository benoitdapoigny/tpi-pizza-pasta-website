/**
 * Add custom twig filters
 */
var swig = new require('swig');

/**
 * Returns if key is in object.
 *
 * @param key
 * @param object
 * @returns {boolean}
 */
swig.setFilter('in', function(key, object) {
  return key in object;
});

/**
 * Remove new line with tab and new line.
 *
 * @param input
 * @returns {string|*}
 */

swig.setFilter('parse_code', function(input, lang='') {
  // input = input.trim();
  // input = "\t" + input;
  // input = input.replace(/(?:\r\n|\r|\n)/g, "\n\t");
  // return input;
  input = input.trim();
	input = input.replace(/(?:\r\n|\r|\n)/g, "\n");
	input = "``` " + lang + "\n" + input + "\n" + "```"
  return input;
});

/**
* Output arguments for source code preview.
*
* @param item
* @returns {string}
*/

swig.setFilter('output_arg', function(item) {
 var i = 0;
 var output = "";
 if (item.parameter === undefined) {
	 return "";
 }
 item.parameter.forEach(function(parameter) {
	 var string = '$' + parameter.name;
	 if (parameter.default) {
		 string = string + ': ' + parameter.default;
	 }
	 if (parameter.type.toLowerCase() === 'arglist') {
		 string = string + '...';
	 }
	 if (output === "") {
		 output = string;
	 } else {
		 output = output + ', ' + string;
	 }
 });
 return output;
});

/**
* Output a parameters table.
*
* @param parameters
* @returns String
*/

swig.setFilter('output_parameters', function(parameters) {
 var tableHead = ['Name', 'Description', 'Type', 'Default Value'];
 var tableBody = [];
 var parts = ['name', 'description', 'type', 'default'];
 parameters.forEach(function(parameter){
	 var row = [];
	 parts.forEach(function(part){
		 var output = parameter[part] || "";

		 switch (part) {
			case 'name':
				output = '`$' + output + '`';
				break;

			case 'type':
				output = output.split(' | ');
				output = output.map(_ => '`' + _ + '`').join(' or ');
				break;

			case 'default':
				output = output.length ? '`' + output + '`' : '—'
				break;

			default:
				break;
		 }
		 row.push(output.replace("\n", ""));
	 });
	 tableBody.push(row);
 });
 return [
	'<table>',
		'<thead><tr>',
			tableHead.map(_ => `<td>${_}</td>`).join(''),
		'</tr></thead>',
		'<tbody><tr>',
			tableBody.map(_ =>
				`<td>${_.join('</td><td>')}</td>`
			).join('</tr><tr>'),
		'</tr></tbody>',
	'</table>',
 ].join('');
});

/**
 * Themeleon template helper, using consolidate.js module.
 *
 * See <https://github.com/themeleon/themeleon>.
 * See <https://github.com/tj/consolidate.js>.
 */
var themeleon = require('themeleon')().use('consolidate');

/**
 * Utility function we will use to merge a default configuration
 * with the user object.
 */
var extend = require('extend');

/**
 * SassDoc extras (providing Markdown and other filters, and different way to
 * index SassDoc data).
 *
 * See <https://github.com/SassDoc/sassdoc-extras>.
 */
var extras = require('sassdoc-extras');

/**
 * The theme function. You can directly export it like this:
 *
 *     module.exports = themeleon(__dirname, function (t) {});
 *
 * ... but here we want more control on the template variables, so there
 * is a little bit of preprocessing below.
 *
 * The theme function describes the steps to render the theme.
 */
var theme = themeleon(__dirname, function (t) {
  /**
   * Copy the assets folder from the theme's directory in the
   * destination directory.
   */
	// t.copy('assets');

	/**
	 * Render `views/index.swig` with the theme's context (`ctx` below)
	 * as `index.html` in the destination directory.
	 */
	// const groups = Object.keys(t.ctx.data.byGroupAndType);
	// const filename = groups.length ? groups[0] : 'index';
	// t.swig('views/index.swig', `${filename||'index'}.md`);
	// console.log(t.ctx.data.byGroupAndType.functions.function);
	t.swig('views/index.swig', 'index.md');
});

/**
 * Actual theme function. It takes the destination directory `dest`
 * (that will be handled by Themeleon), and the context variables `ctx`.
 *
 * Here, we will modify the context to have a `view` key defaulting to
 * a literal object, but that can be overriden by the user's
 * configuration.
 */
module.exports = function (dest, ctx) {
  var def = {
    display: {
			"annotations": {
				"function": ["description", "parameter", "return", "example", "throw", "require", "usedby"/*, "since"*/, "see", "todo", "link", "author"],
				"mixin": ["description", "parameter", "content", "example", "output", "throw", "require", "usedby"/*, "since"*/, "see", "todo", "link", "author"],
				"placeholder": ["description", "example", "throw", "require", "usedby"/*, "since"*/, "see", "todo", "link", "author"],
				"variable": ["description", "type", "property", "require", "example", "usedby"/*, "since"*/, "see", "todo", "link", "author"],
				"css": ["description", "example"/*, "since"*/, "see", "todo", "link", "author"]
			},
      access: ['public', 'private'],
      alias: false,
      watermark: true,
    },
    groups: {
      'undefined': 'General',
    },
    'shortcutIcon': 'http://sass-lang.com/favicon.ico',
  };

  // Apply default values for groups and display.
  ctx.groups = extend(def.groups, ctx.groups);
  ctx.display = extend(def.display, ctx.display);

  // Extend top-level context keys.
  ctx = extend({}, def, ctx);

  /**
   * Parse text data (like descriptions) as Markdown, and put the
   * rendered HTML in `html*` variables.
   *
   * For example, `ctx.package.description` will be parsed as Markdown
   * in `ctx.package.htmlDescription`.
   *
   * See <http://sassdoc.com/extra-tools/#markdown>.
   */
  extras.markdown(ctx);

  /**
   * Add a `display` property for each data item regarding of display
   * configuration (hide private items and aliases for example).
   *
   * You'll need to add default values in your `.sassdocrc` before
   * using this filter:
   *
   *     {
   *       "display": {
   *         "access": ["public", "private"],
   *         "alias": false
   *       }
   *     }
   *
   * See <http://sassdoc.com/extra-tools/#display-toggle>.
   */
  extras.display(ctx);

  /**
   * Allow the user to give a name to the documentation groups.
   *
   * We can then have `@group slug` in the docblock, and map `slug`
   * to `Some title string` in the theme configuration.
   *
   * **Note:** all items without a group are in the `undefined` group.
   *
   * See <http://sassdoc.com/extra-tools/#groups-aliases>.
   */
  extras.groupName(ctx);

  /**
   * Use SassDoc indexer to index the data by group and type, so we
   * have the following structure:
   *
   *     {
   *       "group-slug": {
   *         "function": [...],
   *         "mixin": [...],
   *         "variable": [...]
   *       },
   *       "another-group": {
   *         "function": [...],
   *         "mixin": [...],
   *         "variable": [...]
   *       }
   *     }
   *
   * You can then use `data.byGroupAndType` instead of `data` in your
   * templates to manipulate the indexed object.
   */
  ctx.data.byGroupAndType = extras.byGroupAndType(ctx.data);

  /**
   * Now we have prepared the data, we can proxy to the Themeleon
   * generated theme function.
   */
  return theme.apply(this, arguments);
};
