<?php

namespace SUPT\Blocks\Schedule;

use Timber\Timber;

/**
 * ACTION & FILTERS
 * ================
 */
add_action( 'init', __NAMESPACE__ . '\register_blocks' );

function register_blocks() {
	register_block_type( 'supt/schedule', [ 'render_callback' => __NAMESPACE__ . '\callback' ] );
}

function callback( $attrs ) {
	$context = [
		'data' => [
			'opening_schedule' => [
				1 => [
					'day'  => 'Lundi',
					'text' => $attrs['day1']
				],
				2 => [
					'day'  => 'Mardi - Vendredi',
					'text' => $attrs['day2']
				],
				3 => [
					'day'  => 'Samedi',
					'text' => $attrs['day3']
				],
				4 => [
					'day'  => 'Dimanche',
					'text' => $attrs['day4']
				],
			],
			'meal_schedule' => [
				1 => [
					'day'  => 'Lundi',
					'text' => $attrs['mealday1']
				],
				2 => [
					'day'  => 'Mardi - Vendredi',
					'text' => $attrs['mealday2']
				],
				3 => [
					'day'  => 'Samedi',
					'text' => $attrs['mealday3']
				],
				4 => [
					'day'  => 'Dimanche',
					'text' => $attrs['mealday4']
				],
			],
		]
	];

	return Timber::compile( 'organisms/section-schedule/section-schedule.twig', $context );
}
