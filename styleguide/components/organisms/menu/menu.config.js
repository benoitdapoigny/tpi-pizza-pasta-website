/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Menu',
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			list: [
				{
					data: {
						title: 'Entrées',
						list: [
							{
								name: 'Salade verte',
								price: '6.50'
							},
							{
								name: 'Salade mêlée',
								price: '8.00'
							},
							{
								name: 'Velouté aux légumes',
								price: '11.00'
							}
						],
						informations: null
					}
				},
				{
					data: {
						title: 'Propositions d\'été',
						list: [
							{
								name: 'Grande salade mêlée',
								price: '16.00'
							},
							{
								name: 'Salade Pacifique',
								description: 'Poulet, crevettes, ananas, salade verte',
								price: '19.50'
							},
							{
								name: 'Salade fitness',
								description: 'Salade mêlée, escalope de poulet grillée',
								price: '20.00'
							},
							{
								name: 'Salade César',
								description: 'Salade verte, émincé de poulet, croûtons, lardons, tomates',
								price: '20.00'
							},
							{
								name: 'Salade Gourmande',
								description: 'Salade verte, jambon, gruyère, lardons, oeuf dur, croûtons, tomates',
								price: '20.00'
							},
							{
								name: 'Salade de queues de crevettes tièdes',
								description: 'Salade verte, tomates, carottes, queues de crevettes',
								price: '20.00'
							},
						],
						informations: [
							{
								title: 'Sauces au choix',
								list: ['Poivre vert: 4.50', 'Beurre maison: 4.50', 'Morilles: 4.50']
							},
							{
								title: 'Provenance de nos viandes',
								list: ['Suisse: boeuf et veau', 'Canada, Australie: cheval', 'France, Brésil: poulet']
							}
						]
					}
				},
				{
					data: {
						title: 'Pâtes',
						list: [
							{
								name: 'Carbonara',
								price: '16.00'
							},
							{
								name: 'Bolognaise',
								price: '19.50'
							}
						],
						informations: null
					}
				},
				{
					data: {
						title: 'Burgers',
						list: [
							{
								name: 'The Beef Burger',
								description: 'Gruyère, rucola, oignons rouges, moutarde en grains et mayonnaise, pommes frites',
								price: '20.00'
							},
							{
								name: 'The Beef Burger',
								description: 'Gruyère, rucola, oignons rouges, moutarde en grains et mayonnaise, pommes frites',
								price: '20.00'
							}
						],
						informations: [
							{
								title: 'Sauces au choix',
								list: ['Poivre vert: 4.50', 'Beurre maison: 4.50', 'Morilles: 4.50']
							},
							{
								title: 'Provenance de nos viandes',
								list: ['Suisse: boeuf et veau', 'Canada, Australie: cheval', 'France, Brésil: poulet']
							}
						]
					}
				}
			]
		},
	},
};
