/**
 * Load assets
 */
import '../../../styleguide/icons';

/**
 * Global blocks styles
 * =====
 */
import './blocks-loader.scss';

/**
 * Existing blocks overrides/hacks
 * =====
 *
 * Example: (un)register custom styles
 *
 * @see https://wordpress.org/gutenberg/handbook/designers-developers/developers/filters/block-filters/
 */
import './core-button';

/**
 * Gutenberg blocks
 * =====
 *
 * Include editor entrypoint of each block, to:
 * - register the block
 * - inject styles in editor
 */

import './contact';
import './schedule';
import './menu';
import './news';
import './weekmenu';

/**
 * ACF blocks
 * =====
 *
 * Include editor entrypoint of each block, to:
 * - inject styles in editor
 * - run custom js in editor if needed
 */
// import './acf-blocks/accordion';

