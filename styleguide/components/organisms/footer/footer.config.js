/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Footer',
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			left: [
				'Pizza – Pasta',
				'Rue du Chemin 4',
				'1000 Lausanne'
			],
			right: [
				'Visitez notre page Facebook',
				'021 267 42 14',
				'contact@pizza-pasta.ch'
			]
		},
	},
};
