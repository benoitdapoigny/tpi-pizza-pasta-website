/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Section Schedule',
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			opening_schedule: [
				{
					day: 'Lundi',
					text: '9h - 14h30 / fermé'
				},
				{
					day: 'Mardi-Vendredi',
					text: '9h - 14h30 / 18h - 23h'
				},
				{
					day: 'Samedi',
					text: '11h - 14h30 / 18h - 23h'
				},
				{
					day: 'Dimanche',
					text: 'fermé'
				},
			],
			meal_schedule: [
				{
					day: 'Lundi',
					text: '11h30 - 14h / fermé'
				},
				{
					day: 'Mardi-Vendredi',
					text: '11h30 - 14h / 18h30 - 22h'
				},
				{
					day: 'Samedi',
					text: '11h30 - 14h / 18h - 22h30'
				},
				{
					day: 'Dimanche',
					text: 'fermé'
				}
			]
		},
	},
};
