<?php

namespace SUPT;

/**
 * THEME ENTRY POINT
 * =================
 */

require_once __DIR__ . '/SuptTheme.php';


/**
 * LIBRARIES
 * =========
 */
require_once __DIR__ .'/helpers/_loader.php';
require_once __DIR__ .'/admin/_loader.php';
require_once __DIR__ .'/customizer/_loader.php';
require_once __DIR__ .'/controllers/_loader.php';
require_once __DIR__ .'/blocks/_loader.php';
require_once __DIR__ .'/taxonomies/_loader.php';
require_once __DIR__ .'/post-types/_loader.php';
require_once __DIR__ .'/rest-api/_loader.php';
require_once __DIR__ .'/settings/_loader.php';
require_once __DIR__ .'/sidebars/_loader.php';
require_once __DIR__ .'/tweaks/_loader.php';
require_once __DIR__ .'/twig/_loader.php';
require_once __DIR__ .'/widgets/_loader.php';;
