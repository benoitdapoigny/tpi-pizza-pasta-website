<?php

namespace SUPT\Twig;

use \Twig_SimpleFilter;
use function \SUPT\get_image_attr;

// Action & filter hooks
add_filter( 'timber/twig', '\SUPT\Twig\add_filter_img' );

/**
 * Generate the img attributes.
 * If available use the Advanced image handling for Timber, Timmy @see https://github.com/mindkomm/timmy
 * or fallback to our custom get_image_attr function.
 *
 * @param int|array $img      Image attachment ID or an array with the attributes.
 * @param string    $size     Optional. Image size. Default value: 'page-content'.
 * @param boolean   $lazy     Optional. Prepend 'data-' to the image attributes. Default value: true.
 * @param boolean   $with_src Optional. Wether or not to include the "src" tag. Useful to exclude it for a `<source>` tag for example. Default value: true.
 * @return string   Returns available image attributes. (src, srcset, size, alt and title)
 */
function responsive_img( $img, $size = 'content', $lazy = true, $with_src = true) {
	$result = '';

	if ( empty($img) ) return $result;

	// Styleguide
	if ( is_array($img) ) {
		$result = attr_to_string($img);
	}
	// Wordpress
	else if (function_exists('get_timber_image_responsive')) {
		if($lazy) {
			$result = make_timber_image_lazy(
				get_timber_image_responsive($img, $size), ['srcset', 'src', 'sizes']
			);
		} else {
			$result = get_timber_image_responsive($img, $size);
		}
	}
	else {
		// Pass the attributes if the library isn't available
		$result = attr_to_string( get_image_attr( $img ) );
	}

	// strip src tag if needed
	if (!$with_src) {
		$result = preg_replace( '/(src="(?:.*)")/', "", $result );
	}

	return $result;
}

function attr_to_string( $attrs = [] ) {
	$strings = array();
	foreach ($attrs as $key => $value) {
		$strings[] = "$key=\"$value\"";
	}

	return implode(' ', $strings);
}

/**
 * Register our custom filter
 */
function add_filter_img( $twig ) {
	$twig->addFilter( new Twig_SimpleFilter( 'responsive', '\SUPT\Twig\responsive_img' ) );
	return $twig;
}
