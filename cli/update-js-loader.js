const fs = require('fs');
const path = require('path');

const CONFIG = require('../config.json');

const componentName = process.argv[3];
const componentPath = process.argv[2];

const indexPath = CONFIG.styleguide.src.loaders.js;
const fullPath = path.resolve(indexPath);

const capitalize = string => string.replace(/\w/, c => c.toUpperCase());

const importInsertion = `import ${capitalize(componentName)} from './${componentPath}/${componentName}/${componentName}';`;
const loaderInsertion = `\t{ view: ${capitalize(componentName)}, name: '${capitalize(componentName)}', selector: '[data-${componentName}]' },`;

let data = fs.readFileSync(fullPath).toString().split('\n');

const importIndex = data.indexOf('// === Imports ===');
data.splice(importIndex + 1, 0, importInsertion);

const loaderIndex = data.indexOf('// === Loader Object ===');
data.splice(loaderIndex + 2, 0, loaderInsertion);

data = data.join('\n');

fs.writeFile(indexPath, data, (err) => {
	if (err) throw err;
	console.log('index.js was successfully updated...');
});
