<?php

namespace SUPT\Types;

class PostType {
	/**
	 * Constants
	 */
	const MODEL_NAME = 'post';
	const ALLOWED_BLOCK_TYPES = [
		// Core blocks
		'core/paragraph',
		'core/heading',
		'core/list',
		'core/button',
		'core/image'

		// Custom blocks
	];

	/**
	 * Action & filter hooks
	 */
	public static function init() {
		add_action( 'init', [__CLASS__ , 'register_blocks_template'], 20 );
		add_action( 'allowed_block_types', [__CLASS__ , 'allowed_block_types'], 20, 2 );
	}

	/**
	 * Register the intial template
	 */
	public static function register_blocks_template() {
		$page_type_object = get_post_type_object( self::MODEL_NAME );
		$page_type_object->template = [
			[ 'core/heading' ],
			[ 'core/paragraph' ],
			[ 'core/button' ],
		];
	}

	/**
	 * Define allowed blocks for the editor
	 */
	public static function allowed_block_types( $allowed_block_types, $post ) {
		if ( $post->post_type !== self::MODEL_NAME ) return $allowed_block_types;

		return self::ALLOWED_BLOCK_TYPES;
	}
}
