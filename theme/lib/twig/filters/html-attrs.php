<?php

namespace SUPT\Twig;

use \Twig_SimpleFilter;

// Action & filter hooks
add_filter( 'timber/twig', '\SUPT\Twig\add_filter_attrs');

/**
 * Usage <div class="my-div" {{ attributes|attrs }}></div>
 * NOTE Do not forget to put a space before the helper
 *
 * Config:
 * attributes: { 'attr': true, 'attr2': 'value1', 'attr3': false, 'attr4': 0 }
 *
 * Result
 * <div class="my-div" attr attr2="value1" attr4="0"></div>
 */
function html_attrs( $data = [] ) {

	if ( !is_array($data) && empty($data) ) return null;

	$attrs = [];
	foreach ($data as $k => $val) {
		// vars
		$attribute = "" . $k;

		// skip if false
		if ($val === false) continue;

		// append value if anything but bool(true)
		if ($val !== true) $attribute .= "=\"" . $val . "\"";

		// add to results
		$attrs[] = $attribute;
	}

	// return
	return implode( ' ', $attrs );
}

function add_filter_attrs( $twig ) {
	$twig->addFilter( new Twig_SimpleFilter( 'attrs', '\SUPT\Twig\html_attrs' ) );
	return $twig;
};
