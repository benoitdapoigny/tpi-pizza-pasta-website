import './editor.scss';

import { registerBlockType } from '@wordpress/blocks';

import edit from './edit';

registerBlockType('supt/schedule', {
	title: 'Horaires',
	description: 'Ajouter et modifiez vos horaires d\'ouverture',
	icon: 'clock',
	category: 'common',
	attributes: {
		day1: { type: 'string' },
		day2: { type: 'string' },
		day3: { type: 'string' },
		day4: { type: 'string' },
		mealday1: { type: 'string' },
		mealday2: { type: 'string' },
		mealday3: { type: 'string' },
		mealday4: { type: 'string' },
	},
	keywords: [
		'horaires'
	],
	edit,
	save: () => null,
});
