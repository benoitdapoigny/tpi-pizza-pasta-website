<?php

namespace SUPT\Controllers\Footer;

use Timber\Menu;

/**
 * Action & Filter hooks
 */
add_action( 'timber/context', __NAMESPACE__.'\add_context' );


function add_context($context) {

	$NAME        = get_theme_mod( \SUPT\Customizer\Footer\FOOTER_RESTAURANT_NAME );
	$STREET      = get_theme_mod( \SUPT\Customizer\Footer\FOOTER_RESTAURANT_STREET );
	$CITY        = get_theme_mod( \SUPT\Customizer\Footer\FOOTER_RESTAURANT_CITY );
	$SOCIAL_TEXT = get_theme_mod( \SUPT\Customizer\Footer\FOOTER_SOCIAL_TEXT );
	$SOCIAL_LINK = get_theme_mod( \SUPT\Customizer\Footer\FOOTER_SOCIAL_LINK );
	$PHONE       = get_theme_mod( \SUPT\Customizer\Footer\FOOTER_PHONE );
	$EMAIL       = get_theme_mod( \SUPT\Customizer\Footer\FOOTER_EMAIL );

	$context['footer'] = [
		'modifiers' => [],
		'data' => [
			'left' => [
				$NAME,
				$STREET,
				$CITY
			],
			'right' => [
				"<a href='$SOCIAL_LINK' target='_blank'>$SOCIAL_TEXT</a>",
				$PHONE,
				"<a href='mailto:$EMAIL'>$EMAIL</a>"
			]
		]
	];

	return $context;
}
