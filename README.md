Pizza Pasta Website
============================================

Superhuit's Pizza Pasta theme for WordPress made with Fractal.build and the latest web technologies.

Installation
============

Requirements
---

* [nvm](https://github.com/creationix/nvm)
* [node](https://nodejs.org/) = lts/dubnium (v10.x)
* [yarn](https://yarnpkg.com/) >= 1.19.0
* [docker](https://www.docker.com/products/docker-engine) >= 1.12
* [docker-compose](https://docs.docker.com/compose/) >= 1.8
* [docker-sync](http://docker-sync.io/) >= 0.5
* [wp-cli](https://wp-cli.org) >= 2.4


Getting Started
===============

🚀 Setup & run
---

1. `nvm install` - Install and use the correct version of Node
2. `yarn install` - Install all dependencies
3. `yarn start` - Start styleguide and webpack hot-reload
  *ℹ️ NOTE: if you want to develop on wordpress with hmr but don't need the styleguide, run `yarn start:assets` instead*
4. `docker-sync start` - Start Docker sync which will create the volumes.
  *ℹ️ Wait until the command finishes creating the volume*
5. `docker-compose up -d` - Start WordPress with db, phpmyadmin and mailhog
6. `yarn bootstrap` - Run the bootstrap script to setup WordPress

> ℹ️ After setup, you will just need
>
> - `yarn start`
> - `docker-sync start`
> - `docker-compose up -d`
>
> …to have both the styleguide and wordpress with hot reload!

🔗 Project urls
---

* website: [http://localhost](http://localhost) <!-- Set to https when enable HTTPs -->
* phpmyadmin: [http://localhost:8181](http://localhost:8181)
* styleguide: [http://localhost:4000](http://localhost:4000)
<!-- * Mailhog: [http://localhost:8025](http://localhost:8025) TODO fix ssmt -->

> ℹ️ both the styleguide and wordpress connect to the webpack devserver for hot-reloaded assets on [http://localhost:3000](http://localhost:3000)

☝️ Helpful commands
---

* `yarn build` - build the theme assets for production
* `yarn build:fractal` - build a static version of the styleguide (needed only if you want to host it somewhere, i.e. on gitlab pages)
* `yarn component` - generate a frontend component
* `yarn sass` - update the scss _loader.scss file with existing components
* `yarn logs` - show the wordpress logs (PHP)

🔐 HTTPS Config
---

To enable SSL (HTTPS) for the stack, you will need to uncomment some configurations and
setup your computer to trust the self-signed certificate.

- Search for "enable HTTPs" in the stack, and uncomment/update the configuration.

To trust the self-signed certificate, you have to add the Root certificate to your trusted certificates.

1. Open _Keychain Access_
2. Select **System** keychains and **Certificates** Category.
3. Import `./server/ssl/rootCA.pem` (File > Import Items )
4. Double click the imported certificate and change the “When using this certificate:” dropdown to **Always Trust** in the Trust section.

![](./server/ssl/keychain.png)


Development
========

Auto-color-injection for documentation
---
 Colors are automatically injected into documentation as long as
 we keep the following file structure:

	/**
	 * {Name of the color category} color variables.
	 */ (no empty lines)
	${name of the color variable}: {color value}

Auto populating icons in documentation
---
Icon doc page is automatically populated from the icons folder `./styleguide/assets/icons`.

Rendering child components
---
- Twig syntax: `{% include 'path/to/component.twig'%}`
(make sure your path starts at the atoms, molecules or organisms level, like this:
`{% include 'molecules/address-list/address-list.twig' %}`)

TODO
---

Document:
- project structure
- configuration files
- database import/export
- templating (image `responsive` filter, etc)
- deploy

Improve:
- plugins management with composer? https://roots.io/using-composer-with-wordpress/ and https://wpackagist.org/ /!\ problem: wpml will scan strings in vendor folder… not recommended.


Advanced
========

Debug
-----

You can enable WordPress & php debugs tools to help develop.

### WordPress
Set 1 to `WORDPRESS_DEBUG` environment variable in `docker-compose.yml` file.

For this modification to by applied, the container needs to be rebuild:

```
docker-compose down && docker-compose build && docker-compose up -d
```


Theming
-------

### Templating
We use [Timber](http://upstatement.com/timber/) to handle the templating.
The theme "pizza-pasta" is based on [Timber Starter Theme](https://github.com/timber/starter-theme).

### Block editor (aka. Gutenberg)
There are 2 ways to create custom components for Gutenberg:

#### Native blocks

- requires React, gutenberg knowledge, and more time than an ACF block
- great experience possibilities

**How to**

See examples in `./theme/lib/blocks/**/*.js`.
Read docs in https://www.notion.so/superhuit/Getting-up-to-speed-with-Gutenberg-85c8605860b3431e9b406cd1d5f8ba3b

#### ACF blocks

- quick to do, perfect for prototypes
- not as good as a native gutenberg block for editorial experience

**How to**
We use [ACF >= v5.8](https://www.advancedcustomfields.com/blog/acf-5-8-introducing-acf-blocks-for-gutenberg/) to create specific field groups to be used in custom blocks.

1. Register a custom block. Check README in file `./theme/lib/blocks/block-model/block-model.php`
2. Create an ACF field group and set the location rule to `Block` with your custom block
3. Style your block in the editor. Check README in file `./theme/lib/blocks/block-model/block-model.scss`
4. Adapt/customize your block js. Check README in file `./theme/lib/blocks/block-model/block-model.js`
5. Enjoy your custom block.


WordPress tools
---------------
### WP-CLI
You can use all the power of **wp-cli** in your terminal by running any command with `wp @local`, for example:
`wp @local user list`

_See `wp-cli.yml` for the aliases list_

#### Alias to remote (staging/prod)

For the alias to work on a remote server, the wp-cli bin file should be runnable by ssh. To do that, install wp-cli on the remote server, and put the bin file in the `~/bin/` folder.

Then add the alias in the `wp-cli.yml` file.

### Import all ACF configs
* `wp @local package install git@github.com:superhuit-ch/wp-cli-acf-json.git` - if not already installed
* `wp @local acf-json sync --all_sites` - imports all the JSON ACF configs in the database

_Official docs: [https://wp-cli.org/commands/](https://wp-cli.org/commands/)_


ACF - Advanced Custom Fields
----------------------------
We use ACF to add custom fields on post types (& elsewhere).

To version, share and deploy the ACF fields we create we use the feature called [Synchronized JSON](https://www.advancedcustomfields.com/resources/synchronized-json/).
Basically, it means we have nothing to take care of manually. A typical flow is:

### Add/edit fields
1. Edit the fields as you want in the backend
2. They are automatically saved in the folder `theme/acf-json`, nothing to do here
3. When you commit, don't forget to commit these files as well

### Sync all fields
By default, when the acf-json folder is present, WordPress loads the ACF config from there, so it's always in sync.
If you want to edit them in the backend though, wp will show "Available for sync". You can click manually "sync", or easier:

* `wp @local acf-json sync --all_sites` - import all the JSON ACF configs in the database

Custom Post Types
-----------------------

### Add a post type
1. Register a new post type class that extends the class `\SUPT\Model` located in `theme/lib/post-types/Model.php`
2. Include it in the loader file: `theme/lib/_loader.php`

Deploy
======

Deploiement is done by `deploy-to-remote.sh` script. Update variables in order for your deploiement to succeed.
