import { Component, Fragment } from '@wordpress/element';
import { Button } from '@wordpress/components';
import { RichText } from '@wordpress/block-editor';

const DEFAULT_ATTRS = {
	title: '',
	list: [
		{ name: '', description: '', price: '' }
	],
	informations: [
		{ title: '', list: [ '' ] }
	]
};

export default class MenuSection extends Component {
	constructor( ...args ) {
		super( ...args );

		this.state = {
			attrs: { ...DEFAULT_ATTRS, ...this.props.attrs }
		};
	}

	componentDidMount() {
	}

	onChange(attrs, i) {
		const newSection = [...(this.props.sections || [])];

		if (typeof i === 'undefined') {
			newSection[this.props.index] = Object.assign(newSection[this.props.index], attrs);
		} else {
			newSection[this.props.index].list[i] = Object.assign(newSection[this.props.index].list[i], attrs);
		}

		this.props.setAttributes({ sections: newSection });
	}

	updateInformations(attrs, section_index, informations_index, item_index) {
		const newSection = [...(this.props.sections || [])];

		if (typeof item_index === 'undefined') {
			newSection[section_index].informations[informations_index] = Object.assign(newSection[section_index].informations[informations_index], attrs);
		} else {
			newSection[section_index].informations[informations_index].list[item_index] = attrs.text;
		}
		console.log(attrs.text);
		console.log(newSection);
		console.log(newSection[section_index].informations[informations_index].list[item_index]);
		this.props.setAttributes({ sections: newSection });
	}

	render() {
		const { attrs, setAttributes, isSelected } = this.props;

		return (
			<Fragment>
				<div class="menu-section" data-menu-section>
					<div class="menu-section__header">
						<h2><RichText
								allowedFormats={ [] }
								value={ attrs.title }
								onChange={ (title) => this.onChange({ title }) }
								placeholder={ 'Titre de la section, ex: Burger, Salades...' }
								keepPlaceholderOnFocus
							/></h2>
							<Button
								isSecondary
								isSmall={ true }
								onClick={ () => { this.props.removeRow() } }
							>{'Supprimer la section'}</Button>
					</div>
					<ul class="menu-section__list">
						{ attrs.list.map( ( item, i ) => (
							<li class="menu-section__item" key={ i }>
								<span class="menu-section__item-text">
								<RichText
									allowedFormats={ [] }
									value={ item.name }
									onChange={ (name) => this.onChange({ name }, i) }
									placeholder={ 'Nom du plat, ex: Salade verte...' }
									keepPlaceholderOnFocus
								/><span>
									<RichText
									allowedFormats={ [] }
									value={ item.description }
									onChange={ (description) => this.onChange({ description }, i) }
									placeholder={ 'Description du plat (optionel)' }
									keepPlaceholderOnFocus
								/></span></span>
								<span class="menu-section__item-price">
								<RichText
									allowedFormats={ [] }
									value={ item.price }
									onChange={ (price) => this.onChange({ price }, i) }
									placeholder={ 'Prix du plat, ex: 16.50' }
									keepPlaceholderOnFocus
								/></span>
							</li>
						) ) }
						<div class="controls-buttons">
							<Button
								isSecondary
								isSmall={ true }
								onClick={ () => {
									console.log('menu section click add row');

									this.props.addRow()
								}}
							>{'Ajouter une ligne'}</Button>
							{ !attrs.informations &&
								<Button
									isSecondary
									isSmall={ true }
									onClick={ () => {
										const newSections = [...(this.props.sections || [])];
										newSections[this.props.index].informations = [
											{ title: '', list: [ '' ] }
										];
										this.props.setAttributes({ sections: newSections });
									}}
								>{'Ajouter des informations'}</Button>
							}
						</div>
						{ attrs.informations &&
						<div class="menu-section__informations">
							{ attrs.informations.map( (item, section_i) => (
							<Fragment key={ section_i }>
								<div class="menu-section__informations-item">
									<span class="menu-section__informations-title"><RichText
										allowedFormats={ [] }
										value={ item.title }
										onChange={ (title) => this.updateInformations({ title }, this.props.index, section_i) }
										placeholder={ 'Titre, ex: Sauces, provenance de nos viandes' }
										keepPlaceholderOnFocus
									/></span>
									<ul class="menu-section__informations-list">
										{ item.list.map( (item, item_i) => (
											<li key={ item_i }><RichText
											allowedFormats={ [] }
											value={ item.text }
											onChange={ (text) => this.updateInformations({ text }, this.props.index, section_i, item_i) }
											placeholder={ 'ex: Poivre vert: 4.50' }
											keepPlaceholderOnFocus
										/>
										</li>
										) ) }
									</ul>
									<Button
										className={ 'menu-section__informations-button' }
										isSecondary
										isSmall={ true }
										onClick={ () => {
											const newSections = [...(this.props.sections || [])];
											newSections[this.props.index].informations[section_i].list.push( '' );
											this.props.setAttributes({ sections: newSections });
										} }
									>{'Ajouter une info'}</Button>
								</div>
							</Fragment>
							) ) }
							<Button
								className={ 'menu-section__informations-button' }
								isPrimary
								isSmall={ true }
								onClick={ () => {
									const newSections = [...(this.props.sections || [])];
									newSections[this.props.index].informations.push( { title: '', list: [ { text: '' } ] } );
									this.props.setAttributes({ sections: newSections });
								} }
							>{'Ajouter une section d\'info'}</Button>
						</div>
						}
					</ul>
				</div>
			</Fragment>
		);
	}
}
