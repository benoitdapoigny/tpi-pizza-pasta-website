<?php

namespace SUPT\Admin\Timmy;

// TIMMY CONFIGURATION
// Read more about it here: https://github.com/mindkomm/timmy#getting-startedpreparations

// enable content responsive images
// new Timmy\Responsive_Content_Images(); // TODO: figure out why it's not working (images in article content are not responsive)

// reset image sizes options
const TIMMY_RESET_OPTIONS = [
	'thumbnail_size_w',
	'thumbnail_size_h',
	'medium_size_w',
	'medium_size_h',
	'medium_large_size_h',
	'medium_large_size_w',
	'large_size_w',
	'large_size_h'
];

/**
 * Action & filter hooks
 */
add_filter( 'timmy/sizes', __NAMESPACE__.'\timmy_sizes' );
add_filter( 'timmy/oversize', __NAMESPACE__.'\timmy_oversize' );
add_filter( 'image_size_names_choose', __NAMESPACE__.'\image_size_names_choose', 11 );

foreach ( TIMMY_RESET_OPTIONS as $opt ) {
	add_filter( "pre_option_{$opt}", '__return_zero' );
}

// reset thumbnail size
set_post_thumbnail_size( 0, 0 );

/**
 * never set the style_attr for an image
 */
function timmy_oversize( $oversize ) {
	$oversize['style_attr'] = false;
	return $oversize;
}

/**
 * make the full size unavailable when an image is inserted into the WordPress Editor
 */
function image_size_names_choose( $sizes ) {
	unset( $sizes['full'] );
	return $sizes;
}

/**
 * Configure sizes
 * NOTE: these are generated on image load as well as on page load if not existing
 * REMARK: please read this before adding unnecessary sizes: https://github.com/mindkomm/timmy#using-an-image-size-array-instead-of-a-key
 */
function timmy_sizes( $sizes ) {

	$card_image_ratio = 0.575;
	$gallery_img_ratio = 9/16;

	return [

		//---------------
		// REQUIRED SIZES
		//---------------

		// admin: used to preview images in the admin
		'admin' => [
			'resize' => [ 300 ],
			'srcset' => [],
			'sizes' => '',
			'name' => 'Admin preview',
			'post_types' => [ 'all' ],
			'show_in_ui' => true,
		],

		// thumbnail: used in admin?
		// see https://github.com/mindkomm/timmy/blob/master/docs/image-configuration.md#the-thumbnail-key
		'thumbnail' => [
			'resize' => [ 300, 300 ],
			'name' => 'Thumbnail',
			'post_types' => [ 'all' ],
			'show_in_ui' => false,
		],

		// Used in Media Library
		'medium' => [
			'resize' => [ 300 ],
			'name' => 'Medium',
			'post_types' => [ 'all' ],
			'show_in_ui' => true,
		],

		// large: used by Yoast for og:image
		// see https://github.com/mindkomm/timmy/blob/master/docs/image-configuration.md#the-thumbnail-key
		'large' => [
			'resize' => [ 2000 ],
			'name' => 'Large',
			'post_types' => [ 'all' ],
			'show_in_ui' => false,
		],

		//------------------
		// ADDITIONNAL SIZES
		//------------------

		// Banner Article
		'banner-article' => [
			'resize' => [ 1800 ],
			'srcset' => [ [375], [512], [768], [1024], [1280], [1500], [1800], [2500] ],
			'sizes' => '100vw',
			'name' => 'Banner Home',
			'post_types' => [ 'post' ],
			'show_in_ui' => false,
		],

		// Banner home (10 columns of the grid)
		'banner-home' => [
			'resize' => [ 1280 ],
			'srcset' => [ [375], [512], [768], [1024], [1280], [1500], [1800] ],
			'sizes' => '(max-width: 1024px) 100vw, 1100px',
			'name' => 'Banner Home',
			'post_types' => [ 'all' ],
			'show_in_ui' => false,
		],

		// Image gallery grid (force 16/9 ratio)
		'gallery-grid' => [
			'resize' => [ 1024, (1024 * $gallery_img_ratio) ],
			'srcset' => [ [375, (375 * $gallery_img_ratio)], [512, (512 * $gallery_img_ratio)], [768, (768 * $gallery_img_ratio)], [1024, (1024 * $gallery_img_ratio)], [1280, (1280 * $gallery_img_ratio)], [1500, (1500 * $gallery_img_ratio)] ],
			'sizes' => '(max-width: 767px) 100vw, (max-width: 1023px) 75vw, (max-width: 1679px) 60vw, 768px',
			'name' => 'Gallery grid',
			'post_types' => [ 'all' ],
			'show_in_ui' => false,
		],

		// Image gallery small thumbnail (force 16/9 ratio)
		'gallery-thumb' => [
			'resize' => [ 400, (400 * $gallery_img_ratio) ],
			'name' => 'Gallery thumbnail',
			'post_types' => [ 'all' ],
			'show_in_ui' => false,
		],

		// cards Small (force the ratio)
		'card-small' => [
			'resize' => [ 512, (512 * $card_image_ratio) ],
			'srcset' => [ [375, (375 * $card_image_ratio)], [512, (512 * $card_image_ratio)], [768, (768 * $card_image_ratio)], [1024, (1024 * $card_image_ratio)] ],
			'sizes' => '(max-width: 767px) 100vw, (max-width: 1023px) 50vw, (max-width: 1679px) 33vw, 420px',
			'name' => 'Cards small',
			'post_types' => [ 'all' ],
			'show_in_ui' => false,
		],

		// Cards (force the ratio)
		'card' => [
			'resize' => [ 768, (768 * $card_image_ratio) ],
			'srcset' => [ [375, (375 * $card_image_ratio)], [512, (512 * $card_image_ratio)], [768, (768 * $card_image_ratio)], [1024, (1024 * $card_image_ratio)], [1280, (1280 * $card_image_ratio)] ],
			'sizes' => '(max-width: 767px) 100vw, (max-width: 1679px) 50vw, 650px',
			'name' => 'Cards',
			'post_types' => [ 'all' ],
			'show_in_ui' => false,
		],

		// Big card (force the ratio)
		'card-big' => [
			'resize' => [ 1024, (1024 * $card_image_ratio) ],
			'srcset' => [ [375, (375 * $card_image_ratio)], [512, (512 * $card_image_ratio)], [768, (768 * $card_image_ratio)], [1024, (1024 * $card_image_ratio)], [1280, (1280 * $card_image_ratio)], [1500, (1500 * $card_image_ratio)] ],
			'sizes' => '(max-width: 1023px) 100vw, (max-width: 1679px) 60vw, 768px',
			'name' => 'Cards big',
			'post_types' => [ 'all' ],
			'show_in_ui' => false,
		],

		// Content
		'content' => [
			'resize' => [ 1024 ],
			'srcset' => [ [375], [512], [768], [1024], [1280], [1500], [1800], [2500] ],
			'sizes' => '(max-width: 767px) 100vw, (max-width: 1023px) 75vw, (max-width: 1679px) 60vw, 768px',
			'name' => 'Content',
			'post_types' => [ 'all' ],
			'show_in_ui' => false,
		],

	];
}



