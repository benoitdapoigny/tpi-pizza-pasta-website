import { debounce } from 'lodash-es';

/**
 * This helper is used to
 * measure the scroll percentage
 * with analytics.
 */

/**
 * Get a new status object.
 */
function getStatusObject() {
	return [0, 25, 50, 75, 100].map((p) => {
		if (p === 100) {
			return { level: (p - 2), label: p, completed: false };
		}
		return { level: p, label: p, completed: false };
	})
}

/**
 * Get scroll percentage.
 */
function getScrollPercent() {
	let s = window.pageYOffset,
		d = document.documentElement.scrollHeight,
		c = window.innerHeight;

		return Math.ceil((s / (d-c)) * 100);
}


/**
 * Scroll event handler.
 */
function scrollHandler(status, sendHandler) {
	let percentage = getScrollPercent();
	status.forEach((s) => {
		if ((percentage >= s.level) && !s.completed) {
			sendHandler([s.label, '%'].join(''));
			s.completed = true;

			// Unbind to scroll event
			if ( s.level >= 98 ) {
				window.removeEventListener( 'scroll', scrollHandlerDebounced );
			}
		}
	});
}

let scrollHandlerDebounced;
export default function() {

	if ( window.ga === undefined ) {
		return;
	}

	let scroll_status = getStatusObject();

	let sendEvent = (label) => {
		window.ga('send', 'event', 'scroll', label);
	};

	scrollHandlerDebounced = debounce( scrollHandler.bind(null, scroll_status, sendEvent), 250 );

	window.addEventListener( 'scroll', scrollHandlerDebounced );
	setTimeout(() => { scrollHandler(scroll_status, sendEvent); }, 2000);

}
