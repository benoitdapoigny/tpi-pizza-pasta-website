<?php

namespace SUPT\Taxonomies;

class Category {

	const TAXONOMY_NAME = 'category';
	const LINKED_POST_TYPES = [ 'Post' ];

	const COLUMN_CUSTOM = 'my-custom-column';

	/**
	 * Register all that is needed for the taxonomy
	 */
	public static function init() {
		add_action( 'init', [__CLASS__, 'register']);

	}

	/**
	 * Proper taxonomy registration
	 */
	public static function register() {
		// register_taxonomy( self::TAXONOMY_NAME, self::LINKED_POST_TYPES, $args );
	}
}
