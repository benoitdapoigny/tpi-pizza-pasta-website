/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Weekmenu',
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			date: 'Du 21 au 25 mai 2018',
			list: [
				{
					modifiers: ['menu-section--weekmenu'],
					data: {
						title: 'Proposition de la semaine',
						list: [
							{
								name: 'Entrecôte Parisienne de boeuf, beurre maison, pomme fites, salade',
								description: null,
								price: '23.00'
							},
							{
								name: 'La chasse, spätzli et tout le tsoin tsoin',
								description: null,
								price: '28.00'
							},
						],
						informations: null,
						holiday: null
					},
				},
				{
					modifiers: ['menu-section--weekmenu'],
					data: {
						title: 'Lundi',
						list: [
							{
								name: 'Entrecôte Parisienne de boeuf, beurre maison, pomme fites, salade',
								description: null,
								price: '23.00'
							},
							{
								name: 'La chasse, spätzli et tout le tsoin tsoin',
								description: null,
								price: '28.00'
							},
						],
						informations: null,
						isHoliday: true,
						holiday: 'Lundi de pentecôte'
					},
				},
				{
					modifiers: ['menu-section--weekmenu'],
					data: {
						title: 'Mardi',
						list: [
							{
								name: 'Entrecôte Parisienne de boeuf, beurre maison, pomme fites, salade',
								description: null,
								price: '23.00'
							},
							{
								name: 'La chasse, spätzli et tout le tsoin tsoin',
								description: null,
								price: '28.00'
							},
							{
								name: 'Salade italienne',
								description: 'Tomates, mozarellade buffala, ruccola et jambon cru',
								price: '17.50'
							},
							{
								name: 'Pizza orientale',
								description: 'Sauce tomate, mozarella, merguez, poivrons, oeuf cru, origano',
								price: '17.00'
							},
						],
						informations: null,
						holiday: null,
						isHoliday: false,
					},
				}
			]
		},
	},
};
