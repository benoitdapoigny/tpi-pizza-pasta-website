import { Component, Fragment } from '@wordpress/element';
import { RichText } from '@wordpress/block-editor';
import { select, dispatch } from '@wordpress/data';

export default class Contact extends Component {
	constructor(...args) {
		super(...args);

		this.state = {
		};
	}

	componentDidMount() {
		const { attributes: attrs } = this.props;
	}

	onChange(attrs) {
		dispatch('core/editor').editPost({ meta: { contact: Object.assign({}, this.props.attributes, attrs) } });
		this.props.setAttributes( attrs );
	}

	render() {
		const { attributes: attrs, setAttributes, isSelected } = this.props;

		return (
			<Fragment>
				<div class="contact">
					<div class="contact__section">
						<h1 class="contact__title">Contact</h1>
						<div class="contact__content">
							<RichText
								className="contact__content-item"
								allowedFormats={ [] }
								value={ attrs.phone }
								onChange={ ( phone ) => this.onChange({ phone }) }
								placeholder={ 'Entrez votre numéro de téléphone' }
								keepPlaceholderOnFocus
							/>
							<RichText
								className="contact__content-item"
								allowedFormats={ [] }
								value={ attrs.email }
								onChange={ ( email ) => this.onChange({ email }) }
								placeholder={ 'Entrez votre adresse e-mail' }
								keepPlaceholderOnFocus
							/>
						</div>
					</div>
					<div class="contact__section">
						<h1 class="contact__title">Adresse</h1>
						<div class="contact__content">
							<RichText
								className="contact__content-item"
								allowedFormats={ [] }
								value={ attrs.name }
								onChange={ ( name ) => this.onChange({ name }) }
								placeholder={ 'Entrez le nom de votre restaurant' }
								keepPlaceholderOnFocus
							/>
							<RichText
								className="contact__content-item"
								allowedFormats={ [] }
								value={ attrs.street }
								onChange={ ( street ) => this.onChange({ street }) }
								placeholder={ 'Entrez l\'adresse de votre rue' }
								keepPlaceholderOnFocus
							/>
							<RichText
								className="contact__content-item"
								allowedFormats={ [] }
								value={ attrs.city }
								onChange={ ( city ) => this.onChange({ city }) }
								placeholder={ 'Entrez votre NPA / Ville' }
								keepPlaceholderOnFocus
							/>
						</div>
					</div>
				</div>
			</Fragment>
		);
	}
}
