import './editor.scss';

import { registerBlockType } from '@wordpress/blocks';

import edit from './edit';

registerBlockType('supt/menu', {
	title: 'Carte',
	description: 'Modifier les différents menus présents sur la carte.',
	icon: 'excerpt-view',
	category: 'common',
	attributes: {
		sections: { type: 'array' }
	},
	keywords: [
		'carte',
	],
	edit,
	save: () => null,
});
