<?php

namespace SUPT\Helper;

use SUPT\Taxonomies\Rubrique;

/**
 * Retrieve the first taxonomy term
 *
 * @param int|WP_Post $post Post which the term is linked to
 * @param string $taxonomy Optional. The taxonomy of the term. Default: category
 * @return null|WP_Term
 */
function get_single_taxonomy_term($post, $taxonomy = Rubrique::TAXONOMY_NAME) {
	if ( is_null($post) ) return null;

	return get_the_terms($post, $taxonomy)[0] ?? null;
}
