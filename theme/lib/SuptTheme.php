<?php

use function SUPT\test_url;

class SuptTheme extends TimberSite {

	function __construct() {

		$this->dev_server_url = (is_ssl() ? 'https' : 'http').'://localhost:3000';
		$this->docker_url = (is_ssl() ? 'https' : 'http').'://host.docker.internal:3000';

		// Theme supports
		// -> more info: https://developer.wordpress.org/reference/functions/add_theme_support/
		// add_theme_support( 'custom-logo' );
		add_theme_support( 'post-thumbnails', [ 'post' ] );
		add_theme_support( 'menus' );
		add_theme_support( 'editor-styles' );
		// add_theme_support( 'align-wide' );
		// Add custom logo support to theme
    // add_theme_support( 'custom-logo', array(
    //   'height'      => 150,
    //   'width'       => 150,
		// ) );

		// Disable some theme features
		add_theme_support( 'disable-custom-colors' );
		add_theme_support( 'editor-color-palette', array() );
		add_theme_support( 'disable-drop-cap' ); // this does not seems to work
		add_theme_support( 'disable-font-sizes' ); // this does not seems to work
		add_theme_support( 'disable-custom-font-sizes' );

		// Filters
		// -> more info: https://developer.wordpress.org/reference/functions/add_filter/
		add_filter( 'body_class', array( $this, 'body_class' ), 30 );
		add_filter( 'get_twig', array( $this, 'add_fn_to_twig' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ), -1 );

		// Actions
		// -> more info: https://developer.wordpress.org/reference/functions/add_action/
		add_action( 'init', array( $this, 'register_assets' ) );
		add_action( 'init', array( $this, 'unregister_cookie_law_plugin_styles' ), 20 );
		add_action( 'customize_register', array( $this, 'register_customizer_settings' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_assets' ) );
		add_action( 'enqueue_block_editor_assets',  array( $this, 'enqueue_editor_assets' ) );

		// Enqueue specific style for the block editor
		add_editor_style( 'style-editor.css' );
	}

	/**
	 * Filters the list of CSS body class names for the current post or page.
	 *
	 * @param string[] $classes An array of body class names.
	 *
	 * Added class:
	 *  - page
	 *  - page-single page-single-xyz (xyz = post type)
	 *  - page-page (for Pages)
	 *  - page-pwd (for Pages with password)
	 *  - page-archive page-archive-xyz (xyz = post type)
	 *  - page-404
	 *  - page-search
	 *  - page-privacy
	 *
	 * Removed classes:
	 *  - page-template - page-template-template-xyz - page-template-template-xyz-php - page-id-xxx
	 *  - post-template-default - event-template-default - postid-xxx
	 *  - product-template-default
	 *  - single-post - single-event - single-format-standard
	 *  - wp-custom-logo
	 *  - no-customize-support
	 */
	function body_class( $classes ) {

		$classes = array_filter($classes, function($c) {
			return !preg_match("/(privacy|product|single|page|post|event|tax|term|archive|wp-custom-logo|no-customize-support|home|blog|search|error|theme)[^\s]*/", $c);
		});

		$classes[] = 'page';

		if ( is_404() )                 $classes[] = 'page-404';
		else if ( is_search() )         $classes[] = 'page-search';
		else if ( is_single() )         $classes[] = 'page-single page-single-'. sanitize_html_class( get_post_type() );
		else if ( is_singular()
			&& post_password_required() ) $classes[] = 'page-pwd';
		else if ( is_archive()
			|| is_home() )                $classes[] = 'page-archive page-archive-'. sanitize_html_class( get_post_type() );
		else                            $classes[] = 'page-page';

		if ( is_privacy_policy() )      $classes[] = 'page-privacy';

		return $classes;
	}


	function add_to_context( $context ) {
		// Inception
		$context['site'] = $this;

		// Theme base URI
		$context['theme_uri'] = THEME_URI;

		$context['menu'] = new Timber\Menu( 'primary' );

		return $context;
	}

	function add_fn_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );

		// Temp: this filter looks like it's not registered properly for gutenberg acf blocks
		// TODO: check check
		$twig->addFilter( new Twig_SimpleFilter( 'get_timber_image_src', 'get_timber_image_src' ) );

		return $twig;
	}


	function register_assets() {
		// vars
		$paths = [];
		$manifest_path = get_template_directory() . '/static/manifest.json';
		$manifest_exists = file_exists($manifest_path);

		// bail early if no manifest (except if we are in dev mode)
		if (!$manifest_exists && !WP_DEBUG) {
			wp_die('Please build the theme with webpack (manifest.json cannot be found).');
		}

		// dev: if webpack devserver loaded, use it to benefit from hmr
		if (WP_DEBUG && test_url("{$this->docker_url}/bundle.css")) {
			$paths['css'] = "{$this->dev_server_url}/bundle.css";
			$paths['js']  = "{$this->dev_server_url}/bundle.js";
		}

		// prod: get assets paths from
		elseif ($manifest_exists) {
			$manifest = json_decode(file_get_contents($manifest_path, true));
			// TODO: serve legacy if old browser, see https://www.smashingmagazine.com/2018/10/smart-bundling-legacy-code-browsers/
			$paths['css'] = $manifest->{'bundle.css'};
			$paths['js']  = $manifest->{'bundle.js'};
		}

		// dev: no manifest and no webpack devserver, throw an error
		else {
			wp_die('No assets could be found. Since you are in dev mode, either start webpack devserver with `yarn start:assets` or build the theme with `yarn build`.');
		}

		// register styles
		wp_register_style( 'supt-styles', $paths['css'], null, null, 'screen' );
		wp_register_style( 'supt-style-admin-bar', THEME_URI .'/style-admin-bar.css', false, null, 'screen' );

		// register scripts
		if (!is_admin()) {
			wp_register_script( 'supt-scripts', $paths['js'], null, null, true );
		}
		else {
			wp_register_script( 'supt-scripts', $paths['js'], ['wp-element', 'wp-i18n', 'wp-editor', 'wp-hooks', 'wp-blocks'], null, true );
		}
	}

	function unregister_cookie_law_plugin_styles() {
		wp_deregister_style( 'cookie-law-consent-style' );
	}


	function enqueue_assets() {
		// Only front-end
		if ( !is_admin() ) {
			wp_enqueue_style( 'supt-styles' );
			// wp_enqueue_script( 'supt-modernizr');
			wp_enqueue_script( 'supt-scripts');

			if ( is_user_logged_in() ) wp_enqueue_style( 'supt-style-admin-bar' );

			// don't load jquery
			if ( !WP_DEBUG && !is_plugin_active( 'query-monitor/query-monitor.php' )) wp_deregister_script('jquery');

			// Remove default WP block styles in FE
			wp_dequeue_style( 'wp-block-library' );
			wp_dequeue_style( 'wp-block-library-theme' );
		}
	}

	function enqueue_admin_assets() {
		wp_enqueue_style( 'admin-style', THEME_URI . '/style-admin.css', false, THEME_VERSION );
	}

	function enqueue_editor_assets() {
		// vars
		$paths = [];
		$manifest_path = get_template_directory() . '/static/manifest.json';
		$manifest_exists = file_exists($manifest_path);

		// bail early if no manifest (except if we are in dev mode)
		if (!$manifest_exists && !WP_DEBUG) {
			wp_die('Please build the theme with webpack (manifest.json cannot be found).');
		}

		// dev: if webpack devserver loaded, use it to benefit from hmr
		if (WP_DEBUG && test_url("{$this->docker_url}/admin.css")) {
			$paths['css'] = "{$this->dev_server_url}/admin.css";
			$paths['js']  = "{$this->dev_server_url}/admin.js";
		}

		// prod: get assets paths from
		elseif ($manifest_exists) {
			$manifest = json_decode(file_get_contents($manifest_path, true));
			$paths['css'] = $manifest->{'admin.css'};
			$paths['js']  = $manifest->{'admin.js'};
		}

		// dev: no manifest and no webpack devserver, throw an error
		else {
			wp_die('No assets could be found. Since you are in dev mode, either start webpack devserver with `yarn start:assets` or build the theme with `yarn build`.');
		}

		// register scripts
		wp_enqueue_style( 'supt-editor-style', $paths['css'], ['wp-editor'], null );
		wp_register_script( 'supt-editor-script', $paths['js'], [
			'wp-dom-ready',
			'wp-edit-post',
			'wp-hooks',
			'wp-components',
			'wp-blocks',
			'wp-element',
			'wp-data',
			'wp-date',
			'wp-i18n',
			'wp-api-fetch'
		], null, true );

		wp_localize_script( 'supt-editor-script', 'supt', [
			'theme_uri' => THEME_URI,
			'icons' => [
				// 'cta' => get_icon_group('card-cta'),
			],
		]);
		wp_enqueue_script( 'supt-editor-script' );
	}
}
