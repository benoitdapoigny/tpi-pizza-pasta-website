/**
 * Loads custom Post Type plugins
 */
import './post-types/_loader';

/**
 * Loads custom Taxonomy plugins
 */
import './taxonomies/_loader';

/**
 * Load custom Block Editor Blocks
 */
import './blocks/blocks-loader';

