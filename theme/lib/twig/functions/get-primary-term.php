<?php

namespace SUPT;

use \Timber\Term;
use \WPSEO_Primary_Term;
use \Twig_SimpleFunction;

/**
 * Return Yoast primary term if available,
 * or first term taxonomy
 *
 * @param		int			$post_id		The post to retrieve the Term from.
 * @param		string	$taxonomy		Optional. Default: category. The Taxonomy of the term.
 * @return	WP_Term							The primary term or first in the list.
 */
function get_primary_term( $post_id, $taxonomy = 'category', $return_id = false ) {
	if ( class_exists('WPSEO_Primary_Term') ) {
		$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post_id );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$term = get_term( $wpseo_primary_term );
	}

	// Default to first category (not Yoast) if an error is returned
	if ( !isset($term) || is_wp_error($term) ) {
		$term = get_the_terms( $post_id, $taxonomy )[0];
	}

	return $return_id ? $term->term_id : new \Timber\Term($term);
}


// Add the function to Twig
add_filter( 'get_twig', function( $twig ) {
	$twig->addFunction( new Twig_SimpleFunction( 'get_primary_term', function($post_id, $taxonomy = 'category') {
		$term_id = get_primary_term($post_id, $taxonomy, true);
		return new \Timber\Term($term_id);
	}) );
	return $twig;
});
