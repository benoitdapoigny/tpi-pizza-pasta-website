<?php

namespace SUPT\Admin;

/**
 * ADMIN
 * =====
 *
 * Adding or altering features in the admin
 *
 * Examples:
 * - hide a feature we don't use
 * - rename a default menu label
 * - customize the wysiwyg editor
 * - add a "help" section
 */

require_once __DIR__.'/acf-custom-metabox-title.php';
require_once __DIR__.'/customize-tinymce.php';
require_once __DIR__.'/menu-locations.php';
require_once __DIR__.'/timmy-config.php';
