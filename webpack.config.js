/**
 * README
 * ======
 *
 * Webpack is used for:
 *
 * DEV
 * ---
 * - uses devServer which builds assets in memory and
 * - serves assets on http|https://localhost:3000
 *
 * PROD
 * ---
 * - builds and optimises assets in ./theme/static
 * - fingerprints assets to invalidate browser caching (i.e. build.37862af.css)
 * - outputs a manifest.json file which can be used by wordpress to serve the correct files
 */

const path = require('path');

const { PATHS, HOST, PORT, SCHEME, THEME_NAME, HTTPS } = require("./env.config");
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const ManifestPlugin = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
// const DependencyExtractionWebpackPlugin = require( '@wordpress/dependency-extraction-webpack-plugin' );

const DEV = process.env.NODE_ENV === 'development';
const PUBLIC_PATH = DEV ? `${SCHEME}://${HOST}:${PORT}/` : `/wp-content/themes/${THEME_NAME}/static/`;
const isModern = process.env.BROWSERSLIST_ENV === 'modern';

const camelCaseDash = string => string.replace( /-([a-z])/g, ( match, letter ) => letter.toUpperCase() );
const wplib = [
	'a11y',
	'api-fetch',
	'blob',
	'block-editor',
	'blocks',
	'components',
	'compose',
	'data',
	'date',
	'dom-ready',
	'edit-post',
	'editor',
	'element',
	'hooks',
	'html-entities',
	'i18n',
	'icons',
	'keycodes',
	'plugins',
	'plugins',
	'rich-text',
	'url',
	'utils',
	'viewport'
];

module.exports = {
	stats: 'errors-only',
	bail: !DEV,
	mode: DEV ? 'development' : 'production',
	// We generate sourcemaps in production. This is slow but gives good results.
	// You can exclude the *.map files from the build during deployment.
	// See https://webpack.js.org/configuration/devtool/
	devtool: DEV ? 'cheap-eval-source-map' : 'source-map',
	entry: {
		bundle: [
			isModern ?
				PATHS.polyfillsModernJS :
				PATHS.polyfillsLegacyJS,
			PATHS.indexJs
		],
		admin: PATHS.adminJs,
	},
	output: {
		path: isModern ? PATHS.build : path.join(PATHS.build, 'legacy'),
		publicPath: PUBLIC_PATH,
		filename: DEV ? '[name].js' : '[name].[hash:8].js',
	},
	// require existing gutenberg dependencies instead of bundling duplicates
	// see https://www.cssigniter.com/importing-gutenberg-core-wordpress-libraries-es-modules-blocks/
	externals: wplib.reduce(
		(externals, name) => ({
			...externals,
			[`@wordpress/${name}`]: `wp.${camelCaseDash(name)}`,
		}),
		{
			wp: 'wp',
			ga: 'ga', // Old Google Analytics.
			gtag: 'gtag', // New Google Analytics.
			react: 'React', // React itself is there in Gutenberg.
			jquery: 'jQuery', // import $ from 'jquery'; // Use jQuery from WP after enqueuing it.
			'react-dom': 'ReactDOM',
			// lodash: 'lodash', // Lodash is there in Gutenberg.
			cgbGlobal: 'cgbGlobal', // import globals from 'cgbGlobal'; // Localized data.
		}
	),
	module: {
		rules: [
			// Disable require.ensure as it's not a standard language feature.
			{ parser: { requireEnsure: false } },
			// Transform ES6 with Babel
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				options: {
					babelrc: false,
					exclude: [
						/core-js/,
						/regenerator-runtime/,
					],
					presets: [
						['@babel/preset-env', {
							// loose: true,
							modules: false,
							// debug: true,
							corejs: 3,
							useBuiltIns: 'usage',
							// targets: {} // leave this unset for preset-env to use browserslist config from package.json
						}],
					],
					plugins: [
						'@babel/plugin-syntax-dynamic-import',
						['@babel/plugin-transform-react-jsx', {
							"pragma": "wp.element.createElement",
							"pragmaFrag": "React.Fragment"
						}]
					],
				}
			},
			// styles
			{
				test: /.scss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							hmr: DEV,
						},
					},
					{
						loader: "css-loader",
					},
					{
						loader: "postcss-loader",
						options: {
							ident: "postcss", // https://webpack.js.org/guides/migrating/#complex-options
							plugins: () => [
								autoprefixer({
									cascade: false,
									remove: false,
									flexbox: true,
									grid: false,
								}),
							],
						},
					},
					{
						loader: "resolve-url-loader",
						options: {
							root: path.join(__dirname, 'styleguide'),
						},
					},
					{
						loader: "sass-loader",
						options: {
							sourceMap: true, // needed for resolve-url-loader
						},
					},
				],
			},
			// images
			{
				test: /\.(gif|png|jpe?g|svg|ico)$/i,
				include: [
					path.resolve('./styleguide/img'),
					path.resolve('./theme/lib/blocks/acf-blocks')
				],
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[path][name].[ext]',
							context: 'styleguide',
						}
					},
					{
						loader: 'image-webpack-loader',
						options: {
							disable: DEV,
						},
					},
				],
			},
			// fonts
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/i,
				include: path.resolve('./styleguide/fonts'),
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[path][name].[ext]',
							context: 'styleguide',
						}
					},
				],
			},
			// svg sprite
			{
				test: /\.svg$/,
				include: path.resolve('./styleguide/icons'),
				use: [
					// note: the svg sprite is injected in dom automatically
					//       use "extract" if we want to change this, to either
					//       fetch async or inject server-side
					'svg-sprite-loader',
					'svgo-loader',
				],
			},
			// other assets
			// {
			// 	test: /\.(xml|json)$/,
			// 	use: [
			// 		'file-loader',
			// 	],
			// },
		],
	},
	optimization: {
		minimize: !DEV,
		minimizer: [
			new OptimizeCSSAssetsPlugin({ // TODO: refine config
				cssProcessorOptions: {
					map: {
						inline: false,
						annotation: true,
					}
				}
			}),
			new TerserPlugin({ // TODO: find out why when activated, the js gets transpiled to es2015
				sourceMap: true,
				terserOptions: {
					compress: {
						warnings: false
					},
					output: {
						comments: false,
					}
				},
			})
		]
	},
	plugins: [
		// prod only
		!DEV && new CleanWebpackPlugin(),
		// prod & dev
		new MiniCssExtractPlugin({
			filename: DEV ? '[name].css' : '[name].[hash:8].css'
		}),
		new webpack.EnvironmentPlugin({
			NODE_ENV: 'development', // use 'development' unless process.env.NODE_ENV is defined
			DEBUG: false,
		}),
		new ManifestPlugin(),
		new SpriteLoaderPlugin(),
		// new DependencyExtractionWebpackPlugin({ // see https://developer.wordpress.org/block-editor/packages/packages-dependency-extraction-webpack-plugin/
		// 	requestToHandle(request) {
		// 		// avoid including lodash, as we don't include it in the frontend
		// 		// TODO: apply DependencyExtractionWebpackPlugin only on the "admin" entry to avoid this hack
		// 		console.log(request);
		// 		if (request === 'lodash' || request === 'lodash-es') {
		// 			return request; // same name ==> don't extract
		// 		}
		// 		else {
		// 			return false; // not lodash, continue extracting
		// 		}
		// 	}
		// }),
		// dev only
		DEV &&
			new webpack.HotModuleReplacementPlugin(),
		DEV &&
			new FriendlyErrorsPlugin({
				clearConsole: false,
			}),
	].filter(Boolean),
	devServer: {
		quiet: true,
		historyApiFallback: false,
		compress: false,
		host: 'localhost',
		port: 3000,
		https: (SCHEME === 'https' ? HTTPS : false),
		disableHostCheck: true,
		publicPath: `${SCHEME}://${HOST}:${PORT}/`,
		contentBase: path.join(__dirname, 'styleguide'),
		hot: true,
		// noInfo: true,
		overlay: true,
		open: false,
		stats: {
			colors: true
		},
		headers: { // fix cors
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
			"Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
		},
	},
}
