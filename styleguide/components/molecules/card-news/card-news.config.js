/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Card News',
	display: {
		padding: '30px',
	},
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			title: 'Découvrez la saveur de nos pizza !',
			description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
			link: '#'
		},
	},
};
