import './editor.scss';

import { registerBlockType } from '@wordpress/blocks';

import edit from './edit';

registerBlockType('supt/news', {
	title: 'Liste des articles',
	description: 'Réglez le nombre d\'articles à afficher. Attention, seuls les articles existants réellement dans \'Articles\' s\'afficheront.',
	icon: 'editor-table',
	category: 'common',
	attributes: {
		max_articles: { type: 'number', default: 1 },
		iteration: { type: 'array' }
	},
	keywords: [
		'articles'
	],
	edit,
	save: () => null,
});
