(function() {
	const sizes = {
		mobile: "375px",
		tablet: "768px",
		desktop: "110%"
  };

  const previewEl = document.querySelector( '.Pen-preview .Preview-wrapper');
  const btns = [].slice.call( document.querySelectorAll( '.Pen-device-size' ) );

  function resize( sizeName = 'desktop' ) {
    event.preventDefault();
      previewEl.style.width = sizes[ sizeName ];
  }

	btns.forEach(btn => {
    btn.addEventListener( 'click', function(event) {
      resize( event.target.getAttribute('rel') );
    });
  });

  window.addEventListener( 'keydown', function(event) {
    if ( event.ctrlKey ) {
      switch ( event.key.toLowerCase() ) {
        case 'm':
          resize( 'mobile' );
          break;
        case 't':
          resize( 'tablet' );
          break;
        case 'd':
          resize( 'desktop' );
          break;
      }
    }
  });

})();
