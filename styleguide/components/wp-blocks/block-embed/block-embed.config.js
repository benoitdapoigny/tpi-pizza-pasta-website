/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Block-embed',
	default: 'youtube',
	context: {
		modifiers: [],
		html_attrs: [],
		data: {},
	},
	variants: [
		{ name: 'facebook' },
		{ name: 'twitter' },
		{ name: 'youtube' },
	],
};
