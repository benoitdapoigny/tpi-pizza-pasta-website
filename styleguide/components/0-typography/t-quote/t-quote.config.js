/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Quote',
	display: {
		padding: '30px',
	},
	context: {
		modifiers: [],
		html_attrs: {},
		data: {
			quote: '«Eu laborum exercitation pariatur cillum voluptate magna eiusmod in pariatur dolore dolore. In ex magna anim culpa culpa enim. Magna veniam amet nostrud sint incididunt esse Lorem aliqua incididunt voluptate qui deserunt nisi non.»' ,
			source: '<b>John Smith</b><br/>Elit amet sunt consectetur culpa<br/>Lorem ea magna dolore <a href="#">occaecat anim commodo</a>'
		},
	}
};
