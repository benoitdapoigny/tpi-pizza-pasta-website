/* eslint-disable import/order */

/**
 * Config files
 */
const fs = require('fs');
const path = require('path');
const PKG = require('./package.json');
const CONFIG = require('./config.json');

/**
 * External dependencies
 */
const fractal = require('@frctl/fractal').create();
const mandelbrot = require('@frctl/mandelbrot');

/**
 * Local dependencies
 */
const responsiveImg = require('./styleguide/_fractal/helpers/responsive-img.js');
const tagAttrs = require('./styleguide/_fractal/helpers/tag-attrs.js');
const listComponents = require('./cli/list-components');
const generateSassFile = require('./cli/update-sass-loader');
const { twigSVG, hbsSVG } = require('./styleguide/_fractal/helpers/inline-svg.js');
const { hbsColorOrGetFromVar } = require('./styleguide/_fractal/helpers/colorOrGetFromVar.js');
const { __, _x, _n, _nx } = require('./styleguide/_fractal/helpers/wp-l10n.js');

const twigAdapter = require('@frctl/twig')({
	functions: { __, _x, _n, _nx },
	filters: {
		inlinesvg: twigSVG,
		attrs: tagAttrs,
		responsive: responsiveImg,
		get_timber_image_src: (data) => {
			if (
				typeof data === 'undefined' ||
				typeof data.src === 'undefined'
			) {
				return '';
			}
			return data.src;
		}, // placeholder
	},
	importContext: true,
});

const hbs = require('@frctl/handlebars')({
	helpers: {
		inlinesvg: hbsSVG,
		colorOrGetFromVar: hbsColorOrGetFromVar,
	},
});

// ================================
//          Setup Fractal
// ================================
fractal.set('project.title', CONFIG.styleguide.title);
fractal.set('project.logo', CONFIG.styleguide.logo);
fractal.set('project.repo', CONFIG.styleguide.repo);
fractal.set('project.version', PKG.version);

// Web
fractal.web.set('builder.dest', path.resolve(__dirname, CONFIG.styleguide.build)); // destination for the static export
fractal.web.set('static.path', path.resolve(__dirname, CONFIG.styleguide.static)); // location of public assets

// Documentation
fractal.docs.set('path', path.resolve(__dirname, CONFIG.styleguide.docs.path)); // location of the documentation directory.
fractal.docs.engine(hbs);

// Components
fractal.components.set('path', path.resolve(__dirname, CONFIG.styleguide.src.components)); // location of the component directory.
fractal.components.set('default.preview', '@preview'); // preview template component
fractal.components.set('ext', '.twig');
fractal.components.engine(twigAdapter);

/* Customize collation function
* @see https://fractal.build/guide/components/configuration-reference.html#default-collator
*/
fractal.components.set('default.collator', (markup, item) => `<!-- Start: @${ item.handle } -->\n${ markup }\n<!-- End: @${ item.handle } -->\n<div style="margin-top: 30px;"></div>`);
/**
 * Customize statuses
 * see https://fractal.build/guide/core-concepts/statuses
 */
fractal.components.set('statuses', CONFIG.styleguide.statuses);

// ================================
//    Customize styleguide theme
// ================================
const myCustomisedTheme = mandelbrot({
	favicon: '/img/favicons/favicon.ico',
	skin: 'white',
	nav: ['docs', 'components'],
	panels: ['html', 'view', 'resources', 'context', 'info', 'notes'],
	styles: ['default', ...fs.readdirSync(path.resolve(__dirname, CONFIG.styleguide.src.inception.css)).map((file) => `/_fractal/css/${ file }`)],
	scripts: ['default', ...fs.readdirSync(path.resolve(__dirname, CONFIG.styleguide.src.inception.js)).map((file) => `/_fractal/js/${ file }`)],
});
myCustomisedTheme.addLoadPath(path.resolve(__dirname, CONFIG.styleguide.src.inception.theme)); // specify a directory to hold the theme override templates
myCustomisedTheme.addStatic(CONFIG.styleguide.src.inception.css, '/_fractal/css'); // specify the static assets directory
myCustomisedTheme.addStatic(CONFIG.styleguide.src.inception.js, '/_fractal/js'); // specify the static assets directory
fractal.web.theme(myCustomisedTheme); // tell Fractal to use the configured theme by default

// ================================
//         Browser sync
// ================================
fractal.web.set('server.port', CONFIG.styleguide.server.port);
fractal.web.set('server.sync', CONFIG.styleguide.server.sync);
fractal.web.set('server.syncOptions', CONFIG.styleguide.syncOptions);

// ================================
//        Fractal commands
// ================================
fractal.cli.command('list:components', listComponents, { description: 'Lists components in the project' });
fractal.cli.command('generate:sass', generateSassFile, { description: 'Generate the SCSS component loader file' });

// ================================
//    Export fractal as module
// ================================
module.exports = fractal;

/**
 * Prevent Bluebird warnings like "a promise was created in a handler but was not returned from it"
 * caused by Nunjucks from polluting the console
 */
const bluebird = require('bluebird');

bluebird.config({
	warnings: false,
});
