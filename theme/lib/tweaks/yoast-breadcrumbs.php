<?php

namespace SUPT\Tweaks;

/**
 * Action & Filter hooks
 */
add_filter(	 'wpseo_breadcrumb_separator', __NAMESPACE__.'\replace_breadcrumbs_separator' );
add_filter(	 'wpseo_breadcrumb_links', __NAMESPACE__.'\breadcrumb_search_links' );

function replace_breadcrumbs_separator( $separator ) {
	return '<svg class="breadcrumbs__separator" role="img"><use href="#chevron-right" /></svg>';
}

/**
 * Make sure the search breadcrumbs item for the search page
 * is equal to the text defined in Yoast config (remove the "my searched text")
 */
function breadcrumb_search_links( $links ) {

	if ( is_search() ) {
		$searhc_page_bc_text = get_option('wpseo_titles')['breadcrumbs-searchprefix'];
		$links[1]['text'] = $searhc_page_bc_text;
	}

	return $links;
}
