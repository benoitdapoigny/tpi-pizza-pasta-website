#!/bin/sh

#===========================================
# Install and setup WordPress core
#===========================================
#
# README:
# * Edit the variable below to define
#   the website configurations
#
# * /!\ The site title should not contain
#   any space character as it make
#   WP-CLI to fail. Update the site name
#   afterwards in the admin backend.
#
#===========================================

LOCALE="fr_FR"
URL="localhost"
TITLE="PizzaPasta"
ADMIN_USER="admin"
ADMIN_PASSWORD="admin"
ADMIN_EMAIL="tech+supt-stack@superhuit.ch"

#===========================================
# /!\ STOP to edit here /!\
#===========================================
echo "----------------------------------"
echo "      WordPress installation      "
echo "----------------------------------"
echo
# Install core
wp @local core install --url="$URL" --title="$TITLE" --admin_user="$ADMIN_USER" --admin_password="$ADMIN_PASSWORD" --admin_email="$ADMIN_EMAIL"


# Set HTTPS uncomment to enable HTTPs
# wp @local search-replace "http://$URL" "https://$URL" --quiet

# Setup the language
wp @local language core install "$LOCALE" --activate


echo
echo "----------------------------------"
echo "  Theme install & configuration   "
echo "----------------------------------"
echo

wp @local theme activate pizza-pasta

wp @local theme uninstall twentynineteen --quiet
wp @local theme uninstall twentytwenty --quiet

echo
echo "----------------------------------"
echo "        Plugin activations        "
echo "----------------------------------"
echo

# Plugins installation & activation
wp @local plugin install disable-comments --activate
wp @local plugin install disable-embeds --activate
wp @local plugin install disable-emojis --activate
wp @local plugin install redirection --activate
wp @local plugin install resmushit-image-optimizer --activate
wp @local plugin install safe-svg --activate
wp @local plugin install timber-library --activate
wp @local plugin install wordpress-seo --activate

# Plugins present in codebase
# wp @local plugin activate advanced-custom-fields-pro

# Debug purpose plugin
wp @local plugin install query-monitor --activate-network

wp @local plugin uninstall hello --deactivate --quiet
wp @local plugin uninstall akismet --deactivate --quiet

echo
echo "----------------------------------"
echo "          Other configs           "
echo "----------------------------------"
echo

# Update WP translations
wp @local language core update

# Disable comments system
wp @local option update disable_comments_options '{"disabled_post_types":["post","page","event","attachment"],"remove_everywhere":true,"permanent":false,"extra_post_types":false,"db_version":6}' --format=json

# Other options
# wp $INSTANCE option update __option_name__ __otion_value__

echo
echo "----------------------------------"
echo "       Installation complete      "
echo "----------------------------------"
