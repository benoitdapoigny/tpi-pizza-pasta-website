<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $_ENV['WORDPRESS_DB_NAME']);

/** MySQL database username */
define('DB_USER', $_ENV['WORDPRESS_DB_USER']);

/** MySQL database password */
define('DB_PASSWORD', $_ENV['WORDPRESS_DB_PASSWORD']);

/** MySQL hostname */
define('DB_HOST', $_ENV['WORDPRESS_DB_HOST']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '11ee288a952c4cf3942fc2a715f0990fe28ad615');
define('SECURE_AUTH_KEY',  '6eaa28f851554784d3a307fd819b0ad417618e04');
define('LOGGED_IN_KEY',    '9586ed256987094116649e61d6b5be473799578c');
define('NONCE_KEY',        '267eaf12bb27526df3ee721b3c341c98a6875678');
define('AUTH_SALT',        '2bab5bb8d15e2e5ed6f79f4e7e9662b5509622dd');
define('SECURE_AUTH_SALT', 'b8c91b43222d768f27823d89f6d53b4eecac1f9b');
define('LOGGED_IN_SALT',   '353a46ffacd090003291a95651ad9b9708c6bde7');
define('NONCE_SALT',       '243b87be0c07d820783d5b999696c37429aa74d2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', $_ENV['WORDPRESS_DEBUG']);

/** Disable theme & plugins editor */
define( 'DISALLOW_FILE_EDIT', true );

/** Auto-update without FTP */
define('FS_METHOD','direct');


// If we're behind a proxy server and using HTTPS, we need to alert WordPress of that fact
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
        $_SERVER['HTTPS'] = 'on';
}

// // TODO uncomment to set multi-site
// define( 'MULTISITE',            true       );
// // Set this to true for sub-domain installations.
// define( 'SUBDOMAIN_INSTALL',    false      );
// define( 'DOMAIN_CURRENT_SITE',  'localhost');
// define( 'PATH_CURRENT_SITE',    '/'        );
// define( 'SITE_ID_CURRENT_SITE', 1          );
// define( 'BLOG_ID_CURRENT_SITE', 1          );
// define( 'WP_DEFAULT_THEME', 'storefront'   );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
        define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
