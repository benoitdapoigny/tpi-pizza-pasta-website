const CONFIG = require("../config.json").styleguide;
const fractal = require('../fractal');
const os = require('os');

const server = fractal.web.server(CONFIG.syncOptions);

server.on( 'error', err => fractal.cli.console.error(err.message) );
server.start().then(() => {
	fractal.cli.log('');
	fractal.cli.success(`Fractal server is now running`);
	fractal.cli.log(`Local URL: ${server.url}`);
	fractal.cli.log(`Network URL: http://${os.hostname()}:${server.port}`);
	fractal.cli.log(`BrowserSync UI: ${server.urls.sync.ui}\n`);
});
server.stop();
