/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Contact',
	display: {
		padding: '30px',
	},
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			phone: '021 634 73 73',
			email: 'contact@pizza-pasta.ch',
			name: 'Pizza-Pasta',
			street: 'Rue du Chemin 4',
			city: '1000 Lausanne'
		},
	},
};
