<?php

namespace SUPT\Admin;

use SUPT\Types\PostType;

add_filter( 'acf/fields/wysiwyg/toolbars', __NAMESPACE__ .'\add_tinymce_custom_toolbars' );
add_filter( 'tiny_mce_plugins', __NAMESPACE__ .'\remove_tinymce_custom_color_palette' );
// add_filter( 'tiny_mce_before_init', __NAMESPACE__ .'\force_paste_as_text');


/**
 * Adds custom toolbars to the wysiwyg
 *
 * @link  https://www.advancedcustomfields.com/resources/customize-the-wysiwyg-toolbars/
 *        https://github.com/AdvancedCustomFields/acf/blob/master/includes/fields/class-acf-field-wysiwyg.php#L148
 */
function add_tinymce_custom_toolbars( $toolbars ) {
	$toolbars['Only link'] = array();
	$toolbars['Only link'][1] = array('link');

	$toolbars['Very Simple'] = array();
	$toolbars['Very Simple'][1] = array('bold' , 'italic');

	$toolbars['Very Simple with Link'] = array();
	$toolbars['Very Simple with Link'][1] = array('bold', 'italic', 'link');

	return $toolbars;
}

/**
 * Sets 'paste_as_text' option to true to keep the text content junk free
 *
 * @link     https://developer.wordpress.org/reference/hooks/tiny_mce_before_init/
 *
 * @param    array    $mceInit      An array with TinyMCE config
 * @return   array                  The updated config
 *
 */
function force_paste_as_text($mceInit) {
	$current_post_type = $GLOBALS['current_screen']->post_type;

	// Array of post type slugs for which we want to apply the filter
	$post_type_targets = [PostType::MODEL_NAME];

	// bail eary if the current post type is not one of the post_type_targets
	if (!in_array($current_post_type, $post_type_targets)) return $mceInit;

	$mceInit['paste_as_text'] = true;
	return $mceInit;
}


/**
 * Removes the custom colors palete from TinyMCE
 *
 * @link     https://developer.wordpress.org/reference/hooks/tinymce_plugins/
 *
 * @param    array    $plugins    An array of default TinyMCE plugins
 * @return   array                The updated array
 *
 * Note: As we don't use this feature this is taking effect globally.
 *       Remove if needed or scope by using current_screen global
 */
function remove_tinymce_custom_color_palette($plugins) {
	foreach ( $plugins as $key => $plugin_name ) {
		if ( 'colorpicker' === $plugin_name ) {
			unset( $plugins[ $key ] );
			return $plugins;
		}
	}
	return $plugins;
}
