/**
 * Load assets
 */
import './icons';
import './img';

/**
 * Polyfills
 *
 * Do not load any polyfill here.
 * Load the appropriate polyfill in the appropriate file
 *
 * @see polyfills.legacy.js
 * @see polyfills.modern.js
 */

/**
 * Services etc
 */
import './js/services/viewport';

/**
 * Components loader
 */
import { componentsLoader } from './components/_loader';

/**
 * Styles
 */
import './main.scss';

componentsLoader();
