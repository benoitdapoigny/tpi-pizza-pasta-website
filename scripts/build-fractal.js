const fractal = require('../fractal');

const builder = fractal.web.builder();
builder.on('progress', (completed, total) => fractal.cli.console.update(`Exported ${completed} of ${total} items`, 'info'));
builder.on('error', err => fractal.cli.console.error(err.message));
// sleep(500).then(() => { // this is a temporary hack because sometimes fractal copies /static to /build before the svgsprite is done
	builder.build().then(() => {
		fractal.cli.console.success('Fractal build completed!');
	});
// });
