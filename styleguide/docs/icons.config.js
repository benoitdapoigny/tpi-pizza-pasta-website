module.exports = {
  context: {
    groups: [{
        name: 'general',
        icons: ['chevron-right',
          'external',
          'form-error',
          'form-success',
          'load-more'
        ],
        order: 0
      },
      {
        name: 'action',
        icons: ['clear', 'next', 'previous', 'tick'],
        order: 1
      },
      {
        name: 'social',
        icons: ['email', 'facebook', 'twitter', 'whatsapp'],
        order: 1
      }
    ]
  }
}