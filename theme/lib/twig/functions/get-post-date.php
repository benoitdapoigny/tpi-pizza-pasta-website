<?php

namespace SUPT;

use \Twig_SimpleFunction;

function get_post_date($w3c = false) {
	return get_the_date( ($w3c ? DATE_W3C : '') );
}

// Add the function to Twig
add_filter( 'get_twig', function( $twig ) {
	$twig->addFunction( new Twig_SimpleFunction( 'get_post_date', 'SUPT\get_post_date' ) );
	return $twig;
});
