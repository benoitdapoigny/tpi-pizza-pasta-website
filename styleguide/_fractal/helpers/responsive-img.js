module.exports = (data) => {
	if (typeof data === 'undefined') return '';

	return Object.keys(data)
		.filter(name => typeof data[name] !== 'undefined' && data[name] && data[name] != '')
		.filter(name => !name.startsWith('_'))
		.map(name => `${name}="${data[name]}"`)
		.join(' ');
};
