import { Component, Fragment } from '@wordpress/element';
import { RichText } from '@wordpress/block-editor';
import { ToggleControl } from '@wordpress/components';

export default class Weekmenu extends Component {
	constructor(...args) {
		super(...args);

		this.state = {
		};
	}

	componentDidMount() {
		const { attributes: attrs } = this.props;
	}

	updateAttributes(value, section_index, line_index) {
		const newAttrs = [...(this.props.attributes.meals || [])];
		newAttrs[section_index].list[line_index] = Object.assign(newAttrs[section_index].list[line_index], value);
		this.props.setAttributes({ meals: newAttrs });
	}

	holidayToggle(section_index) {
		const newAttrs = [...(this.props.attributes.meals || [])];
		newAttrs[section_index].isHoliday = !newAttrs[section_index].isHoliday;
		this.props.setAttributes({ meals: newAttrs });
	}

	updateHoliday(value, section_index) {
		const newAttrs = [...(this.props.attributes.meals || [])];
		newAttrs[section_index].holiday = value;
		this.props.setAttributes({ meals: newAttrs });
	}

	render() {
		const { attributes: attrs, setAttributes, isSelected } = this.props;
		const meals = attrs.meals;

		return (
			<Fragment>
				<div class="weekmenu">
					<div class="weekmenu__inner">
						<div class="weekmenu__header">
							<h1 class="weekmenu__title">Menu de la semaine</h1>
							<RichText
								className="weekmenu__header-date"
								allowedFormats={ [] }
								value={ attrs.date }
								onChange={ (date) => setAttributes({ date }) }
								placeholder={ 'Du xx au xx Mois Année' }
								keepPlaceholderOnFocus
							/>
						</div>
						<ul class="weekmenu__list">
							{ ( meals || [] ).map( ( section, section_index ) => (
							<li class="weekmenu__list-section" key={ section_index }>
								<div class="menu-section" data-menu-section>
									<div class="menu-section__header">
										<h2>{ section.title }</h2>
										{ section.isHoliday &&
										<h2 class="menu-section__header-holiday">Fermé - <RichText
											allowedFormats={ [] }
											value={ section.holiday }
											onChange={ (holiday) => this.updateHoliday( holiday, section_index ) }
											placeholder={ 'Nom du férié (ex: Lundi de pentecôte)' }
											keepPlaceholderOnFocus
										/></h2>
										}
										<ToggleControl
											label="Jour férié"
											checked={ section.isHoliday }
											onChange={ () => this.holidayToggle( section_index ) }
										/>
									</div>
									{ !section.isHoliday &&
									<ul class="menu-section__list">
									{ ( section.list || [] ).map( (line, line_index) => (
										<li class="menu-section__item" key={ line_index }>
											<span class="menu-section__item-text">
											<RichText
												allowedFormats={ [] }
												value={ line.name }
												onChange={ (name) => this.updateAttributes({name}, section_index, line_index) }
												placeholder={ 'Entrecôte Parisienne de boeuf...' }
												keepPlaceholderOnFocus
											/><span>
											<RichText
												allowedFormats={ [] }
												value={ line.description }
												onChange={ (description) => this.updateAttributes({ description }, section_index, line_index) }
												placeholder={ 'Description du plat...' }
												keepPlaceholderOnFocus
											/></span></span>
											<span class="menu-section__item-price">
											<RichText
												allowedFormats={ [] }
												value={ line.price }
												onChange={ (price) => this.updateAttributes({ price }, section_index, line_index) }
												placeholder={ '23.00' }
												keepPlaceholderOnFocus
											/></span>
										</li>
									) ) }
									</ul>
									}
								</div>
							</li>
							) ) }
						</ul>
						<span class="weekmenu__information">Service de midi uniquement</span>
					</div>
				</div>
			</Fragment>
		);
	}
}
