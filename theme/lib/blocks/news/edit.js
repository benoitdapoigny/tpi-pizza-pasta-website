import { Component, Fragment } from '@wordpress/element';
import { InspectorControls } from '@wordpress/block-editor';
import { RangeControl } from '@wordpress/components';
import { times } from 'lodash-es';

export default class News extends Component {
	constructor(...args) {
		super(...args);

		this.state = {
		};
	}

	componentDidMount() {
		const { attributes: attrs } = this.props;
	}

	updateArticles(value) {
		let itemCount = _.times(value, String);
		this.props.setAttributes({ iteration: itemCount });
	}

	render() {
		const { attributes: attrs, setAttributes, isSelected } = this.props;
		const iteration = (typeof attrs.iteration !== 'undefined') ? attrs.iteration : [''];

		if (iteration.length === 0) {
			let refreshIteration = _.times(attrs.max_articles);
			setAttributes( { iteration: refreshIteration } )
		}

		return (
			<Fragment>
				<InspectorControls>
					<div class="block-editor-block-card">
						<RangeControl
			        label="Nombre d'articles"
			        value={ attrs.max_articles }
			        onChange={ ( value ) => {
			          this.props.setAttributes({ max_articles: value })
								this.updateArticles( value );
			        } }
			        min={ 1 }
			        max={ 20 }
						/>
					</div>
				</InspectorControls>
				<div class="section-card-news">
					<div class="section-card-news__inner">
						<div class="section-card-news__title"><h1>News</h1></div>
						<div class="section-card-news__list">
						{ ( this.props.attributes.iteration || [''] ).map( ( card, i ) => (
							<a class="card-news" key={ i }>
								<h2 class="card-news__title">Ceci est un aperçu</h2>
								<p class="card-news__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							</a>
						) ) }
						</div>
					</div>
				</div>
			</Fragment>
		);
	}
}
