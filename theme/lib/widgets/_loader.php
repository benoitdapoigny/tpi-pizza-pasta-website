<?php

namespace SUPT\Widgets;

// require_once __DIR__ . '/CustomWidget.php';

// add_action('widgets_init', __NAMESPACE__ . '\unregister_core_widgets', 20);
// add_action('widgets_init', __NAMESPACE__ . '\register_widgets', 30);

/**
 * Unregister all widgets
 */
function unregister_core_widgets() {
	if ( empty ( $GLOBALS['wp_widget_factory'] ) )return;
	$GLOBALS['wp_widget_factory']->widgets = array();
}

/**
 * Register our widgets
 */
function register_widgets() {
	register_widget( 'SUPT\Widgets\CustomWidget' );
}
