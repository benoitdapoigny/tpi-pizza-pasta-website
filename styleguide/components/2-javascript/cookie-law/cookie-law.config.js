
module.exports = {
	status: 'wip',
	label: 'Cookie Law banner',
	context: {
		modalHidden: true
	},
	variants: [
		{
			name: 'open',
			label: 'Modal open',
			context: {
				modalHidden: false,
			}
		}
	]
}
