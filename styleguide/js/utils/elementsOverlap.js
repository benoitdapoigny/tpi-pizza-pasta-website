/**
 * Check if two dom elements are overlapping.
 *
 * @param {Element} element
 * @return {Boolean}
 */
function elementsOverlap(element_1, element_2) {
	if( element_1.length && element_1.length > 1 ){
		element_1 = element_1[0];
	}
	if( element_2.length && element_2.length > 1 ){
		element_2 = element_2[0];
	}
	var rect1 = element_1 instanceof Element ? element_1.getBoundingClientRect() : false;
	var rect2 = element_2 instanceof Element ? element_2.getBoundingClientRect() : false;
	
	// window.console ? console.log(rect1, rect2 ) : null ;
	var overlap = null;
	if( rect1 && rect2 ){
		overlap = !(
				rect1.right < rect2.left || 
				rect1.left > rect2.right || 
				rect1.bottom < rect2.top || 
				rect1.top > rect2.bottom
			)
		return overlap;  
	} else {
		window.console ? console.warn( 'Please pass native Element object' ) : null;
		return overlap;
	}
}

export default elementsOverlap;
