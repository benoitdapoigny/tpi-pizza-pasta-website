/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Section Image',
	display: {
		padding: '30px',
	},
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			image: {
				src: '/img/placeholder/food_plate.jpg',
				alt: ''
			}
		},
	},
};
