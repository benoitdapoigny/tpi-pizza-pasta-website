import './editor.scss';

import { registerBlockType } from '@wordpress/blocks';

import edit from './edit';

registerBlockType('supt/contact', {
	title: 'Contact et adresse',
	description: 'Vos informations de contact.',
	icon: 'admin-home',
	category: 'common',
	attributes: {
		name:   { type: 'string' },
		street: { type: 'string' },
		city:   { type: 'string' },
		phone:  { type: 'string' },
		email:  { type: 'string' },
	},
	keywords: [
		'contact'
	],
	edit,
	save: () => null,
});
