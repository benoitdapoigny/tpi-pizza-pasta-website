/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Quote',
	display: {
		padding: '30px',
	},
	collated: true,
	context: {
		modifiers: [],
		html_attrs: {},
		data: {
			quote: '“We think there is <strong>no limit</strong> to a child’s potential and we help every student explore theirs, so they have the best possible chance of leading a successful, fulfilling life.”',
			source: '<em>Frazer Cairns</em>, <a href="#">ISL School Director</a>'
		},
	}
};
