module.exports = {
  context: {
    palettes: [{
        title: 'Brand colors',
        index: 35,
        colors: [{
            name: 'color-brand',
            value: '#2B2828'
          },
          {
            name: 'color-brand-alt',
            value: '#F0F0F0'
          }
        ]
      },
      {
        title: 'Neutral colors',
        index: 41,
        colors: [{
            name: 'color-neutral',
            value: '#000000'
          },
          {
            name: 'color-neutral--800',
            value: '#222'
          },
          {
            name: 'color-neutral--700',
            value: '#505050'
          },
          {
            name: 'color-neutral--600',
            value: '#707070'
          },
          {
            name: 'color-neutral--500',
            value: '#858585'
          },
          {
            name: 'color-neutral--400',
            value: '#CBCBCB'
          },
          {
            name: 'color-neutral--300',
            value: '#DEDEDE'
          },
          {
            name: 'color-neutral--200',
            value: '#EFEFEF'
          },
          {
            name: 'color-neutral--100',
            value: '#FFFFFF'
          }
        ]
      },
      {
        title: 'Text colors',
        index: 54,
        colors: [{
            name: 'color-text',
            value: '#000000'
          },
          {
            name: 'color-text-muted',
            value: '#707070'
          }
        ]
      },
      {
        title: 'Status colors',
        index: 60,
        colors: [{
            name: 'color-status--sending',
            value: '#A9AAAD'
          },
          {
            name: 'color-status--success',
            value: '#00A54B'
          },
          {
            name: 'color-status--error',
            value: '#EC1A1A'
          }
        ]
      }
    ]
  }
}
