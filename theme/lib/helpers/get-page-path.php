<?php

namespace SUPT;

function get_page_path( $page_id ) {

	if ( get_post_type( $page_id ) !== 'page' ) return false;

	$site_url = get_site_url();
	$page_permalink = get_permalink( $page_id );
	$page_slug = str_replace( $site_url, '', $page_permalink);

	return trim($page_slug, '/');
}
