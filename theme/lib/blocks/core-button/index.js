/**
 * Adapt built-in WP Block to match our needs
 *
 * - Change block category to common
 * - Remove custom Class names support
 * - Remove alignement support
 * - Replace default styles by Primary & Secondary styles
 * - Remove multiple options while editing
 *    - Border radius
 *    - Text/Background colors
 *    - Contrast checker
 *    - Link rel
 *    - Text formats (bold, italic)
 *
 * @see https://wordpress.org/gutenberg/handbook/designers-developers/developers/filters/block-filters/
 */

// WordPress dependencies
import { _x } from '@wordpress/i18n';
import domReady from '@wordpress/dom-ready';
import { addFilter } from '@wordpress/hooks';

// Local dependencies
import './button.scss';
import edit from './edit';

const BLOCK_NAME = 'core/button';

function filterBlockType(settings, name) {
	/* eslint-disable no-param-reassign */

	if (name === BLOCK_NAME) {
		settings.edit = edit;
		settings.category = 'common';
		settings.supports = {
			...settings.supports,
			customClassName: false,
			align: false,
			alignWide: false,
		};
	}

	return settings;
}

addFilter('blocks.registerBlockType', 'supt/core-button', filterBlockType, 99);

function unregisterDefaultStyles() {
	wp.blocks.unregisterBlockStyle('core/button', 'default');
	wp.blocks.unregisterBlockStyle('core/button', 'fill');
	wp.blocks.unregisterBlockStyle('core/button', 'outline');
	wp.blocks.unregisterBlockStyle('core/button', 'squared');
}

function registerCustomStyles() {
	wp.blocks.registerBlockStyle('core/button', {
		name: 'primary',
		label: _x('Principal', 'Button style name', 'supt'),
		isDefault: true,
	});
	wp.blocks.registerBlockStyle('core/button', {
		name: 'secondary',
		label: _x('Secondaire', 'Button style name', 'supt'),
	});
}

domReady(() => {
	unregisterDefaultStyles();
	registerCustomStyles();
});
