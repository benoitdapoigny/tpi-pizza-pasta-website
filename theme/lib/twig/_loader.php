<?php

/**
 * TWIG
 * ====
 *
 * Twig extensions, filters and functions
 */

require_once __DIR__ .'/filters/html-attrs.php';
require_once __DIR__ .'/filters/responsive-img.php'; // TODO: add lazy loading option (see ISL)
require_once __DIR__ .'/functions/get-post-date.php';
require_once __DIR__ .'/functions/get-primary-term.php';
