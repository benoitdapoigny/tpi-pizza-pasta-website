<?php

namespace SUPT\Blocks\News;

use Timber\Timber;

/**
 * ACTION & FILTERS
 * ================
 */
add_action( 'init', __NAMESPACE__ . '\register_blocks' );

function register_blocks() {
	register_block_type( 'supt/news', [ 'render_callback' => __NAMESPACE__ . '\callback' ] );
}

function get_articles($query, $attrs) {
	$articles = array();

	for ($i=0; $i < $attrs['max_articles']; $i++) {
		if (isset($query[$i])) {
			$articles[$i]['title'] = $query[$i]->post_title;
			$articles[$i]['description'] = get_the_excerpt($query[$i]->ID);
			$articles[$i]['link'] = get_permalink($query[$i]->ID);
		}
	}

	return $articles;
}

function callback( $attrs ) {

	$context = [
		'data' => [
			'list' => get_articles(get_posts(), $attrs)
		]
	];

	return Timber::compile( 'organisms/section-card-news/section-card-news.twig', $context );
}
