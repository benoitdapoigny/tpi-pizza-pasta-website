# Content exports

Files in this folder are exported content to be used for dev purpose only.

It is to facilitate the creation of content between developers, in order to avoid
a new developer to create by hand all the content needed for him/her to start developing.

Simply use the WordPress Importer.