/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-underscore-dangle */

/**
 * These function are copied from https://github.com/WordPress/WordPress/blob/master/wp-includes/l10n.php
 * translated to JS and adapted to be used in Fractal styleguide
 */

/**
 * Retrieve the translation of text.
 *
 * @param {string} text   Text to translate.
 * @param {string} domain Optional. Text domain. Unique identifier for retrieving translated strings.
 * @return {string} Translated text.
 */
function __(text, domain = null) {
	return text;
}

/**
 * Retrieve translated string with gettext context.
 *
 * Quite a few times, there will be collisions with similar translatable text
 * found in more than two places, but with different translated context.
 *
 * By including the context in the pot file, translators can translate the two
 * strings differently.
 *
 * @param {string} text    Text to translate.
 * @param {string} context Context information for the translators.
 * @param {string} domain  Optional. Text domain. Unique identifier for retrieving translated strings.
 * @return {string} Translated context string without pipe.
 */
function _x(text, context = null, domain = null) {
	return text;
}

/**
 * Translates and retrieves the singular or plural form based on the supplied number.
 *
 * Used when you want to use the appropriate form of a string based on whether a
 * number is singular or plural.
 *
 * Example:
 *
 *     printf( _n( '%s person', '%s people', count, 'text-domain' ), number_format_i18n( count ) );
 *
 * @param {string} single The text to be used if the number is singular.
 * @param {string} plural The text to be used if the number is plural.
 * @param {number} number The number to compare against to use either the singular or plural form.
 * @param {string} domain Optional. Text domain. Unique identifier for retrieving translated strings.
 * @return {string} The translated singular or plural form.
 */
function _n(single, plural, number = 1, domain = null) {
	return (number > 1 ? plural : single);
}

/**
 * Translates and retrieves the singular or plural form based on the supplied number, with gettext context.
 *
 * This is a hybrid of _n() and _x(). It supports context and plurals.
 *
 * Used when you want to use the appropriate form of a string with context based on whether a
 * number is singular or plural.
 *
 * Example of a generic phrase which is disambiguated via the context parameter:
 *
 *   printf( _nx( '%s group', '%s groups', people, 'group of people', 'text-domain' ), number_format_i18n( people ) );
 *   printf( _nx( '%s group', '%s groups', animals, 'group of animals', 'text-domain' ), number_format_i18n( animals ) );
 *
 * @since 2.8.0
 *
 * @param {string} single  The text to be used if the number is singular.
 * @param {string} plural  The text to be used if the number is plural.
 * @param {number} number  The number to compare against to use either the singular or plural form.
 * @param {string} context Context information for the translators.
 * @param {string} domain  Optional. Text domain. Unique identifier for retrieving translated strings.
 * @return {string} The translated singular or plural form.
 */
function _nx(single, plural, number = 1, context = null, domain = null) {
	return (number > 1 ? plural : single);
}

module.exports = {
	__, _x, _n, _nx,
};
