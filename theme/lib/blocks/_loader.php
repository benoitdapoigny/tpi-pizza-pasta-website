<?php
/**
 * BLOCKS
 * ======
 *
 * Register Blocks for WP block editor (aka. Gutenberg)
 */

// ACF blocks
require_once __DIR__.'/acf-blocks/_loader.php';


// Native Blocks controllers
require_once __DIR__.'/contact/contact.php';
require_once __DIR__.'/news/news.php';
require_once __DIR__.'/schedule/schedule.php';
require_once __DIR__.'/menu/menu.php';
require_once __DIR__.'/weekmenu/weekmenu.php';
