<?php

namespace SUPT\Admin\AcfCustomMetaboxTitle;

// Action & filter hooks
add_action( 'acf/render_field_group_settings', __NAMESPACE__.'\add_custom_field_group_settings' );
add_filter( 'acf/get_field_groups', __NAMESPACE__.'\apply_custom_field_group_settings', 20, 1 );

/**
 * Add a new field in admin pages for the field group.
 */
function add_custom_field_group_settings( $field_group ) {
	acf_render_field_wrap(array(
		'label' => __('Metabox Title', 'supt'),
		'instructions' => __('If specified it will be displayed instead of field group title.', 'supt'),
		'type' => 'text',
		'name' => 'metabox_title',
		'prefix' => 'acf_field_group',
		'value' => (isset($field_group['metabox_title'])) ? $field_group['metabox_title'] : NULL,
	));

	acf_render_field_wrap(array(
		'label' => __('Hide Metabox Title', 'supt'),
		'instructions' => __('If checked it will completly hide the title.', 'supt'),
		'type' => 'true_false',
		'name' => 'metabox_hide_title',
		'prefix' => 'acf_field_group',
		'value' => ( isset($field_group['metabox_hide_title']) ) ? $field_group['metabox_hide_title'] : 0,
		'default_value'	=> 0,
		'ui' => 1,
		'ui_on_text' => __('Yes', 'supt'),
		'ui_off_text' => __('No', 'supt'),
	));
}


/**
 * Replace metabox title for field group title if defined.
 */
function apply_custom_field_group_settings($field_groups) {

	// Bail early if we are in a REST request cause
	// PHP crashes after that
	if ( defined( 'REST_REQUEST' ) && REST_REQUEST ) return $field_groups;

	$current_screen = get_current_screen();

	// Bail early if current screen is not object
	// Happens on ACF Blocks ajax request
	if (! is_object($current_screen)) return $field_groups;

	/**
	 * Bail early if we're on the "Field Groups" listing page
	 * We don't want to customize or hide the field group title there
	 */
	if($current_screen->post_type == "acf-field-group") return $field_groups;

	// Loop through available field groups
	foreach ( $field_groups as $k => $field_group ) {
		// If a metabox title is set and not empty, change the original title
		if ( isset($field_group['metabox_title']) && !empty($field_group['metabox_title']) ) {
			$field_groups[$k]['title'] = $field_group['metabox_title'];
		}

		// Remove the title if 'Hide Metabox Title' is checked
		if ( isset($field_group['metabox_hide_title']) && $field_group['metabox_hide_title']) {
			$field_groups[$k]['title'] = "";
		}
	}
	// return the data
	return $field_groups;
}
