/* eslint-disable func-names, prefer-arrow-callback, no-var, default-case, wrap-iife */
(() => {
	var iframeEl = document.querySelector('.Preview-iframe');
	var selectEl = document.querySelector('#Pen-theme-select');

	if (! (iframeEl && selectEl)) {
		return;
	}

	selectEl.addEventListener('change', function() {
		iframeEl.contentWindow.postMessage({
			type: 'theme-change',
			value: selectEl.value,
		}, '*');
	});
})();
