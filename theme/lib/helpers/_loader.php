<?php

/**
 * HELPERS
 * ====
 *
 * Helper functions
 */

require_once __DIR__.'/get-page-path.php';
require_once __DIR__.'/get-post-meta-db.php';
require_once __DIR__.'/get-single-taxonomy-term.php';
require_once __DIR__.'/slugify.php';
require_once __DIR__.'/test-url.php';
require_once __DIR__.'/truncate.php';
