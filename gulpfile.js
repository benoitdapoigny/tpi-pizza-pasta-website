/**
 * External
 */
const { parallel } = require('gulp');
/**
 * Internal
 */
const CONFIG = require('./config.json');

/**
 * Load Tasks.
 */
const sgDocIcons = require('./gulp/styleguide-doc/icons.js');
const sgDocColors = require('./gulp/styleguide-doc/colors.js');

/**
 * Styleguide specific tasks
 *
 * @param done
 */
function sgDocIconsTask(done) {
	sgDocIcons(CONFIG.styleguide.docs.icons)
		.then(done);
}
function sgDocColorsTask(done) {
	sgDocColors(CONFIG.styleguide.docs.colors)
		.then(done);
}

exports.default = parallel(sgDocIconsTask, sgDocColorsTask);
