import BaseView from '../../../js/base-view';

const SELECTOR_TOGGLE = '.menu-section__header';
const SELECTOR_LIST   = '.menu-section__list'
const CLASS_IS_OPEN   = 'is-open';

export default class MenuSection extends BaseView {

	/**
	 * Entrypoint
	 */
	init() {
		// refs
		// to keep reference of scoped dom elements
		this.refs = {
			toggleButton: this.el.querySelector( SELECTOR_TOGGLE ),
			list:         this.el.querySelector( SELECTOR_LIST ),
		};

		// state
		// the component's dynamic data representing its "state"
		this.state = {
			toggle: false,
		};

		// attach events
		this.bindEvents();
	}

	/**
	 * Bind component's events
	 */
	bindEvents() {
		this.on('click', SELECTOR_TOGGLE, this.toggle.bind(this));
		this.on('resize', this.onResize.bind(this));
	}


	// --------------------------------
	// #region Event Handlers
	// --------------------------------

	/**
	 * Toggle when we click on the plus button
	 * @param {Event} ev
	 */
	toggle() {
		if (!this.state.toggle) this.el.classList.add( CLASS_IS_OPEN )
		else this.el.classList.remove( CLASS_IS_OPEN )
		this.refs.list.style.maxHeight = (!this.state.toggle) ? `${this.getListHeight(this.refs.list.children)}px` : '';
		this.state.toggle = !this.state.toggle;
	}

	/**
	 * Trigger the toggle when resizing the window to avoid height issues
	 * @param {Event} ev
	 */
	onResize() {
		if (this.state.toggle) this.toggle();
	}

	// --------------------------------
	// #endregion
	// --------------------------------

	/**
	 * Get the height of the list
	 * @param {HTMLCollection} list
	 */
	getListHeight(list) {
		let height = 0;

		Array.from(list).forEach(function(item) {
			height += item.offsetHeight;
	  });

	  return height;
	}

	/**
	 * Before the component gets destroyed
	 * - unbind any event not bound with this.on()
	 * - reset UI if needed (any classes/attributes added?)
	 */
	beforeDestroy() {
		// this.resetUI();
	}
}
