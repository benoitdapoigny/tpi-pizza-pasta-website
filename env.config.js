const fs = require('fs');
const path = require('path');

module.exports = {
	THEME_NAME: 'pizza-pasta',
	PROXY_TARGET: 'localhost',
	HOST: 'localhost',
	PORT: 3000,
	SCHEME: 'http', // Can be set to https to enable HTTPs
	HTTPS: {
		key: fs.readFileSync('./server/ssl/server.key'),
		cert: fs.readFileSync('./server/ssl/server.crt'),
		ca: fs.readFileSync('./server/ssl/rootCA.pem'),
	},
	PATHS: {
		build: path.resolve(__dirname, 'theme/static'),
		indexJs: path.resolve(__dirname, 'styleguide/index.js'),
		polyfillsModernJS: path.resolve(__dirname, 'styleguide/polyfills.modern.js'),
		polyfillsLegacyJS: path.resolve(__dirname, 'styleguide/polyfills.legacy.js'),
		adminJs: path.resolve(__dirname, 'theme/lib/editor-loader.js'),
		modules: path.resolve(__dirname, 'node_modules'),
		base: path.resolve(__dirname, '.'),
	}
};
