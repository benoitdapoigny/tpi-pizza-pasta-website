/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'ready',
	label: 'Button',
	display: {
		padding: '30px',
	},
	default: 'primary',
	collated: true,
	notes: "This component is not following the BEM methodology because we use the WP Button block which has some specific classnames.",
	variants: [
		{
			name: 'primary',
			context: {
				modifiers: ['button--secondary'],
				data: {
					title: "Primary",
					link: "#",
				}
			},
		},
		{
			name: 'Secondary external link',
			context: {
				modifiers: ['button--secondary'],
				data: {
					title: "Primary Blank",
					link: "#",
					target: "_blank"
				}
			},
		},
	]
};
