<?php

namespace SUPT\Customizer;

// Action & Filter hooks
add_action( 'customize_register', '\SUPT\Customizer\deregister_customize_elements' );
add_action( 'customize_register', '\SUPT\Customizer\register_selective_refresh' );

/**
 * De-register unwanted panels, section or controls
 */
function deregister_customize_elements( $wp_customize ) {
	$wp_customize->remove_control("site_icon");
	$wp_customize->remove_section("custom_css");
}

function register_selective_refresh( $wp_customize ) {
	// TODO: update with correct values!
	// $wp_customize->selective_refresh->add_partial( 'nav_menu_locations[primary]', array( 'selector' => '.nav__menu' ) );
	// $wp_customize->selective_refresh->add_partial( 'nav_menu_locations[quicklinks]', array( 'selector' => '.quick-links' ) );
	// $wp_customize->selective_refresh->add_partial( 'nav_menu_locations[cta]', array( 'selector' => '.button__inscription' ) );
	// $wp_customize->selective_refresh->add_partial( 'nav_menu_locations[toolbox]', array( 'selector' => '[data-trigger-toolbox]' ) );
}
