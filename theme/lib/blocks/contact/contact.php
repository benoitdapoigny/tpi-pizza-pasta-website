<?php

namespace SUPT\Blocks\Contact;

use Timber\Timber;

/**
 * ACTION & FILTERS
 * ================
 */
add_action( 'init', __NAMESPACE__ . '\register_blocks' );

function register_blocks() {
	register_block_type( 'supt/contact', [ 'render_callback' => __NAMESPACE__ . '\callback' ] );
}

function callback( $attrs ) {
	return;
}
