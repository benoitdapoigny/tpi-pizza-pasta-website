// import all icons to be included in the webpack bundle.
function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context('./', true, /\.svg$/));
