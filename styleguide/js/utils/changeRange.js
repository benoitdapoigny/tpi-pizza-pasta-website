/**
 * Update range of value
 *
 * @param {Number} num Value to be converted
 * @param {Number} inMin Initial Min Value
 * @param {Number} inMax Initial Max Value
 * @param {Number} outMin Output Min Value. Optional. Default: 0
 * @param {Number} outMax Output Max Value. Default: 1
 * @return {Boolean}
 */

function changeRange(num, inMin, inMax, outMin = 0, outMax = 1) {
	return (
		((num - inMin) * (outMax - outMin)) / (inMax - inMin) + outMin
	);
}

export default changeRange;
