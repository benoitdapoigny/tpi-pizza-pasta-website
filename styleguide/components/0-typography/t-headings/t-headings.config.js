/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'ready',
	label: 'Headings',
	display: {
		padding: '30px',
	},
	collated: true,
	default: 'h1',
	context: {
		modifiers: [],
		attributes: [],
	},
	variants: [
		{
			name: 'h1',
			context: {
				tag: 'h1',
				class: 'h1',
				data: {
					text: 'h1. Content Heading',
				},
			},
		},
		{
			name: 'h2',
			context: {
				tag: 'h2',
				class: 'h2',
				data: {
					text: 'h2. Content Heading',
				},
			},
		},
		{
			name: 'h3',
			context: {
				tag: 'h3',
				class: 'h3',
				data: {
					text: 'h3. Content Heading',
				},
			},
		},
		{
			name: 'h4',
			context: {
				tag: 'h4',
				class: 'h4',
				data: {
					text: 'h4. Content Heading',
				},
			},
		},
	],
};
