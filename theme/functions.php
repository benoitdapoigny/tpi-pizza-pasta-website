<?php
/**
 * GLOBAL CONSTANTS
 */
$theme = wp_get_theme();
define( 'THEME_NAME'   , $theme['Name'] );
define( 'THEME_VERSION', $theme['Version'] );
define( 'THEME_PATH'   , get_stylesheet_directory() );
define( 'THEME_URI'    , get_stylesheet_directory_uri() );

/**
 * REQUIREMENTS
 */

// composer packages
// note: vendor folder should be in the wordpress root folder
//       ex: - /var/www/html/vendor/
//           - /var/www/html/wp-content/themes/my-theme/functions.php
require_once __DIR__ . '/../../../vendor/autoload.php';

// Stop if Timber not defined
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	} );

	add_filter('template_include', function( $template ) {
		return THEME_PATH . '/templates/no-timber.html';
	});

	return;
}

// This is where the magic happens
require_once __DIR__ . '/lib/_loader.php';


/**
 * SETUP & INIT
 */

// Sets the directories (inside your theme) to find .twig files
\Timber::$dirname = array('templates', 'templates/components');

new Timber\Timber();

// Initialize Timber theme
new SuptTheme();

// Initialize Timmy (Timber image manipulation)
if (function_exists('Timmy\Timmy')) {
	new Timmy\Timmy();
}

// Local development - setup mailhog
if (WP_DEBUG) {
	add_action( 'phpmailer_init', 'setup' );
	function setup( $phpmailer ) {
			$phpmailer->Host = 'mailhog';
			$phpmailer->Port = 1025;
			$phpmailer->IsSMTP();
	}
}
