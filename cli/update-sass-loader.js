const fs = require('fs');

const CONFIG = require('../config.json');

/**
 * Return the relative path
 * for the scss file.
 *
 * @param {String}
 * @return {String}
 */
function getRelativePath(viewPath) {
	return `@import "./components/${viewPath.replace('.twig', '')}";\n`;
}

/**
 * Generate the Sass file used to load components.
 * The file will be a list of import.
 */
function generateSassFile(args, done) {
	const stream = fs.createWriteStream(CONFIG.styleguide.src.loaders.scss);
	let importBuffer = '// __ THIS IS AN AUTO-GENERATED FILE __\n';

	for (const item of this.fractal.components.flatten()) {
		const view_path = item.relViewPath;
		const stylesheetExists = fs.existsSync(`${item.viewDir}/_${item.name}.scss`);
		if (view_path.indexOf('_preview') === -1 && stylesheetExists) {
			importBuffer += getRelativePath(view_path);
		}
	}
	stream.write(importBuffer);
	// Wait for the end event to complete
	// the task, or the write will fail.
	stream.on('end', () => {
		stream.end();
		done();
	});
}

module.exports = generateSassFile;
