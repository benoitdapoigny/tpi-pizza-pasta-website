<?php

namespace SUPT\Customizer\Footer;

use \WP_Customize_Control;

/**
 * Constants
 */
const SECTION_ID = 'footer_section';

const FOOTER_RESTAURANT_NAME = 'footer_name';
const FOOTER_RESTAURANT_STREET = 'footer_street';
const FOOTER_RESTAURANT_CITY = 'footer_city';
const FOOTER_SOCIAL_TEXT = 'footer_social_text';
const FOOTER_SOCIAL_LINK = 'footer_social_link';
const FOOTER_PHONE = 'footer_phone';
const FOOTER_EMAIL = 'footer_email';

// Action & Filter hooks
add_action( 'customize_register', 'SUPT\Customizer\Footer\customize_register' );

function customize_register( $wp_customize ) {

	$wp_customize->add_section(
		SECTION_ID,
		array(
			'title' 			=> 'Footer',
			'description' =>  'Définissez le contenu du footer',
			'priority' 		=> 160,
			'capability' 	=> 'edit_theme_options',
		)
	);

	// Restaurant name
	$wp_customize->add_setting(
		FOOTER_RESTAURANT_NAME,
		array(
			'type'       => 'theme_mod',
			'capability' => 'edit_theme_options',
			'transport'  => 'refresh',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			FOOTER_RESTAURANT_NAME.'_control',
			array(
				'type'        => 'text',
				'label'       =>  'Nom du restaurant',
				'input_attrs' => array(
					'placeholder' => '',
				),
				'section'     => SECTION_ID,
				'settings'    => FOOTER_RESTAURANT_NAME
			)
		)
	);

	// Restaurant street
	$wp_customize->add_setting(
		FOOTER_RESTAURANT_STREET,
		array(
			'type'       => 'theme_mod',
			'capability' => 'edit_theme_options',
			'transport'  => 'refresh',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			FOOTER_RESTAURANT_STREET.'_control',
			array(
				'type'        => 'text',
				'label'       =>  'Rue du restaurant',
				'input_attrs' => array(
					'placeholder' => 'Rue du Chemin 24',
				),
				'section'     => SECTION_ID,
				'settings'    => FOOTER_RESTAURANT_STREET
			)
		)
	);

	// Restaurant city
	$wp_customize->add_setting(
		FOOTER_RESTAURANT_CITY,
		array(
			'type'       => 'theme_mod',
			'capability' => 'edit_theme_options',
			'transport'  => 'refresh',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			FOOTER_RESTAURANT_CITY.'_control',
			array(
				'type'        => 'text',
				'label'       =>  'Ville',
				'input_attrs' => array(
					'placeholder' => 'NPA Ville',
				),
				'section'     => SECTION_ID,
				'settings'    => FOOTER_RESTAURANT_CITY
			)
		)
	);

	// Social media
	$wp_customize->add_setting(
		FOOTER_SOCIAL_TEXT,
		array(
			'type'       => 'theme_mod',
			'capability' => 'edit_theme_options',
			'transport'  => 'refresh',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			FOOTER_SOCIAL_TEXT.'_control',
			array(
				'type'        => 'text',
				'label'       =>  'Réseau social',
				'input_attrs' => array(
					'placeholder' => 'Visitez notre page Facebook',
				),
				'section'     => SECTION_ID,
				'settings'    => FOOTER_SOCIAL_TEXT
			)
		)
	);

	// Social media link
	$wp_customize->add_setting(
		FOOTER_SOCIAL_LINK,
		array(
			'type'       => 'theme_mod',
			'capability' => 'edit_theme_options',
			'transport'  => 'refresh',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			FOOTER_SOCIAL_LINK.'_control',
			array(
				'type'        => 'text',
				'label'       =>  'Lien du réseau social',
				'input_attrs' => array(
					'placeholder' => 'https://www.facebook.com/exemple',
				),
				'section'     => SECTION_ID,
				'settings'    => FOOTER_SOCIAL_LINK
			)
		)
	);

	// Phone number
	$wp_customize->add_setting(
		FOOTER_PHONE,
		array(
			'type' 							=> 'theme_mod',
			'capability' 				=> 'edit_theme_options',
			'transport'         => 'refresh',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'footer_phone_control',
			array(
				'type'        => 'tel',
				'label'       =>  'Phone number',
				'input_attrs' => array(
					'placeholder' => '+41 (0)XX XXX XX XX',
				),
				'section'     => SECTION_ID,
				'settings'    => FOOTER_PHONE
			)
		)
	);

	// Email
	$wp_customize->add_setting(
		FOOTER_EMAIL,
		array(
			'type'       => 'theme_mod',
			'capability' => 'edit_theme_options',
			'transport'  => 'refresh',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			FOOTER_EMAIL,
			array(
				'type'        => 'text',
				'label'       =>  'Email',
				'input_attrs' => array(
					'placeholder' => 'mail@exemple.ch',
				),
				'section'     => SECTION_ID,
				'settings'    => FOOTER_EMAIL
			)
		)
	);

	$wp_customize->selective_refresh->add_partial( FOOTER_RESTAURANT_NAME, array( 'selector' => '.footer__section-name' ) );
	$wp_customize->selective_refresh->add_partial( FOOTER_PHONE, array( 'selector' => '.footer__section-phone' ) );
}

// function validate_swiss_phone_number( $phone ) {
// 	return (bool)preg_match( '/'.SWISS_PHONE_PATTERN.'/', $phone );
// }

// function validate_phone( $validity, $value ) {
// 	if ( !empty($value) && ! validate_swiss_phone_number($value) ) {

// 		$error =  'This phone number is not valid.';
// 		$error .=  '<br><em>Format: '. '+41 (0)XX XXX XX XX' .'</em>';

// 		$validity->add( 'format', $error );
// 	}
// 	return $validity;
// }
