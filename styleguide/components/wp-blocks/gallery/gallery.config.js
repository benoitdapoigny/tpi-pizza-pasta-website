/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Gallery',
	display: {
		padding: '30px',
	},
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			images: [
				{
					src: 'https://picsum.photos/768/432?random=1',
					alt: 'image 1',
				},
				{
					src: 'https://picsum.photos/768/432?random=2',
					alt: 'image 2',
				},
				{
					src: 'https://picsum.photos/768/432?random=3',
					alt: 'image 3',
				},
				{
					src: 'https://picsum.photos/768/432?random=4',
					alt: 'image 1',
				},
				{
					src: 'https://picsum.photos/768/432?random=5',
					alt: 'image 2',
				},
				{
					src: 'https://picsum.photos/768/432?random=6',
					alt: 'image 3',
				},
			]
		},
	},
};
