#!/bin/bash

##
 # @inspired by
 #   - https://github.com/docker-library/wordpress/issues/130
 #   - https://github.com/docker-library/wordpress/issues/194
 ##


# mute CMD from official wordpress image
sed -i -e 's/^exec "$@"/#exec "$@"/g' /usr/local/bin/docker-entrypoint.sh

# execute bash script from official wordpress image
. /usr/local/bin/docker-entrypoint.sh

####################################
# Execute your custom commands here

composer install --prefer-dist --no-scripts --no-dev

# Stop your commands
####################################

# execute CMD
exec "$@"
