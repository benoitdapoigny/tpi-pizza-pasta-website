module.exports = {

	/**
	 * Get the scss variable value if exists.
	 *
	 * For example, we use this for colors documenation generation where
	 * a color value could be either a css value or a scss variable and
	 * we need to output the css value.
	 * Ex: { '$my-color': '$color-brand', '$my-other-color': '#fff' }
	 *
	 * @param {string} color value of the color (ie. #fffff or $color-brand)
	 * @param {array} colors list of scss variables (ie [ {name: 'color1': value: '#abc'}, {name: 'color2': value: '#fa6'} ])
	 */
	hbsColorOrGetFromVar(color, colors) {

    // bail if not a scss var
		if (!color.startsWith('$')) {
      return color;
		}

		// get variable name without the '$'
		const colorName = color.substring(1);

		// return the value of the scss variable
		return colors.find(c => c.name === colorName).value;
	},
};
