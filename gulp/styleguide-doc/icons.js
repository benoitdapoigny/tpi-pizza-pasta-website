/* eslint-disable curly */
const fs = require('fs');
const path = require('path');

const util = require('util');
const jsBeautify = require('js-beautify');

function sortGroups(a, b) {
	// First order by order
	if (a.order < b.order) return -1;
	if (a.order > b.order) return 1;

	// Then by name
	const nameB = b.name.toUpperCase(); // ignore upper and lowercase
	const nameA = a.name.toUpperCase(); // ignore upper and lowercase

	if (nameA < nameB) return -1;
	if (nameA > nameB) return 1;

	return 0; // order and names must be equal
}

function getIconsbyGroups(basePath) {
	const groups = [];
	const folders = [{ name: 'general', src: basePath, order: 0 }];

	do {
		const folder = folders.shift();
		const icons = [];

		fs.readdirSync(folder.src, { withFileTypes: true }).forEach((entry) => {
			if (entry.isDirectory()) {
				folders.push({ name: entry.name, src: path.join(folder.src, entry.name), order: 1 });
			}
			else if (entry.name.includes('.svg')) {
				icons.push(entry.name.replace('.svg', ''));
			}
		});

		groups.push({
			name: folder.name,
			icons,
			order: folder.order,
		});
	} while (folders.length > 0);

	return groups.sort(sortGroups);
}

module.exports = (config) => new Promise((resolve) => {
	const iconConfig = {
		context: {
			groups: getIconsbyGroups(config.src),
		},
	};

	const output = jsBeautify(`module.exports = ${ util.inspect(iconConfig, { depth: null }) }`, {
		indent_size: 2,
	});

	fs.writeFile(config.dest, output, resolve);
});
