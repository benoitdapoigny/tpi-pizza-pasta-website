/* eslint-disable import/no-extraneous-dependencies */
const fs = require('fs');
const jsBeautify = require('js-beautify');
const util = require('util');

function exportConfig(config) {
	return new Promise((resolve) => {
		const variables = {
			categories: [],
		};

		const variableData = fs.readFileSync(config.src).toString().split('\n');

		// get categories
		variableData.forEach((line, index) => {
			if (line.match(/color variables/i) && ! line.match(/\/\//)) {
				variables.categories.push({
					title: line.replace(/\*|\./g, '').trim().replace(/(.*\scolor).*/gi, '$1s'),
					index,
					colors: [],
				});
			}
		});

		// fill up categories with colors
		variables.categories.forEach((category) => {
			let currentIndex = category.index + 2;
			while (variableData[ currentIndex ] !== '/**' && variableData[ currentIndex ] !== '') {
				const matches = variableData[ currentIndex ].match(/\$([^:]*):\s?([^;]*);/);
				category.colors.push({
					name: matches[ 1 ],
					value: matches[ 2 ],
				});
				currentIndex += 1;
			}
		});

		const colorConfig = {
			context: {
				palettes: variables.categories,
			},
		};

		const output = jsBeautify(`module.exports = ${ util.inspect(colorConfig, { depth: null }) }`, {
			indent_size: 2,
		});

		fs.writeFile(config.dest, output, resolve);
	});
}

module.exports = exportConfig;
