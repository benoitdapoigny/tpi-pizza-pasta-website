---
title: "Colors"
label: "Colors"
---
[Design tokens](https://medium.com/eightshapes-llc/25dd82d58421) are named entities that store visual design information. These are used in place of hard-coded values (such as hex values for color or pixels for spacing) in order to maintain a scalable, consistent system for UI development.

**Please prefer the variable over hardcoded value in Sass files.**
***

{{#each palettes }}
<div class="color-palette">
	<h3>{{ title }}</h3>
	<div class="colors">
		{{#each colors }}
			<div class="color" style="color: {{ colorOrGetFromVar value ../colors }};">
				<div class="color-values">
					<pre>${{name}}</pre>
					<div>{{value}}</div>
				</div>
			</div>
		{{/each}}
	</div>
</div>
{{/each}}

<script>
(function() {
	// copy color variable/value in clipboard
	const aioColors = document.querySelectorAll('.color-values div, .color-values pre');
	aioColors.forEach(color => {
		color.addEventListener('click', () => {
			const selection = window.getSelection();
			const range = document.createRange();
			range.selectNodeContents(color);
			selection.removeAllRanges();
			selection.addRange(range);

			try {
				document.execCommand('copy');
				selection.removeAllRanges();
				const original = color.textContent;
				color.textContent = 'Copied!';
				color.classList.add('success');

				setTimeout(() => {
					color.textContent = original;
					color.classList.remove('success');
				}, 1200);
			} catch(e) {
				const errorMsg = document.querySelector('.error-msg');
				errorMsg.classList.add('show');
				setTimeout(() => {
					errorMsg.classList.remove('show');
				}, 1200);
			}
		});
	});
})();
</script>
