All icons are included in the `static/icons.svg` SVG sprite.

**_To include an icon_**, in this example **home** icon, use the following markup:

```
<svg role="img">
	<use href="#home" />
</svg>
```

***

Available icons
---------------

<!-- TODO understand why svg sprite is not included (webpack) -->
<div class="doc-icons">
	{{#each groups }}
	<h3>{{ name }}</h3>
	<ul>
		{{#each icons }}
		<li>
			<div class="doc-icons__icon">
				<svg role="img">
					<use href="#{{ . }}" />
				</svg>
				<h4>{{ . }}</h4>
			</div>
		</li>
		{{/each }}
	</ul>
	{{/each }}
</div>

{{inlinesvg '/img/icons.svg' style="display:none;" id="svg-sprite" }}

***
⤺ _[back to docs homepage](/)_
