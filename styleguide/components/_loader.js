// Utils
import lineClamp from '../js/utils/line-clamp';

/**
 * Auto injected components Imports
 * ↓ please keep comments (starting with `// ===`) to keep the auto insertion working
 */
// === Imports ===
import MenuSection from './molecules/menu-section/menu-section';

// === Loader Object ===
const COMPONENTS_TO_LOAD = [
	{ view: MenuSection, name: 'Menu-section', selector: '[data-menu-section]' },
];

// eslint-disable-next-line prefer-const
let LOADED_COMPONENTS = {};

const LOAD_ONCE_COMPONENTS = [];

// Execute utilities before components loading
lineClamp();

/**
 * Load all the components.
 * All the component are builded using the same interface, so...
 * the .bind method does the magic for everyone.
 */
export function componentsLoader() {
	for (let i = 0; i < COMPONENTS_TO_LOAD.length; i++) {
		const {
			selector, view, name,
		} = COMPONENTS_TO_LOAD[ i ];
		const element = document.querySelectorAll(selector);
		// Only if the DOM element exist
		// we load the view.
		if (element && element.length) {
			for (let j = 0; j < element.length; j++) {
				// Create the view instance
				// and push the view in the loaded view cache object.
				if (name + j in LOADED_COMPONENTS) {
					// eslint-disable-next-line no-continue
					continue;
				}

				let v;
				try {
					// eslint-disable-next-line new-cap
					v = new view(element[ j ], name);
					v.init();
				}
				catch (error) {
					console.error('Could not init view:', name, '⬇️');
					console.error(error);
				}

				LOADED_COMPONENTS[ name + j ] = v;
			}
		}
	}

	// DEBUG: Exposes the loaded views for dev purposes
	// window.COMPONENTS = LOADED_COMPONENTS;

	return LOADED_COMPONENTS;
}

export function componentsDestroy() {
	Object.keys(LOADED_COMPONENTS).forEach(
		(key) => {
			const destroy = ! LOAD_ONCE_COMPONENTS.includes(LOADED_COMPONENTS[ key ].name);

			if (destroy) {
				LOADED_COMPONENTS[ key ].destroy();
				delete LOADED_COMPONENTS[ key ];
			}
		}
	);

	return LOADED_COMPONENTS;
}
