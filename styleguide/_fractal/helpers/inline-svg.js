const svg = require('handlebars-helper-svg');
const fs = require('fs');
const ltx = require('ltx');

function parse(xml, mod) {
	const svg = ltx.parse(xml);
	if (svg.name != 'svg') {
		throw new TypeError("Input must be an SVG");
	}

	delete svg.attrs.xmlns;
	delete svg.attrs['xmlns:xlink'];

	return svg;
}

// TODO: add cache mechanism similar to https://github.com/aredridel/npm-handlebars-helper-svg/blob/master/index.js
module.exports = {
	twigSVG(name, opts) {
		// get file path
		const path = this.context._config.web.static.path + name; // TODO: wrap the export in a function(fractal) and use fractal instead of this? would have to dig a bit to understand if this would be better, inspired by https://github.com/frctl/nunjucks/blob/master/src/filters/path.js

		// bail early if file doesn't exist
		if ( !fs.existsSync(path) ) return;

		// get & parse file content
		const content = fs.readFileSync(path, 'utf-8');
		const svg = parse(content);

		// merge options to svg tag
		if (Array.isArray(opts) && opts.length) {
			delete opts[0]._keys;
			Object.assign(svg.attrs, opts[0]);
		}

		// return svg content
		return svg.root().toString();
	},
	hbsSVG(name, opts) {
    const path = opts.data.root._config.web.static.path + name

    // bail early if file doesn't exist
    if ( !fs.existsSync(path) ) return;

		return svg(path, opts);
	}
}

