/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	order: 1,
	status: 'ready',
	label: 'Paragraph',
	display: {
		padding: '30px',
	},
	context: {
		modifiers: [],
		attributes: [],
		data: {
			text: 'Lorem ipsum sit dolor amet are red, violets are blue, so he said: lets go! And we didn\'t, <del>\'cause</del> <ins>because</ins> we are just some dumb letters with <a href="#">a long link to be sure to see when it wraps across lines</a> trapped in an <u>underlined</u>, <strong>strong</strong>, <b>bold</b>, <em>emphasized</em>, <i>italic</i>, <mark>marked</mark>, <sub>sub</sub>, <sup>sup</sup>, <small>small</small> matrix that we cannot ever escape.',
		},
	},
	variants: [
		{
			name: 'lead',
			context: {
				modifiers: ['p--lead'],
				attributes: [],
			},
		},
	],
};
