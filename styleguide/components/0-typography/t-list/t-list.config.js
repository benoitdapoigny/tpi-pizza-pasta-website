/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'ready',
	label: 'List',
	display: {
		padding: '30px'
	},
	context: {
		modifiers: [],
		attributes: []
	}
};
