/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Article',
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			header: {
				modifiers: ['header--article'],
				data: {
					image: {
						src: '/img/placeholder/header-article.png',
						alt: 'Pizza Pasta'
					},
					article: {
						title: 'This is a nice dark h1 that can grow quite a lot if it is needed',
						date: '18 Novembre 2019',
						author: 'Le chef',
						tag: '#Livraison'
					}
				}
			},
			content: "<summary>This is p.large Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</summary><h2>This is a dark h2 Lorem ipsum dolor sit amet</h2><p><b>This is p.b > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</b></p><figure><img src=\"/img/placeholder/food_plate.jpg\"><p>© Pizza - Pasta</p></figure><p>This is p > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <a href=\"#\">Ut enim ad minim veniam</a>, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p><div class=\"wp-block-button button--secondary\"><a class=\"wp-block-button__link\" href=\"#\" target=\"_blank\">Get more<svg><use href=\"#external\" /></svg></a></div><hr><h3>This is an h3 : Liste à puces</h3><ul><li>Cras dignissim dictum ante, eget mollis leo molestie non</li><li>Aenean hendrerit sem ac lorem iaculis, condimentum sagittis leo bibendum. Curabitur sit amet tellus ut neque commodo dignissim:</li><ul><li>Cras porta consequat egestas.</li><li>Etiam risus eros, congue at leo at, finibus facilisis est.</li></ul></ul><h3>Liste à numéros</h3><ol><li>Cras dignissim dictum ante, eget mollis leo molestie non</li><li>Aenean hendrerit sem ac lorem iaculis, condimentum sagittis leo bibendum. Curabitur sit amet tellus ut neque commodo dignissim:</li><ol><li>Cras porta consequat egestas.</li><li>Etiam risus eros, congue at leo at, finibus facilisis est.</li></ol></ol><h2>This is a dark h2 Lorem ipsum dolor sit amet</h2><p><b>This is p.b > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</b></p><h4>This is a h4</h4><p>This is p > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>"
		},
	},
};
