<?php

namespace SUPT\Types\Page;

/**
 * Constants
 */
const MODEL_NAME = 'page';
const ALLOWED_BLOCK_TYPES = [
	// Core blocks
	'core/image',

	// Custom blocks
	'supt/contact',
	'supt/free-text',
	'supt/schedule',
	'supt/weekmenu',
	'supt/menu',
	'supt/news',
];

const META_CONTACT = 'contact';

/**
 * Action & filter hooks
 */
add_action( 'init', __NAMESPACE__ . '\register_page_metas' );
add_action( 'init', __NAMESPACE__ . '\register_blocks_template', 20 );
add_action( 'allowed_block_types', __NAMESPACE__ . '\allowed_block_types', 20, 2 );

/**
 * Register page meta which will be retrievable
 * in the block editor
 */
function register_page_metas() {
	register_meta('post', META_CONTACT, [
		'object_subtype' => MODEL_NAME,
		'type'         => 'object',
		'single'       => true,
		'show_in_rest' => [
			'schema' => [
				'properties' => [
					'name' => [ 'type' => 'string' ],
					'street' => [ 'type' => 'string' ],
					'city' => [ 'type' => 'string' ],
					'phone' => [ 'type' => 'string' ],
					'email' => [ 'type' => 'string' ],
				]
			]
		],
	]);
}

/**
 * Register the intial template
 */
function register_blocks_template() {
	$page_type_object = get_post_type_object( MODEL_NAME );
	$page_type_object->template = [
		[ 'supt/contact' ],
	];
}

/**
 * Define allowed blocks for the editor
 */
function allowed_block_types( $allowed_block_types, $post ) {
	if ( $post->post_type !== MODEL_NAME ) return $allowed_block_types;

	return ALLOWED_BLOCK_TYPES;
}

function get_meta($page = null) {
	return get_post_meta($page->ID, META_CONTACT, true);
}
