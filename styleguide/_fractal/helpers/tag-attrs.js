/**
 * Usage <div class="my-div" {{ attributes|attrs }}></div>
 * NOTE Do not forget to put a space before the helper
 *
 * Config:
 * attributes: { 'attr': true, 'attr2': 'value1', 'attr3': false, 'attr4': 0 }
 *
 * Result
 * <div class="my-div" attr attr2="value1" attr4="0"></div>
 */
module.exports = (data) => {
	const attrs = [];

	// bail early if invalid data
	if (!data || typeof data !== 'object' || !Object.keys(data) || !Object.keys(data).length) return '';

	Object.keys(data).forEach((k) => {
		// vars
		let attribute = `${k}`;
		const val = data[k];

		// skip if key is "_keys" (swig injects this in the object)
		if (k === '_keys') return;

		// skip if false
		if (val === false || val === null) return;

		// append value if anything but bool(true) or undefined
		if (val !== true || typeof val !== 'undefined') attribute += `="${val}"`;

		// add to results
		attrs.push(attribute);
	});

	// return
	return attrs.join(' ');
};
