<?php

namespace SUPT\Settings\Fields;

function render_switch_field($args) {
	printf(
		'<div class="switch"><input class="switch__chk" type="checkbox" id="%1$s" name="%2$s" %3$s><label for="%1$s" class="switch__label" tabindex="-1"></label></div>',
		$args['id'],
		$args['name'],
		($args['checked']  ? 'checked' : '')
	);
}

/**
 * Render a listing page setting field
 */
function render_field_listing_page($args) {
	wp_dropdown_pages( array(
		'name'              => $args['name'],
		'echo'              => true,
		'show_option_none'  => __( '&mdash; Select &mdash;' ),
		'option_none_value' => '0',
		'selected'          => get_option($args['name']),
	) );
}

/**
 * Render a category setting field
 */
function render_field_categories($args) {
	wp_dropdown_categories( [
		'name'              => $args['name'],
		'echo'              => true,
		'show_option_none'  => __( '&mdash; Select &mdash;' ),
		'option_none_value' => '0',
		'selected'          => get_option( $args['name'] ),
		'taxonomy'          => $args['taxonomy'],
		'hide_empty' => false,
	 ] );
}
