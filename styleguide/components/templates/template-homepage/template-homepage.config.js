/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Homepage',
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			header: {
				data: {
					image: {
						src: '/img/placeholder/header.png',
						alt: 'Pizza Pasta'
					},
					contact: {
						data: {
							phone: '021 634 73 73',
							email: 'contact@pizza-pasta.ch',
							name: 'Pizza-Pasta',
							street: 'Rue du Chemin 4',
							city: '1000 Lausanne'
						}
					},
				}
			},
			schedule: {
				data: {
					opening_schedule: [
						{
							day: 'Lundi',
							text: '9h - 14h30 / fermé'
						},
						{
							day: 'Mardi-Vendredi',
							text: '9h - 14h30 / 18h - 23h'
						},
						{
							day: 'Samedi',
							text: '11h - 14h30 / 18h - 23h'
						},
						{
							day: 'Dimanche',
							text: 'fermé'
						},
					],
					meal_schedule: [
						{
							day: 'Lundi',
							text: '11h30 - 14h / fermé'
						},
						{
							day: 'Mardi-Vendredi',
							text: '11h30 - 14h / 18h30 - 22h'
						},
						{
							day: 'Samedi',
							text: '11h30 - 14h / 18h - 22h30'
						},
						{
							day: 'Dimanche',
							text: 'fermé'
						}
					]
				}
			},
			menu: {
				data: {
					list: [
						{
							data: {
								title: 'Entrées',
								list: [
									{
										name: 'Salade verte',
										price: '6.50'
									},
									{
										name: 'Salade mêlée',
										price: '8.00'
									},
									{
										name: 'Velouté aux légumes',
										price: '11.00'
									}
								],
								informations: null
							}
						},
						{
							data: {
								title: 'Propositions d\'été',
								list: [
									{
										name: 'Grande salade mêlée',
										price: '16.00'
									},
									{
										name: 'Salade Pacifique',
										description: 'Poulet, crevettes, ananas, salade verte',
										price: '19.50'
									},
									{
										name: 'Salade fitness',
										description: 'Salade mêlée, escalope de poulet grillée',
										price: '20.00'
									},
									{
										name: 'Salade César',
										description: 'Salade verte, émincé de poulet, croûtons, lardons, tomates',
										price: '20.00'
									},
									{
										name: 'Salade Gourmande',
										description: 'Salade verte, jambon, gruyère, lardons, oeuf dur, croûtons, tomates',
										price: '20.00'
									},
									{
										name: 'Salade de queues de crevettes tièdes',
										description: 'Salade verte, tomates, carottes, queues de crevettes',
										price: '20.00'
									},
								],
								informations: [
									{
										title: 'Sauces au choix',
										list: ['Poivre vert: 4.50', 'Beurre maison: 4.50', 'Morilles: 4.50']
									},
									{
										title: 'Provenance de nos viandes',
										list: ['Suisse: boeuf et veau', 'Canada, Australie: cheval', 'France, Brésil: poulet']
									}
								]
							}
						},
						{
							data: {
								title: 'Pâtes',
								list: [
									{
										name: 'Carbonara',
										price: '16.00'
									},
									{
										name: 'Bolognaise',
										price: '19.50'
									}
								],
								informations: null
							}
						},
						{
							data: {
								title: 'Burgers',
								list: [
									{
										name: 'The Beef Burger',
										description: 'Gruyère, rucola, oignons rouges, moutarde en grains et mayonnaise, pommes frites',
										price: '20.00'
									},
									{
										name: 'The Beef Burger',
										description: 'Gruyère, rucola, oignons rouges, moutarde en grains et mayonnaise, pommes frites',
										price: '20.00'
									}
								],
								informations: [
									{
										title: 'Sauces au choix',
										list: ['Poivre vert: 4.50', 'Beurre maison: 4.50', 'Morilles: 4.50']
									},
									{
										title: 'Provenance de nos viandes',
										list: ['Suisse: boeuf et veau', 'Canada, Australie: cheval', 'France, Brésil: poulet']
									}
								]
							}
						}
					]
				}
			},
			section_card: {
				data: {
					list: [
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						},
						{
							title: 'Découvrez la saveur de nos pizza !',
							description: 'Lorem ipsum dolores nice news content should not be too long to still be interesting. But yes we can do one more phrase that should work this way! Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
						}
					]
				}
			},
			freetext: '<h1>This is a nice dark h1</h1><h2>This is a dark h2 Lorem ipsum dolor sit amet</h2><p class="p--lead">This is p.large Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p><p><b>This is p.b > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</b></p><p>This is p > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>This is p > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p><hr><h2>This is a dark h2 Lorem ipsum dolor sit amet</h2><p><b>This is p.b > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</b></p><p>This is p > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'
		},
	},
};
