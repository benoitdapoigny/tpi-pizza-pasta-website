module.exports = {
	"extends": [
		"airbnb-base",
		"plugin:@wordpress/eslint-plugin/recommended"
	],
	// "parser": "babel-eslint",
	"parserOptions": {
		"ecmaVersion": 6
	},
	"globals": {
		"navigator": true,
		"wp": true,
		"viewport_service": true
	},
	"settings": {
		"import/core-modules": [
			"base-view",

			// TODO find how to avoid to have to list all of @wordpress/...
			"@wordpress/i18n", "@wordpress/block",
			"@wordpress/element", "@wordpress/data",
			"@wordpress/components", "@wordpress/blocks",
			"@wordpress/block-editor", "@wordpress/keycodes",
			"@wordpress/blob", "@wordpress/compose",
			"@wordpress/dom-ready", "@wordpress/rich-text",
			'@wordpress/a11y', '@wordpress/url', '@wordpress/viewport',
			"@wordpress/edit-post", "@wordpress/plugins",
			"@wordpress/api-fetch", "@wordpress/icons",
			"@wordpress/hooks","@wordpress/date",
			"@wordpress/url", "@wordpress/html-entities"
		],
		"import/resolver": {
			"node": {
				"paths": [ "styleguide/js", "styleguide/components" ]
			}
		}
	},
	"env": {
		"browser": true,
		// TODO: .config.js & other server-side files: node (maybe 2 eslint configs?)
	},
	"rules": {
		"no-tabs": 0,
		"radix": 0,
		"indent": [2, "tab", { "SwitchCase": 1 }],
		"no-console": ["error", { "allow": ["warn", "error", "info"] }],
		"quotes": ["error", "single", { "allowTemplateLiterals": true }],
		"class-methods-use-this": 0,
		"brace-style": ["error", "stroustrup"],
		"import/no-extraneous-dependencies":  0,
		"no-restricted-imports": [2, "lodash"], // please use lodash-es
		"no-plusplus": ["error", { "allowForLoopAfterthoughts": true }],
		"react/prop-types": 0,

		"space-in-parens": ["error", "never"],
		"array-bracket-spacing": ["error", "never"],
		"object-curly-spacing": ["error", "always"],
	}
};
