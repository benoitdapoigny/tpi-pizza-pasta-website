import { EventEmitter } from 'events';
// import debounce from '../utils/debounce';

// const DEBOUNCE_DELAY = 100; // in ms
const ALERTS_SELECTOR = "[data-alerts-wrapper] [data-alert], [data-alerts-wrapper] [data-geoloc-alert]";
const ACTIVE_CLASS = "is-open";
const TARGETS_SELECTOR = "[data-offset-alerts]";
const TARGETS_ATTRIBUTE = "data-offset-alerts";

export default class AlertsOffset extends EventEmitter {
	init() {
		this.changeHandlerInstance = this.changeHandler.bind(this);
	}

	bindEvents() {
		viewport_service.on("change", this.changeHandlerInstance);
		document.addEventListener("alert_changed", this.changeHandlerInstance);
	}

	changeHandler() {
		// get height of the alerts
		let height = [].slice.call(
			document.querySelectorAll(ALERTS_SELECTOR)
		).reduce(
			(prevVal, el) => prevVal + (el.classList.contains(ACTIVE_CLASS) ? el.offsetHeight : 0), 0
		);

		// apply margin-top (or another style attr) to elements we want to "offset"
		// usage example:
		// `<div data-offset-alerts>` - offset alerts, with attribute margin-top (by default)
		// `<div data-offset-alerts="top">` - offset alerts, with attribute top
		[].slice.call(
			document.querySelectorAll(TARGETS_SELECTOR)
		).forEach(el => {
			let attr = el.getAttribute(TARGETS_ATTRIBUTE) || "margin-top";
			el.style.setProperty(attr, height + "px");
		});
	}
}


/**
 * Alerts offset management init
 */
let alerts_offset_service = new AlertsOffset();
alerts_offset_service.init();
alerts_offset_service.bindEvents();
window.alerts_offset_service = alerts_offset_service;
