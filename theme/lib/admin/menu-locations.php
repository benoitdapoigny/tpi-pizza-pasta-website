<?php

namespace SUPT\Admin;

/**
 * Action & Filter hooks
 */
add_action( 'init', __NAMESPACE__.'\register_menu_locations' );


function register_menu_locations() {
	register_nav_menus([
		'primary' => _x( 'Main Menu', 'Menu Name', 'supt' ),
		'footer'  => _x( 'Footer menu', 'Menu name', 'supt' ),
	]);
}
