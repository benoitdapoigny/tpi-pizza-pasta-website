import { Component, Fragment } from '@wordpress/element';
import { RichText } from '@wordpress/block-editor';

export default class Schedule extends Component {
	constructor(...args) {
		super(...args);

		this.state = {
		};
	}

	componentDidMount() {
		const { attributes: attrs } = this.props;
	}

	render() {
		const { attributes: attrs, setAttributes, isSelected } = this.props;

		return (
			<Fragment>
				<div class="schedule">
					<h1 class="schedule__title">Horaires</h1>
					<h2 class="schedule__section-title">Heures d'ouverture</h2>
					<h2 class="schedule__section-title">Heures de repas</h2>
					<div class="schedule__desktop-days">
						<div class="schedule__section-item-day">
							<span>Lundi</span>
							<span>Mardi-Vendredi</span>
							<span>Samedi</span>
							<span>Dimanche</span>
						</div>
					</div>
					<div class="schedule__section">
							<div class="schedule__section-item-text">
								<RichText
									allowedFormats={ [] }
									value={ attrs.day1 }
									onChange={ (day1) => setAttributes({ day1 }) }
									placeholder={ '9h - 14h30 / fermé' }
									keepPlaceholderOnFocus
								/>
								<RichText
									allowedFormats={ [] }
									value={ attrs.day2 }
									onChange={ (day2) => setAttributes({ day2 }) }
									placeholder={ '9h - 14h30 / 18h - 23h' }
									keepPlaceholderOnFocus
								/>
								<RichText
									allowedFormats={ [] }
									value={ attrs.day3 }
									onChange={ (day3) => setAttributes({ day3 }) }
									placeholder={ '11h - 14h30 / 18h - 23h' }
									keepPlaceholderOnFocus
								/>
								<RichText
									allowedFormats={ [] }
									value={ attrs.day4 }
									onChange={ (day4) => setAttributes({ day4 }) }
									placeholder={ 'fermé' }
									keepPlaceholderOnFocus
								/>
							</div>
					</div>
					<div class="schedule__section">
						<div class="schedule__section-item-text">
						<RichText
							allowedFormats={ [] }
							value={ attrs.mealday1 }
							onChange={ (mealday1) => setAttributes({ mealday1 }) }
							placeholder={ '11h30 - 14h / fermé' }
							keepPlaceholderOnFocus
						/>
						<RichText
							allowedFormats={ [] }
							value={ attrs.mealday2 }
							onChange={ (mealday2) => setAttributes({ mealday2 }) }
							placeholder={ '11h30 - 14h / 18h30 - 22h' }
							keepPlaceholderOnFocus
						/>
						<RichText
							allowedFormats={ [] }
							value={ attrs.mealday3 }
							onChange={ (mealday3) => setAttributes({ mealday3 }) }
							placeholder={ '11h30 - 14h / 18h - 22h30' }
							keepPlaceholderOnFocus
						/>
						<RichText
							allowedFormats={ [] }
							value={ attrs.mealday4 }
							onChange={ (mealday4) => setAttributes({ mealday4 }) }
							placeholder={ 'fermé' }
							keepPlaceholderOnFocus
						/>
						</div>
					</div>
				</div>
			</Fragment>
		);
	}
}
