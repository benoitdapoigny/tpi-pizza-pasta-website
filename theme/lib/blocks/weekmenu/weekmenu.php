<?php

namespace SUPT\Blocks\Weekmenu;

use Timber\Timber;

/**
 * ACTION & FILTERS
 * ================
 */
add_action( 'init', __NAMESPACE__ . '\register_blocks' );

function register_blocks() {
	register_block_type( 'supt/weekmenu', [ 'render_callback' => __NAMESPACE__ . '\callback' ] );
}

function buildContext($attrs) {
	$data = [];

	foreach ($attrs as $key => $value) {
		$data[$key]['modifiers'] = ['menu-section--weekmenu'];
		$data[$key]['data'] = $value;
	}

	return $data;
}

function callback( $attrs ) {
	$context = [
		'data' => [
			'date' => $attrs['date'],
			'list' => buildContext($attrs['meals']),
		]
	];

	return Timber::compile( 'organisms/weekmenu/weekmenu.twig', $context );
}
