<?php

namespace SUPT\Blocks\Menu;

use Timber\Timber;

/**
 * ACTION & FILTERS
 * ================
 */
add_action( 'init', __NAMESPACE__ . '\register_blocks' );

function register_blocks() {
	register_block_type( 'supt/menu', [ 'render_callback' => __NAMESPACE__ . '\callback' ] );
}

function build_context( $attrs )  {
	$data = [];

	foreach ($attrs['sections'] as $key => $value) {
		$data[$key] = [
			'data'  => $value
		];
	}

	return $data;
}

function callback( $attrs ) {
	$context = [
		'data' => [
			'list' => build_context($attrs)
		]
	];

	return Timber::compile( 'organisms/menu/menu.twig', $context );
}
