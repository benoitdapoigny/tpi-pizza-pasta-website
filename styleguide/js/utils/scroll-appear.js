/**
 * This file manage the animations active
 * during the scroll.
 * It will trigger:
 * - Elements fade in
 * - Menu chapters
 * - Menu selected color
 */
import ScrollMagic from 'scrollmagic';
// import { Number, setTimeout } from 'core-js/library/web/timers';
// require('utils/scrollmagic-debug'); // TODO comment when not on debug

const SECTION_SELECTOR = '[data-appear]';
const STATE_APPEAR = 'is-appear-on';
const DEFAULT_OFFSET = 0.2;

/**
 * Scroll callback.
 */
function addActiveClass(element, delay=0) {
	setTimeout(() => {
		element.classList.add( STATE_APPEAR );
	}, delay);
}

/**
 * Create scene scroll.
 */
function createSceneScroll(element, config, controller) {
	let delay = element.getAttribute('data-appear-delay-ms');
	delay = delay ? parseInt(delay) : 0;
	let handlers = [];
	handlers.push((evt) => {
		addActiveClass(element, delay);
	});

	let types = [];
	let typesAttr = element.getAttribute('data-appear-types');
	if (typesAttr && typesAttr.length) {
		types = typesAttr.split(',');
	}

	// specific handlers
	types.forEach((type) => {
		switch (type) {
			case 'icons':
				handlers.push((evt) => {
					animateBodymovinIcons(element, delay);
				});
				break;
			default:
				break;
		}
	});

	let handler = (evt) => {
		handlers.forEach((fct) => {
			fct(evt);
		});
	};
	return createScene(config, 'enter', handler, controller);
}

/**
 * Create a scene.
 */
function createScene(config, events, handler, controller) {
	return new ScrollMagic.Scene(config)
		.on(events, handler)
		// .addIndicators({
		// 		name: config.triggerElement.className,
		// })
		.addTo(controller);
}

function setScene( scene ) {
	if ( scene.classList.contains( STATE_APPEAR ) ) {
		return false;
	}

	let dataWith = scene.getAttribute('data-appear-with');
	let dataHook = scene.getAttribute('data-appear-triggerHook');

	// let dataOffset = scene.getAttribute('data-appear-offset');

	let dataOffset = scene.hasAttribute('data-appear-offset') ? parseInt(scene.getAttribute('data-appear-offset')) : parseInt(window.getComputedStyle(scene).paddingTop) + (viewport_service.current_height * DEFAULT_OFFSET);

	let triggerElement = (
		dataWith != null && dataWith !== false && dataWith.length
		? document.querySelector(`[data-appear="${dataWith}"]`)
		: scene
	);
	let triggerHook = (
		dataHook != null && dataHook !== false
		? Number(dataHook)
		: 1
	);
	let offset = (dataOffset != null && dataOffset !== false ? dataOffset : 0 );

	return createSceneScroll(scene, {
		triggerElement: triggerElement,
		triggerHook: triggerHook,
		offset: offset
	}, controller);
}

let controller = null;
let scenes = [];

export default function() {
	// Create a scroll magic controller.
	if (!controller) {
		controller = new ScrollMagic.Controller({ globalSceneOptions: { duration: 0 }});
	}

	// Destroy and unbind the
	// scenes.
	scenes.forEach((s) => {
		if ( s && s.destroy) {
			s.destroy(true);
		}
	});
	scenes.length = 0;
	// Global function
	// for "normal" animations
	// SELECTORS.forEach((s) => {
	scenes = Array.prototype.slice.call(document.querySelectorAll(SECTION_SELECTOR)).map( setScene );
}
