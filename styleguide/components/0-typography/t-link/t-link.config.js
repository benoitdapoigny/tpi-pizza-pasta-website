/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	order: 2,
	status: 'wip',
	label: 'Link',
	collated: true,
	display: {
		padding: '30px',
	},
	context: {
		modifiers: ['link'],
		html_attrs: {},
		data: 'Normal link: underline which follow the text color'
	},
	variants: [
		{
			name: 'link-typo',
			context: {
				modifiers: ['link-typo'],
				data: 'Typography link : underline has the brand color'
			}
		}
	]
};
