<?php

namespace SUPT;

/**
 * Test if a given url returns 200.
 */
function test_url($url) {
	$curl = curl_init($url);

	curl_setopt($curl, CURLOPT_NOBODY, true);

	// Debug options
	if (WP_DEBUG) {
		if ( is_ssl() ) {
			curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
		}
		// curl_setopt($curl, CURLOPT_VERBOSE, true); // to debug only
	}

	$content = curl_exec($curl);
	$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close( $curl );

	if ($content === false || $code == 404) {
		return false;
	}
	else {
		return true;
	}
}
