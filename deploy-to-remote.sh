#!/bin/sh
source ./scripts/multiselect.sh
source $HOME/.nvm/nvm.sh;

#===========================================
# Install and setup WordPress core
#===========================================
#
# README:
#
# * We assume wp-cli is already installed and
#   wp-cli aliases are defined in `./wp-cli.yml` file.
#   If not, install it and define the aliases
#   @see https://gitlab.com/snippets/1928672
#
# * Edit the variable below to define
#   the website configurations
#
#===========================================

SSH_USER='paii_superhuit'
SSH_HOST='83.166.138.39' # IP address

THEME_NAME='pizza-pasta'

LOCALE="fr_FR"
URL="tpi.dapoignybenoit.superhuit.ch"
TITLE="PizzaPasta"
ADMIN_USER="admin"
ADMIN_PASSWORD="admin"
ADMIN_EMAIL="tech+supt-stack@superhuit.ch"

DB_NAME="paii_pizza"
DB_USER="paii_user_pizza"
DB_PASSWORD="2mwxaxCpmMt2"
DB_HOST="paii.myd.infomaniak.com"

echo ""
echo "To which environment do you want to deploy?"
echo "------"
select reply in "Production" "Exit"; do
	case $reply in
		"Production")
			INSTANCE="@production";
			REMOTE_PATH="./web/tpi/";
			break;;
		"Exit")       exit;;
		* )           echo "Invalid option, aborted"; exit;;
	esac
done


# #===========================================
# # /!\ STOP to edit here /!\
# #===========================================

options[0]='Build components'
options[1]='Install and setup WordPress core'
options[2]='Transfert & activate theme'
options[3]='Install composer dependencies'
options[4]='Transfert plugin & activate plugins in codebase'
options[5]='Install & activate plugins from WordPress'
options[6]='Configure other options'

opts="$(IFS=';'; echo "${options[*]}")"

echo ""
echo "Please select the operations you want to perform:"
echo "------"
multiselect results "$opts" "true;true;true;false;false;false;false"

if [ "${results[0]}" = true ]
then
	echo
	echo "Build components"
	echo "------"

	nvm use && yarn && yarn build
fi

if [ "${results[1]}" = true ]
	then
	echo
	echo "Install WordPress"
	echo "------"
	# Download core
	wp $INSTANCE core download

	# Create wp-config.php
	# wp $INSTANCE config create --dbname=$DB_NAME --dbuser=$DB_USER --dbpass=$DB_PASSWORD --dbhost=$DB_HOST
	chmod 644 wp-config.php

	# Install core
	wp $INSTANCE core install --url="$URL" --title="$TITLE" --admin_user="$ADMIN_USER" --admin_password="$ADMIN_PASSWORD" --admin_email="$ADMIN_EMAIL"

	# Setup the language
	wp $INSTANCE language core install "$LOCALE" --activate
fi

if [ "${results[2]}" = true ]
then
	echo
	echo "Transfert theme"
	echo "------"
	rsync -auvrzq ./theme/* "$SSH_USER@$SSH_HOST:$REMOTE_PATH/wp-content/themes/$THEME_NAME"
	rsync -auvrzqm --include="*/" --include="*.twig" --exclude="*" \
		./styleguide/components/* "$SSH_USER@$SSH_HOST:$REMOTE_PATH/wp-content/themes/$THEME_NAME/templates/components"

	echo
	echo "Activate theme"
	echo "------"
	wp $INSTANCE theme activate "$THEME_NAME"

	wp $INSTANCE theme uninstall twentysixteen --quiet
	wp $INSTANCE theme uninstall twentyseventeen --quiet
	wp $INSTANCE theme uninstall twentynineteen --quiet
	wp $INSTANCE theme uninstall twentytwenty --quiet
fi

if [ "${results[3]}" = true ]
then
	echo
	echo "Install composer dependencies"
	echo "------"
	scp composer* "$SSH_USER@$SSH_HOST:$REMOTE_PATH"
	ssh "$SSH_USER@$SSH_HOST" "composer install --working-dir=$REMOTE_PATH"
fi

if [ "${results[4]}" = true ]
then
	echo
	echo "Transfert plugin in codebase"
	echo "------"
	# rsync -auvrzq ./wordpress/wp-content/plugins/advanced-custom-fields-pro "$SSH_USER@$SSH_HOST:$REMOTE_PATH/wp-content/plugins/"


	echo
	echo "Activate plugin in codebase"
	echo "------"
	# wp $INSTANCE plugin activate advanced-custom-fields-pro --network

fi


if [ "${results[5]}" = true ]
then
	echo
	echo "Install & activate plugins from WordPress"
	echo "------"

	# TODO Set the version when development ends
	wp $INSTANCE plugin install disable-comments --activate # --version="1.10.2"
	wp $INSTANCE plugin install disable-embeds --activate # --version="1.4.0"
	wp $INSTANCE plugin install disable-emojis --activate # --version="1.7.2"
	wp $INSTANCE plugin install redirection --activate # --version="4.5.1"
	wp $INSTANCE plugin install resmushit-image-optimizer --activate # --version="0.2.4"
	wp $INSTANCE plugin install safe-svg --activate # --version="1.9.7"
	wp $INSTANCE plugin install timber-library --activate # --version="1.13.0"
	wp $INSTANCE plugin install wordpress-seo --activate # --version="12.7.1"


	wp $INSTANCE plugin uninstall hello --deactivate --quiet
	wp $INSTANCE plugin uninstall akismet --deactivate --quiet

	echo
	echo "Update translations"
	echo "------"
	wp $INSTANCE language core update

	echo
	echo "Disable comments system"
	echo "------"
	# Disable comments system
	wp $INSTANCE option update disable_comments_options '{"disabled_post_types":["post","page","event","attachment"],"remove_everywhere":true,"permanent":false,"extra_post_types":false,"db_version":6}' --format=json
fi


if [ "${results[6]}" = true ]
then
	echo
	echo "Configuring other options"
	echo "------"

	# wp $INSTANCE option update __option_name__ __otion_value__
fi


echo
echo "Deploy complete"
echo "==============="
