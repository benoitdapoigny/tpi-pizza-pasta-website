---
title: UI Component Library
---
{{ _config.project.description }} **{{ _config.project.client.name }}**

## Atomic design
This styleguide follows atomic design principles ([Read more](http://bradfrost.com/blog/post/atomic-web-design/)).

* **atoms** - irreducible parts of the design (HTML tags such as a form label, an input or a button)
* **molecules** - groups of atoms bonded together and are the smallest fundamental units of a compound
* **organisms** - groups of molecules joined together to form a relatively complex, distinct section of an interface
* **pages** - full page, composed of organisms. Should have as minimum styles as possible, only positioning and some generic behavior (fixed elements on scroll, etc.)

<!-- ### Important note on organisms
In our project, organisms are sections that can be integrated individually into the website.
Since we want them to be easily editable form wordpress, we use the same names here and in wordpress.
They are separated by 3 subcategories:

1. **global** - site-wide organisms such as header, footer, etc.
2. **blocks** - blocks of content, think of them as something the admin can drag & drop to create the content of a page (block-text, block-image-gallery, block-video,…)
3. **widgets** - customizable sections of the website, think of them as sections the admin can add and configure in specific places of pages (call to action, newsletter onboarding form,…) -->

## Browser compatibility
This project should be compatible with IE11+ and the 2 latest versions of major browsers.

## Fonts and weights in use

**Lato** `$font-primary`
* 400 - Regular - `font-weight: $font-weight-regular`
* 700 - Bold - `font-weight: $font-weight-bold`
* 900 - Black - `font-weight: $font-weight-black`

**Work Sans** `$font-secondary`
* 700 - Bold - `font-weight: $font-weight-bold`


## Contact

If you have any question or suggestions,
you are welcome to contact us at [{{ _config.project.author.email }}](mailto:{{ _config.project.author.email }})
