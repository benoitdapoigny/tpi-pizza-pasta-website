/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Figure',
	display: {
		padding: '30px',
	},
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			image: {
				src: '/img/placeholder/food_plate.jpg',
				alt: ''
			},
			caption: '© Alisa Anton - Vivamus id turpis vel libero molestie dictum.'
		},
	},
};
