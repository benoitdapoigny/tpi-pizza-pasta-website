/**
 * Simple touch detection approach inspired by
 * http://javascriptkit.com/dhtmltutors/sticky-hover-issue-solutions.shtml
 *
 * Maybe a lib like this one could be more useful
 * http://detect-it.rafrex.com/
 *
 */
document.addEventListener('touchstart', function addtouchclass() {
	document.documentElement.classList.add('can-touch');
	document.removeEventListener('touchstart', addtouchclass, false);
	window.isTouchDevice = true;
}, false);
