import BaseView from 'base-view.js';

/**
 * @class AnalyticsElement
 */
export default class AnalyticsEvents extends BaseView {

	bind() {
		if ( window.ga === undefined ) {
			return;
		}

		this.eventName = this.el.getAttribute('data-ga-event');
		this.eventCat = this.el.getAttribute('data-ga-event-cat');
		this.eventLabel = this.el.getAttribute('data-ga-event-label');

		this.on( this.eventName, this.eventHandler.bind(this) );
	}

	/**
	 * Event handler.
	 */
	eventHandler() {
		window.ga( 'send' , 'event', this.eventCat, this.eventName, this.eventLabel );
	}
}
