<?php

namespace SUPT\Types;

/**
 * CUSTOM POST TYPES
 * =================
 *
 * Custom post types and their associated taxonomies
 */

require_once __DIR__ . '/PageType/PageType.php';
require_once __DIR__ . '/PostType/PostType.php';

// Init the Custom Post Types
PostType::init();
