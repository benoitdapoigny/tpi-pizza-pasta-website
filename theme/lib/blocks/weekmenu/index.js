import './editor.scss';

import { registerBlockType } from '@wordpress/blocks';

import edit from './edit';

registerBlockType('supt/weekmenu', {
	title: 'Menu de la semaine',
	description: 'Modifier différents menus de la semaine.',
	icon: 'excerpt-view',
	category: 'common',
	attributes: {
		date:   { type: 'string' },
		meals: { type: 'array', default: [
			{
				title: 'Proposition de la semaine',
				list: [
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
				],
			},
			{
				title: 'Lundi',
				list: [
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
				],
				isHoliday: false,
				holiday: { type: 'string' }
			},
			{
				title: 'Mardi',
				list: [
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
				],
				isHoliday: false,
				holiday: { type: 'string' }
			},
			{
				title: 'Mercredi',
				list: [
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
				],
				isHoliday: false,
				holiday: { type: 'string' }
			},
			{
				title: 'Jeudi',
				list: [
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
				],
				isHoliday: false,
				holiday: { type: 'string' }
			},
			{
				title: 'Vendredi',
				list: [
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
					{ name: { type: 'string' }, description: { type: 'string' }, price: { type: 'string' } },
				],
				isHoliday: false,
				holiday: { type: 'string' }
			},
		]}
	},
	keywords: [
		'carte',
		'semaine'
	],
	edit,
	save: () => null,
});
