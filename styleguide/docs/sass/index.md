












# functions













<h2 id="function-strip-unit"><a href="#function-strip-unit">strip-unit</a></h2>


``` scss
@function strip-unit($position) { 
	@return $value / ($value * 0 + 1);
}
```
















### Description

<p>strip the unit from any css value</p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$position`</td><td><p>like <code>13px</code>, <code>2em</code> or <code>5%</code></p></td><td>`Value`</td><td>—</td></tr></tbody></table>

















### Used By




























<h2 id="function-non-destructive-map-merge"><a href="#function-non-destructive-map-merge">non-destructive-map-merge</a></h2>


``` scss
@function non-destructive-map-merge($parent-map, $child-map) { 
	$result: $parent-map;
	@each $key, $value in $child-map {
		@if (not map-has-key($result, $key)) or (type-of(map-get($result, $key)) != type-of($value)) or (not (type-of(map-get($result, $key)) == map and type-of($value) == map)) {
			$result: map-merge($result, ($key: $value));
		}
		@else {
			$result: map-merge($result, ($key: non-destructive-map-merge(map-get($result, $key), $value)));
		}
	}
	@return $result;
}
```
















### Description

<p>deep merge two maps
stolen from <a href="https://medium.com/@pentzzsolt/a-non-destructive-map-merge-function-for-sass-f91637f87b2e">https://medium.com/@pentzzsolt/a-non-destructive-map-merge-function-for-sass-f91637f87b2e</a></p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$parent-map`</td><td><p>initial map (ie: <code>$global-styles</code>)</p></td><td>`map`</td><td>—</td></tr><tr><td>`$child-map`</td><td><p>map to merge into the first one (ie: <code>$local-styles</code>)</p></td><td>`map`</td><td>—</td></tr></tbody></table>









































---



# mixins













<h2 id="mixin-clearfix"><a href="#mixin-clearfix">clearfix</a></h2>


``` scss
@mixin clearfix() { 
	&:after {
		content: "";
		display: table;
		clear: both;
	}
}
```
















### Description

<p>The CSS clearfix</p>
















































<h2 id="mixin-line-clamp"><a href="#mixin-line-clamp">line-clamp</a></h2>


``` scss
@mixin line-clamp($max-lines) { 
	display: -webkit-box;
	-webkit-line-clamp: $max-lines;
	-webkit-box-orient: vertical;
	overflow: hidden;
}
```
















### Description

<p>Line Clamp</p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$max-lines`</td><td><p>Number of lines</p></td><td>`Number`</td><td>—</td></tr></tbody></table>












































<h2 id="mixin-reset-button"><a href="#mixin-reset-button">reset-button</a></h2>


``` scss
@mixin reset-button() { 
	font-size: 1em;
	background-color: transparent;
	border: 0;
	border-radius: 0;
	outline: 0;
	appearance: none;
	padding: 0;
	cursor: pointer;

	&::-moz-focus-inner {
		border: 0;
	}
}
```
















### Description

<p>Reset button default styles</p>
















































<h2 id="mixin-reset-input"><a href="#mixin-reset-input">reset-input</a></h2>


``` scss
@mixin reset-input() { 
	font-size: 1em;
	background-color: transparent;
	border: 0;
	border-radius: 0;
	outline: 0;
	padding: 0;
	appearance: none;
}
```
















### Description

<p>Reset input default styles</p>
















































<h2 id="mixin-linear-gradient"><a href="#mixin-linear-gradient">linear-gradient</a></h2>


``` scss
@mixin linear-gradient($direction, $color-stops...) { 
	background: nth(nth($color-stops, 1), 1);
	background: linear-gradient($direction, $color-stops);
}
```
















### Description

<p>Print a linear-gradient with a plain color fallback</p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$direction`</td><td><p>Linear gradient direction</p></td><td>`Keyword` or `Angle`</td><td>—</td></tr><tr><td>`$color-stops`</td><td><p>List of color-stops composing the gradient</p></td><td>`Arglist`</td><td>—</td></tr></tbody></table>












































<h2 id="mixin-ie10"><a href="#mixin-ie10">ie10</a></h2>


``` scss
@mixin ie10() { 
	@warn "DEPRECATED: put the styles in a separate file served only for old browsers instead";
	@if $breakpoint {
		@media screen and (-ms-high-contrast: active) and (min-width: $breakpoint),
			screen and (-ms-high-contrast: none) and (min-width: $breakpoint) {
			@content;
		}
	}

 @else {
		@media screen and (-ms-high-contrast: active),
			screen and (-ms-high-contrast: none) {
			@content;
		}
	}
}
```
















### Description

<p>IE10+ specific styling
NOTE: not recommended, use only if there&#39;s no cleaner way (simple fallback / progressive enhancement)</p>









### Content

This mixin allows extra content to be passed (through the ``@content`` directive).











































<h2 id="mixin-edge"><a href="#mixin-edge">edge</a></h2>


``` scss
@mixin edge() { 
	@warn "DEPRECATED: put the styles in a separate file served only for old browsers instead";
	@if $breakpoint {
		@media (min-width: $breakpoint) {
			@supports (-ms-ime-align: auto) {
				@content;
			}
		}
	}

 @else {
		@supports (-ms-ime-align: auto) {
			@content;
		}
	}
}
```
















### Description

<p>Edge (12+) specific styling
NOTE: not recommended, use only if there&#39;s no cleaner way (simple fallback / progressive enhancement)</p>









### Content

This mixin allows extra content to be passed (through the ``@content`` directive).











































<h2 id="mixin-fluid"><a href="#mixin-fluid">fluid</a></h2>


``` scss
@mixin fluid($properties, $steps, $initial-value: true, $after-value: true, $suffix: null) { 
	// TODO: throw an error if $min-value / $max-value / $min-vw / $max-vw is NOT in px
	//       as it breaks the calculation

	$steps_keys: map-keys($steps);
	$steps_nb: length($steps_keys);
	@if $steps_nb < 2 {
		@error "$steps should contain at least 2 breakpoints";
	}

	// initial values
	@if $initial-value {
		$first-vw: nth($steps_keys, 1);
		@if $initial-value == true {
			$initial-value: map-get($map: $steps, $key: nth($steps_keys, 1));
		}
		@media screen and (max-width: $first-vw - 1) {
			@each $property in $properties {
				#{$property}: $initial-value $suffix;
			}
		}
	}

	// fluid steps
	@for $i from 1 through $steps_nb - 1 {
		$min-vw: nth($steps_keys, $i);
		$min-value: map-get($map: $steps, $key: $min-vw);
		$max-vw: nth($steps_keys, $i + 1);
		$max-value: map-get($map: $steps, $key: $max-vw);

		@media screen and (min-width: $min-vw) and (max-width: $max-vw - 1) {
			@each $property in $properties {
				// TODO: optimise "calc()"? test poly-fluid-props
				#{$property}: calc(
					#{$min-value} +
					#{strip-unit($max-value - $min-value)} *
					(100vw - #{$min-vw}) /
					#{strip-unit($max-vw - $min-vw)}
				)
					$suffix;
			}
		}
	}

	// after values
	@if $after-value {
		$last-vw: nth($steps_keys, $steps_nb);
		@if $after-value == true {
			$after-value: map-get($map: $steps, $key: nth($steps_keys, $steps_nb));
		}
		@media screen and (min-width: $last-vw) {
			@each $property in $properties {
				#{$property}: $after-value $suffix;
			}
		}
	}
}
```
















### Description

<p>Render a fluid sizing in px for any property</p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$properties`</td><td><p>Space-separated list of properties to act on</p></td><td>`Properties`</td><td>—</td></tr><tr><td>`$steps`</td><td><p>Map of fluid steps (viewport_width: size_in_px), ex: (375px: 16px, 1024px: 25px)</p></td><td>`Map`</td><td>—</td></tr><tr><td>`$initial-value`</td><td><p>(optional) Assign a value for smaller viewports (if true, will take the 1st step&#39;s value)</p></td><td>`Boolean` or `Size`</td><td>`true`</td></tr><tr><td>`$after-value`</td><td><p>(optional) Assign a value for bigger viewports (if true, will take the last step&#39;s value)</p></td><td>`Boolean` or `Size`</td><td>`true`</td></tr><tr><td>`$suffix`</td><td><p>(optional) Add a suffix such as <code>!important</code></p></td><td>`Boolean`</td><td>`null`</td></tr></tbody></table>














### Throws


* <p>$steps should contain at least 2 breakpoints</p>







### Requires
































<h2 id="mixin-fluid-props"><a href="#mixin-fluid-props">fluid-props</a></h2>


``` scss
@mixin fluid-props($properties, $min-value, $max-value, $min-vw: $fluid-min, $max-vw: $fluid-max, $set-initial-properties: true, $set-after-properties: true, $suffix: null) { 
	@warn "DEPRECATED: use `fluid` instead";

	// TODO: throw an error if $min-value / $max-value / $min-vw / $max-vw is NOT in px
	//       as it breaks the calculation
	@if $set-initial-properties {
		@media screen and (max-width: $min-vw - 1) {
			@each $property in $properties {
				#{$property}: $min-value $suffix;
			}
		}
	}

	@media screen and (min-width: $min-vw) and (max-width: $max-vw - 1) {
		@each $property in $properties {
			#{$property}: calc(
				#{$min-value} +
				#{strip-unit($max-value - $min-value)} *
				(100vw - #{$min-vw}) /
				#{strip-unit($max-vw - $min-vw)}
			)
				$suffix;
		}
	}

	@if $set-after-properties {
		@media screen and (min-width: $max-vw) {
			@each $property in $properties {
				#{$property}: $max-value $suffix;
			}
		}
	}
}
```
















### Description

<p>Fluid Props - DEPRECATED: use <code>fluid</code> instead</p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$properties`</td><td><p>Space-separated list of properties to act on</p></td><td>`Properties`</td><td>—</td></tr><tr><td>`$min-value`</td><td><p>Minimum value in px (<code>$properties</code> will have this value when <code>viewport width == $min-vw</code>)</p></td><td>`Size`</td><td>—</td></tr><tr><td>`$max-value`</td><td><p>Maximum value in px (<code>$properties</code> will have this value when <code>viewport width == $max-vw</code>)</p></td><td>`Size`</td><td>—</td></tr><tr><td>`$min-vw`</td><td><p>(optional) Viewport width in px at which we start growing</p></td><td>`Size`</td><td>`$fluid-min`</td></tr><tr><td>`$max-vw`</td><td><p>(optional) Viewport width in px at which we stop growing</p></td><td>`Size`</td><td>`$fluid-max`</td></tr><tr><td>`$set-initial-properties`</td><td><p>(optional) Define a default attribute for <code>viewport width &lt; $min-vw</code> ?</p></td><td>`Boolean`</td><td>`true`</td></tr><tr><td>`$set-after-properties`</td><td><p>(optional) Define a lasting attribute after <code>viewport width &gt;= $max-vw</code> ?</p></td><td>`Boolean`</td><td>`true`</td></tr><tr><td>`$suffix`</td><td><p>(optional) Add a suffix such as <code>!important</code></p></td><td>`Boolean`</td><td>`null`</td></tr></tbody></table>

















### Requires
































<h2 id="mixin-object-fit"><a href="#mixin-object-fit">object-fit</a></h2>


``` scss
@mixin object-fit($fit, $position) { 
	-o-object-fit: $fit;
	object-fit: $fit;

	@if $position {
		font-family: 'object-fit: #{$fit}; object-position: #{$position}';
		-o-object-position: $position;
		object-position: $position;
	}
	@else {
		font-family: 'object-fit: #{$fit}';
	}
}
```
















### Description

<p>CSS object-fit feature with polyfill</p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$fit`</td><td><p>object-fit value</p></td><td>`Keyword`</td><td>—</td></tr><tr><td>`$position`</td><td><p>object-position value (optional)</p></td><td>`Keyword`</td><td>—</td></tr></tbody></table>




















### Used By




























<h2 id="mixin-image-aspect-ratio"><a href="#mixin-image-aspect-ratio">image-aspect-ratio</a></h2>


``` scss
@mixin image-aspect-ratio($width, $height) { 
	overflow: hidden;
	position: relative;

	&::before {
		display: block;
		content: "";
		width: 100%;
		padding-top: calc(#{$height} / #{$width} * 100%);
	}

	& > img {
		@include object-fit(cover);
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		width: 100%;
		height: 100%;
	}
}
```
















### Description

<p>CSS image-aspect-ratio feature
image keeps the aspect ratio according to the given width and height values
include mixin on the image-wrapper
adapted from <a href="https://css-tricks.com/aspect-ratio-boxes/">https://css-tricks.com/aspect-ratio-boxes/</a>, <a href="https://css-tricks.com/snippets/sass/maintain-aspect-ratio-mixin/">https://css-tricks.com/snippets/sass/maintain-aspect-ratio-mixin/</a></p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$width`</td><td><p>image width</p></td><td>`Keyword`</td><td>—</td></tr><tr><td>`$height`</td><td><p>image height</p></td><td>`Keyword`</td><td>—</td></tr></tbody></table>

















### Requires
































<h2 id="mixin-grid-row"><a href="#mixin-grid-row">grid-row</a></h2>


``` scss
@mixin grid-row($row) { 
	@warn "DEPRECATED: put the styles in a separate file served only for old browsers instead";
	/* autoprefixer: ignore next */
	grid-row: $row;

	.cssgridlegacy & {
		-ms-grid-row: $row !important;
	}
}
```
















### Description

<p>CSS grid-row feature with IE10+ support</p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$row`</td><td><p>grid-row value</p></td><td>`Keyword`</td><td>—</td></tr></tbody></table>












































<h2 id="mixin-grid-columns-with-ie-fallback"><a href="#mixin-grid-columns-with-ie-fallback">grid-columns-with-ie-fallback</a></h2>


``` scss
@mixin grid-columns-with-ie-fallback($columns, $column-width, $gap, $min-vw, $max-vw: false, $max-vw: 6) { 
	@error "DEPRECATED: put the styles in a separate file served only for old browsers instead";
	@media (min-width: $min-vw) and (max-width: $max-vw) {
		grid-template-columns: repeat($columns, $column-width);

		@content;

		.cssgridlegacy & {
			$cols-repeat: $columns - 1;
			@if $cols-repeat == 0 {
				grid-template-columns: $column-width;
			}
			@else {
				grid-template-columns: $column-width repeat($cols-repeat, $gap + $column-width);
			}
			width: ($column-width * $columns) + (($cols-repeat) * $gap);

			> * {
				@for $col from 1 through $columns {
					&:nth-child(#{$columns}n+#{$col}) {
						grid-column: $col;

						@if $col > 1 {
							margin-left: $gap;
						}
					}
				}

				@for $i from 1 through $nb-items {
					$row: ceil($i / $columns);
					&:nth-child(#{$i}) {
						grid-row: $row;

						@if $row > 1 {
							margin-top: $gap;
						}
					}
				}
			}
		}
	}
}
```
















### Description

<p>CSS grid-column layout with IE10+ support
Helps create a &quot;fixed&quot; layout with n columns
Which set specifically the column % row for each cell on IE
DEPRECATED! this is generating too much garbage. we should now use modern CSS Grid features and have a not-pixel-perfect fallback for IE in a separate css file that we serve only on selected browsers</p>






### Parameters

<table><thead><tr><td>Name</td><td>Description</td><td>Type</td><td>Default Value</td></tr></thead><tbody><tr><td>`$columns`</td><td><p>The number of columns</p></td><td>`Number`</td><td>—</td></tr><tr><td>`$column-width`</td><td><p>The column width in px</p></td><td>`Size`</td><td>—</td></tr><tr><td>`$gap`</td><td><p>The gap width in px</p></td><td>`Size`</td><td>—</td></tr><tr><td>`$min-vw`</td><td><p>Viewport width in px at which the layout should start</p></td><td>`Size`</td><td>—</td></tr><tr><td>`$max-vw`</td><td><p>(optional) Viewport width in px at which the layout should stop</p></td><td>`Size`</td><td>`false`</td></tr><tr><td>`$max-vw`</td><td><p>(optional) The number max of items</p></td><td>`Number`</td><td>`6`</td></tr></tbody></table>





### Content

This mixin allows extra content to be passed (through the ``@content`` directive).













### Throws


* <p>DEPRECATED: put the styles in a separate file served only for old browsers instead</p>












































<h2 id="css-// TODO: Simplify
// The $line-height unit and calculation in this mixin are confusing
// I&#39;m sure we can have something more intuitive  — (Pierre&#39;s opinion).
@mixin lhCrop($line-height)"><a href="#css-// TODO: Simplify
// The $line-height unit and calculation in this mixin are confusing
// I&#39;m sure we can have something more intuitive  — (Pierre&#39;s opinion).
@mixin lhCrop($line-height)">// TODO: Simplify
// The $line-height unit and calculation in this mixin are confusing
// I&#39;m sure we can have something more intuitive  — (Pierre&#39;s opinion).
@mixin lhCrop($line-height)</a></h2>


``` scss
@css // TODO: Simplify
// The $line-height unit and calculation in this mixin are confusing
// I&#39;m sure we can have something more intuitive  — (Pierre&#39;s opinion).
@mixin lhCrop($line-height)() { }
```
















### Description

<p>Crop line-height
remove the top space from your text element
<a href="https://medium.com/codyhouse/line-height-crop-a-simple-css-formula-to-remove-top-space-from-your-text-9c3de06d7c6f">https://medium.com/codyhouse/line-height-crop-a-simple-css-formula-to-remove-top-space-from-your-text-9c3de06d7c6f</a></p>






























---

