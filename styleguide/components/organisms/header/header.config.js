/* eslint-disable */
/**
 * For more information, see
 * http://fractal.build/guide/components/configuration
 */

module.exports = {
	status: 'wip',
	label: 'Header',
	context: {
		modifiers: [],
		html_attrs: [],
		data: {
			image: {
				src: '/img/placeholder/header.png',
				alt: 'Pizza Pasta'
			},
			logo: {
				src: '/img/logo-white.png',
				alt: 'Pizza Pasta Logo'
			},
			contact: {
				data: {
					contact: [
						'021 634 73 73',
						'contact@pizza-pasta.ch'
					],
					adresse: [
						'Pizza-Pasta',
						'Rue du Chemin 4',
						'1000 Lausanne'
					]
				}
			}
		},
	},
	variants: [
		{
			name: 'article',
			context: {
				modifiers: ['header--article'],
				html_attrs: [],
				data: {
					image: {
						src: '/img/placeholder/header-article.png',
						alt: 'Pizza Pasta'
					},
					article: {
						title: 'This is a nice dark h1 that can grow quite a lot if it is needed',
						date: '18 Novembre 2019',
						author: 'Le chef',
						tag: '#Livraison'
					}
				},
			},
		},
	],
};
