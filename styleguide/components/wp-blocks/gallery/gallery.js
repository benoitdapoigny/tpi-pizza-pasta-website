import BaseView from '../../../js/base-view';
import './lightgallery/js/lightgallery';
import './lightgallery/plugins/lg-thumbnail';
import './lightgallery/plugins/lg-zoom';

const CONFIG = {
	selector: '[data-gallery-img]',
	subHtmlSelectorRelative: true,
	speed: 500,
	getCaptionFromTitleOrAlt: false,
	thumbnail: true,
	loadYoutubeThumbnail: false,
	loadVimeoThumbnail: false,
	thumbWidth: 100,
	thumbContHeight: 100 * 9 / 16 + 20, // force 16/9 ratio (there is 20px padding)
	loadDailymotionThumbnail: false,
	download: false,
	autoplayControls: false,
	actualSize: false,
	share: false,
};

export default class gallery extends BaseView {
	/**
	 * Entrypoint
	 */
	init() {
		// avoid error in lightgallery when picturefill is not included
		if (typeof (window.picturefill) !== 'function') {
			window.picturefill = (obj) => null;
		}

		// initialize lightgallery.js
		// eslint-disable-next-line no-undef
		lightGallery(this.el, CONFIG);
	}

	/**
	 * Gets current gallery object
	 */
	getGallery() {
		return window.lgData[ this.el.getAttribute('lg-uid') ];
	}

	/**
	 * Before the component gets destroyed
	 */
	beforeDestroy() {
		// destroy lightgallery.js
		this.getGallery().destroy(true);
	}
}
