/**
 * Simple keyboard/mouse navigation detection
 * Useful to handle focus/hover styles in specific ways
 */

// default
document.documentElement.classList.add('is-using-mouse');
window.isUsingMouse = true;

// mouse event detected
document.addEventListener('mousedown', () => {
	document.documentElement.classList.add('is-using-mouse');
	document.documentElement.classList.remove('is-using-keyboard');
	window.isUsingMouse = true;
	window.isUsingKeyboard = false;
}, false);

// keyboard event detected
document.addEventListener('keydown', () => {
	document.documentElement.classList.remove('is-using-mouse');
	document.documentElement.classList.add('is-using-keyboard');
	window.isUsingMouse = false;
	window.isUsingKeyboard = true;
}, false);
