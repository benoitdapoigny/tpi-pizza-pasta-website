/**
 * List the components registered
 * in the styleguide.
 */
function listComponents(args, done){
	const app = this.fractal;
	for (let item of app.components.flatten()) {
		this.log(`${item.handle} - ${item.status.label}`);
	}
	done();
};

module.exports = listComponents;
