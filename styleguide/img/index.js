// import all images to be included in the webpack bundle.
function requireAll(r) {
	r.keys().forEach(r);
}
requireAll(require.context('./', true, /\.(gif|png|jpe?g|svg|ico)$/i));
