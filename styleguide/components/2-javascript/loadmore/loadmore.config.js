module.exports = {
	status: 'wip',
	label: 'Load more',
	display: {
		padding: '30px',
	},
	context: {
		config: {
			target: '[data-loadmore-container="3616-code-ula"]',
			query: {},      // json_encode($wp_query->query_vars) // to preserve filters in your query
			currentPage: 1, // get_query_var( 'paged' ) ? get_query_var('paged') : 1 // 1-based index to indicate which page we are viewing now
			maxPage: 4,     // $wp_query->max_num_pages // 1-based index to indicate the last page (when we reach it we hide the loadmore element)
			texts: {
				initial: "Load more",
				loading: "loading…",
				maxReached: "No more articles to load!"
			},
			isFake: true,   // will fake the ajax request, for demo
		}
	}
}
