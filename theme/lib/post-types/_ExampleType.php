<?php

namespace SUPT\Types;

use \WP_Query;

class PostType {

	const MODEL_NAME = 'post';

	const SETTING_LISTING_PAGE = 'page_for_posts';

	const COLUMN_CUSTOM = 'custom-column';


	public static function init() {
		add_action( 'init', [__CLASS__, 'register'] );

		add_filter( 'manage_edit-'. self::MODEL_NAME .'_columns', [__CLASS__, 'customize_edit_table_columns'] );
		add_action( 'manage_'. self::MODEL_NAME .'_posts_custom_column', [ __CLASS__, 'populate_edit_table_columns'], 10, 2 );
	}

	/**
	 * Register the Custom Post Type
	 */
	public static function register() {
		// register_post_type(self::MODEL_NAME, [
		// 	// ...
		// ]);
	}

	/**
	 * Add extra columns in edit table for Post type
	 *
	 * @param array $post_columns An array of column names.
	 */
	public static function customize_edit_table_columns($columns) {
		return array_merge(
			array_slice( $columns, 0, 2 ),
			[
				self::COLUMN_CUSTOM => _x('Custom Column', 'Post Type column name', 'supt')
			],
			array_slice( $columns, 2 )
		);
	}

	/**
	 * Populate extra columns with specific content, if available
	 *
	 * @param string $column_name The name of the column to display.
	 * @param int    $post_id     The current post ID.
	 */
	public static function populate_edit_table_columns( $column_name, $post_id ) {
		if ( $column_name !== self::COLUMN_CUSTOM) return;

		echo  'Custom data';
	}
}
