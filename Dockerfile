#====================================
# README
# This file is for dev purpose only!
#====================================

# TODO:
# - [ ] add htaccess config to protect `./vendor`, `composer.json`, `compose.lock`,…

FROM wordpress:5-php7.4-apache

# Add sudo in order to run wp-cli as the www-data user
RUN apt-get -qq update && apt-get -qq install -y \
  sudo \
  less \
  nano \
  git \
  zip \
  unzip

# PHP config

## Copy custom php config
COPY ./server/php.ini $PHP_INI_DIR/php.ini
## Show php logs in stderr
RUN touch /usr/local/etc/php/conf.d/php_error.ini
RUN echo "log_errors = on" >> /usr/local/etc/php/conf.d/php_error.ini
RUN echo "error_log = /dev/stderr" >> /usr/local/etc/php/conf.d/php_error.ini

#-------------
# Add WP-CLI
#-------------
RUN curl -o /bin/wp-cli.phar https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
COPY ./server/wp-su.sh /bin/wp
RUN chmod +x /bin/wp-cli.phar /bin/wp
## Create directory for wp-cli packages
RUN mkdir -p /var/www/.wp-cli/packages
RUN chmod -R 777 /var/www/.wp-cli

#-------------
# Install Composer
#-------------
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#-------------
# Copy website files
#-------------
## config
COPY ./wordpress/wp-config.php /var/www/html/wp-config.php
COPY ./wordpress/.htaccess /var/www/html/.htaccess


# Cleanup
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

## TODO uncomment to enable HTTPs
# Setup & copy files for SSL (HTTPS) config
# COPY ./server/ssl/localhost-ssl.conf /etc/apache2/sites-available/localhost-ssl.conf
# COPY ./server/ssl/000-default.conf /etc/apache2/sites-available/000-default.conf
# COPY ./server/ssl/server.crt /var/www/ssl/server.crt
# COPY ./server/ssl/server.key /var/www/ssl/server.key
# RUN sudo a2enmod ssl
# RUN sudo a2ensite localhost-ssl
##

COPY ./docker-custom-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-custom-entrypoint.sh"]
CMD ["apache2-foreground"]
